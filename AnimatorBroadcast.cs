﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorBroadcast : MonoBehaviour
{
	public UnityEngine.Events.UnityEvent[] controllerEvents;
	public void CallEvents(int index)
	{
		if(controllerEvents.Length > index)
		{
			controllerEvents[index].Invoke();
			Debug.Log("called method");
		}
	}
}
