﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToGround : MonoBehaviour
{
	public LayerMask ground;
	// Start is called before the first frame update
	void Awake()
    {
		// sit everything on terrain
		Vector3 high = transform.position;
		high.y += 100000f;
		Ray downRay = new Ray(high, Vector3.down);
		RaycastHit raycastHit;
		foreach (Transform child in GetComponentsInChildren<Transform>())
		{
			if(child.tag != "DontGround")
			{
				high.x = child.position.x;
				high.z = child.position.z;
				downRay.origin = high;
				Physics.Raycast(downRay, out raycastHit, float.MaxValue, ground);
				child.position = raycastHit.point;
			}
		}
		enabled = false;
	}
}
