using System;

public static class IntToTime {
	public static string GetTimeFromInt(int seconds) {
		TimeSpan time = TimeSpan.FromSeconds(seconds);
		string timeString;
		if (seconds < 3600) {
			timeString = time.ToString("mm':'ss");
		} else {
			timeString = time.ToString("hh':'mm':'ss");
		}
		return timeString;
	}
}
