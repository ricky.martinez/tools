﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ShopItem : MonoBehaviour
{
	public static float holdTime = .7f;

	public string tooltip;
	public SAVEKEY currencyType, saveType;
	public int index = -1, cost, limit = -1;
	public Text costText;
	public UnityEvent purchasedFunction, setup, couldntAfford, limitReached;
	public Condition limitCheck;

	private void OnEnable()
	{
		setup.Invoke();
	}

	void Start()
	{
		if (costText)
			costText.text = cost.ToString();
	}

	public static string IndexName(SAVEKEY _keyname, int _index)
	{
		return _keyname.ToString() + (_index == -1 ? "" : _index.ToString());
	}

	public static void SetUpAll()
	{
		foreach (ShopItem item in FindObjectsOfType<ShopItem>())
		{
			item.setup.Invoke();
		}
	}

	public void ClickBuy()
	{
		ResetToolTipCounter();
		if (DefaultAfford())
		{
			if (limit == -1 || limitCheck.Invoke())
			{
				SoundManager.instance.PlaySoundEffect(SFX_CLIP.Buy);
				PersistentManager.SaveKeyAddVal(currencyType, -cost);
				purchasedFunction.Invoke();
				SetUpAll();
			}
			else
			{
				limitReached.Invoke();
			}
		}
		else
		{
			couldntAfford.Invoke();
		}
	}

	public void BuyMultiple(int _amount)
	{
		while (--_amount != -1)
		{
			Invoke("ClickBuy", .5f * _amount);
		}
	}

	public void StartToolTip()
	{
		Invoke("ToolTip", holdTime);
	}

	public void ToolTip()
	{
		ToastKit.CallToast(tooltip);
	}

	public void ResetToolTipCounter()
	{
		CancelInvoke("ToolTip");
	}

	public void DefaultPurchase()
	{
		PersistentManager.SaveKeyInc(saveType);
	}

	public void MoneyPurchase()
	{
		ToastKit.CallToast("Money system not set up yet");
	}

	public bool DefaultLimit()
	{
		return (PlayerPrefs.GetFloat(IndexName(saveType, index)) < limit);
	}

	public bool DefaultAfford()
	{
		return PlayerPrefs.GetFloat(currencyType.ToString()) >= cost;
	}

	public void DefaultSetup()
	{
		Animator mAnimator = GetComponent<Animator>();
		if (!limitCheck.Invoke() || !DefaultAfford())
		{
			mAnimator.SetBool("Dis", true);
			mAnimator.SetTrigger("Disabled");
		}
		else
		{
			mAnimator.SetBool("Dis", false);
			mAnimator.SetTrigger("Normal");
		}
	}

	public void DefaultCantBuy()
	{
		ToastKit.CallToast("Not enough " + currencyType.ToString());
	}

	public void DefaultLimitReached()
	{
		ToastKit.CallToast("You can't have anymore " + IndexName(saveType, index));
	}

	public void DefaultWarning(string _message)
	{
		ToastKit.CallToast(_message);
	}
}
