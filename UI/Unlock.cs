﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 0649

public class Unlock : MonoBehaviour {

	#region Setup
	public int value;
	public SAVEKEY saveKey;
	[SerializeField]
	Condition unlock, isNew;
	public bool unlocked
	{
		get { return unlock.Invoke(); }
	}
	[SerializeField]
	UnityEngine.Events.UnityEvent setLocked, setUnlocked, setNew, setOld;

	private void OnEnable()
	{
		CheckNew();
		CheckUnlocked();
	}

	public void CheckUnlocked()
	{
		if (unlock.Invoke())
		{
			setUnlocked.Invoke();
		}
		else
		{
			setLocked.Invoke();
		}
	}

	public void CheckNew()
	{
		if (isNew.Invoke())
		{
			setNew.Invoke();
		}
		else
		{
			setOld.Invoke();
		}
	}
	#endregion

	#region CommonChanges

	public void ChangeImageBlack(UnityEngine.UI.Image _image)
	{
		_image.color = Color.black;
	}

	public void ChangeImageWhite(UnityEngine.UI.Image _image)
	{
		_image.color = Color.white;
	}

	public void FadeImage(UnityEngine.UI.Image _image)
	{
		Color color = _image.color;
		color.a = .3f;
		_image.color = color;
	}
	#endregion

	#region Conditions
	public bool Always()
	{
		return true;
	}

	public bool UnderKey()
	{
		return PlayerPrefs.GetInt(saveKey.ToString()) >= value;
	}

	public bool OnKey()
	{
		return PlayerPrefs.GetInt(saveKey.ToString()) == value;
	}

	//public bool HasSpell()
	//{
	//	return PlayerController.HasSpell(GetComponent<AddSpellDetails>().scroll);
	//}

	//public bool HasAllDark()
	//{
	//	return PlayerController.HasSpells(1,2,3);// && PlayerController.HasSpell(2) && PlayerController.HasSpell(3);
	//}

	//public bool HasAllLight()
	//{
	//	return PlayerController.HasSpells(4,5,6);// && PlayerController.HasSpell(2) && PlayerController.HasSpell(3);
	//}

	//public bool HasAllIce()
	//{
	//	return PlayerController.HasSpells(7, 8, 9);// && PlayerController.HasSpell(2) && PlayerController.HasSpell(3);
	//}

	//public bool HasAllFire()
	//{
	//	return PlayerController.HasSpells(10, 11, 12);// && PlayerController.HasSpell(2) && PlayerController.HasSpell(3);
	//}
	#endregion
}
