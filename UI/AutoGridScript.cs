﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoGridScript : MonoBehaviour
{

	public enum MOVE_TYPE
	{
		Instant,
		Lirp
	}

	public enum ALIGNMENT
	{
		Center,
		TopLeft
	}

	[Tooltip("How should the positions change")]
	public MOVE_TYPE disableType = MOVE_TYPE.Lirp;
	public ALIGNMENT xAlignment = ALIGNMENT.Center;
	public ALIGNMENT yAlignment = ALIGNMENT.Center;

	[Tooltip("Optional, the parent of children to add (this, if nothing is selected)")]
	public Transform parent;
	public bool includeChilren = true;
	[Tooltip("Additional objects")]
	public List<Transform> children;
	public Transform[] removable;

	[System.Serializable]
	public struct Grid
	{
		public Vector2 dimensions, distances;
	}

	[Tooltip("optional, if you placed objects correctly")]
	public Grid grid;
	Vector3[] lirpPoss;

	delegate void SetObjectType();
	SetObjectType SetObjects;
	float lirp, midX, midY;
	public float maxDiff = 5f;

	private void Start()
	{
		if (!parent)
		{
			parent = transform;

		}
		if (includeChilren)
		{
			foreach (Transform child in parent)
			{
				children.Add(child);
			}
		}
		foreach (Transform rem in removable)
		{
			children.Remove(rem);
		}
		removable = null;
		SetObjects += SetMidPoss;

		//cant put this in a turnary? wtf?!
		ConfigureGrid();
		if (disableType == MOVE_TYPE.Instant)
		{
			SetObjects += SetObjectPositionsInstant;
		}
		else
		{
			SetObjects += SetObjectPositionsLirp;
		}
		SetObjects();// run this in case they were set slightly off.
	}

	private void Update()
	{
		if (lirpPoss != null)
		{
			lirp += Time.deltaTime;
			int i = -1; while (++i != children.Count)
			{
				children[i].localPosition = Vector3.Lerp(children[i].localPosition, lirpPoss[i], lirp);
			}
			if (lirp > .95f)
			{
				lirp = 0f;
				lirpPoss = null;
			}
		}
	}

	public void ChangeMoveType(MOVE_TYPE _type)
	{
		disableType = _type;
		if (disableType == MOVE_TYPE.Instant)
		{
			SetObjects -= SetObjectPositionsLirp;
			SetObjects += SetObjectPositionsInstant;
		}
		else
		{
			SetObjects -= SetObjectPositionsInstant;
			SetObjects += SetObjectPositionsLirp;
		}
	}

	void SetMidPoss()
	{
		midX = xAlignment == ALIGNMENT.Center ? (grid.dimensions.x - 1) * .5f : 0f;
		midY = yAlignment == ALIGNMENT.Center ? (grid.dimensions.y - 1) * .5f : 0f;
	}

	Vector3 ReturnGridVector(Transform _tab, int _index)
	{
		return new Vector3(
			(_index % (int)grid.dimensions.x - midX) * grid.distances.x
			, (midY - _index / (int)grid.dimensions.x) * grid.distances.y
			, transform.localPosition.z
			);
	}

	void SetObjectPositionsInstant()
	{
		int fooieIndex, i;
		fooieIndex = i = -1; while (++i != children.Count)
		{
			if (!children[i].gameObject.activeSelf)
			{
				continue;
			}
			children[i].localPosition = ReturnGridVector(children[i], ++fooieIndex);
		}
	}

	void SetObjectPositionsLirp()
	{
		int fooieIndex, i;
		lirpPoss = new Vector3[children.Count];
		fooieIndex = i = -1; while (++i != children.Count)
		{
			if (!children[i].gameObject.activeSelf)
			{
				lirpPoss[i] = children[i].localPosition;
				continue;
			}
			lirpPoss[i] = ReturnGridVector(children[i], ++fooieIndex);// new Vector3(xIndex * grid.distances.x, yIndex * grid.distances.y, children[i].localPosition.z);
		}

	}

	public void SetColumns(int _dim)
	{
		grid.dimensions.x = _dim;

		SetObjects();
	}

	public void SetGrid(Vector2 _dim, Vector2 _dist)
	{
		grid.dimensions = _dim;
		grid.distances = _dist;
		SetObjects();
	}

	public void SetGrid(Grid _grid)
	{
		grid = _grid;
		SetObjects();
	}

	public void SetDistance(Vector2 _dist)
	{
		grid.distances = _dist;
		SetObjects();
	}

	public void SetPositions()
	{
		SetObjects();
	}


	public void ConfigureGrid()
	{
		if (children.Count > 1)
		{
			if (grid.dimensions == Vector2Int.zero && Vector2.zero == grid.distances)
			{
				// establish how many different y's and x's to get a grid size
				List<float> xs = new List<float>(), ys = new List<float>();
				bool same = false;
				float minX, minY, maxX, maxY;
				minX = minY = float.MaxValue;
				maxX = maxY = float.MinValue;
				foreach (Transform tab in children)
				{
					//	x
					float newX = tab.localPosition.x;
					foreach (float x in xs)
					{
						if (Mathf.Abs(x - newX) < maxDiff)// if the x is the same as ANY other, don't add it
						{
							same = true;
							break;
						}
					}
					if (!same)
					{
						xs.Add(newX);
						if (newX < minX)
							minX = newX;
						if (newX > maxX)
							maxX = newX;
					}
					else
					{
						same = false;
					}
					//	y
					newX = tab.localPosition.y;
					foreach (float y in ys)
					{
						if (Mathf.Abs(y - newX) < maxDiff)// if the x is the same as ANY other, don't add it
						{
							same = true;
							break;
						}
					}
					if (!same)
					{
						ys.Add(newX);
						if (newX < minY)
							minY = newX;
						if (newX > maxY)
							maxY = newX;
					}
					else
					{
						same = false;
					}
				}
				if (grid.dimensions.x == 0)
					grid.dimensions.x = xs.Count;
				if (grid.dimensions.y == 0)
					grid.dimensions.y = Mathf.Max(1, Mathf.Ceil(children.Count / (int)grid.dimensions.x));
				if (grid.distances.x == 0)
					grid.distances.x = grid.dimensions.x == 1 ? 0 : (maxX - minX) / (grid.dimensions.x - 1);
				if (grid.distances.y == 0)
				grid.distances.y = grid.dimensions.y == 1 ? 0 : (maxY - minY) / (grid.dimensions.y - 1);
			}
		}
		else
		{
			grid.dimensions = Vector2.one;
		}
	}
}
