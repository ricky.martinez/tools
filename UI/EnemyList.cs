﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyList : MonoBehaviour
{
	public Image image;
	public string description;

	public STAT health, damage, armor, speed, range;

	public string weakness;
	void Start()
	{
		image = transform.GetChild(0).GetComponent<Image>();
		//if (unlock == PlayerPrefs.GetInt(SAVEKEY.MaxLevel.ToString()))
		//{
		//	GetComponent<Outline>().enabled = true;

		//}
		//else if (unlock > PlayerPrefs.GetInt(SAVEKEY.MaxLevel.ToString()))
		//{
		//	image.color = Color.black;
		//}
	}

	public void Selected()
	{
		//FindObjectOfType<Description>().UpdateEnemy(this);// = new Component();
		//GetComponent<Outline>().enabled = false;
		GetComponent<Shadow>().enabled = true;
	}
	public void Deselect()
	{
		//if (unlock == PlayerPrefs.GetInt("Level"))
		//{
		//	GetComponent<Outline>().enabled = true;
		//}
		GetComponent<Shadow>().enabled = false;
	}
}
