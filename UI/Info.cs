﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(Outline))]
[RequireComponent(typeof(EventTrigger))]
[RequireComponent(typeof(Button))]
public class Info : BaseInfo
{
	public string[] descriptions;

	void Start()
	{
		Button btn = GetComponent<Button>();
		if (btn.onClick.GetPersistentEventCount() == 0)
			btn.onClick.AddListener(Selected);
		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.Deselect;
		entry.callback.AddListener(Deselect);
		EventTrigger trig = GetComponent<EventTrigger>();
		if (trig.triggers.Count == 0)
			trig.triggers.Add(entry);
	}

	public virtual void Selected()
	{
		Descriptor[] descrs = FindObjectsOfType<Descriptor>();
		Descriptor descr = null;
		foreach (Descriptor des in descrs)
		{
			if (des.tag == tag)
			{
				descr = des;
				break;
			}
		}
		UpdateInfo(descr);
		GetComponent<Outline>().enabled = true;
	}
	public virtual void Deselect(BaseEventData _data)
	{
		GetComponent<Outline>().enabled = false;
	}

	virtual public void UpdateInfo(Descriptor _descriptor)
	{

		int i = -1; while (++i != images.Length)
		{
			_descriptor.images[i].sprite = images[i].sprite;
			_descriptor.images[i].color = images[i].color;
		}

		i = -1; while (++i != enables.Length)
		{
			_descriptor.enables[i].enabled = enables[i].enabled;
		}

		i = 0;
		_descriptor.descriptions[i].text = name;

		while (++i != descriptions.Length + 1)
		{
			_descriptor.descriptions[i].text = descriptions[i - 1];
		}

	}

	public virtual void ChangeKilledText()
	{
		descriptions[0] = "Killed: " + UnityEngine.PlayerPrefs.GetInt(name);
	}

	public void EmptyDescriptions()
	{
		int i = -1; while (++i != descriptions.Length)
		{
			descriptions[i] = string.Empty;
		}
	}

	public void EmptyDescriptions(int _index)
	{
		int i = -1; while (++i != descriptions.Length)
		{
			if (i != _index)
				descriptions[i] = string.Empty;
		}
	}

	public void EnableRedOutline()
	{
		enables[0].enabled = true;
	}

	public void DisableRedOutline()
	{
		enables[0].enabled = false;
	}
}
