﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public static class TextToTexture
{
	static GameObject _billBObject;
	static public GameObject billBObject
	{
		get
		{
			if (!_billBObject)
				_billBObject = GameObject.Instantiate(Resources.Load<GameObject>("BillbText"));
			return _billBObject;
		}
	}

	static Camera _preRenderCam;
	static public Camera preRenderCamera
	{
		get
		{
			if (!_preRenderCam)
				_preRenderCam = GameObject.Instantiate(Resources.Load<GameObject>("TextCamera")).GetComponent<Camera>();
			return _preRenderCam;
		}
	}

	static TextMeshProUGUI _tmp;
	public static TextMeshProUGUI tmp
	{
		get
		{
			if (!_tmp)
				_tmp = preRenderCamera.GetComponentInChildren<TextMeshProUGUI>();
			return _tmp;
		}
	}

	public static void ChangeTextOfRender(MeshRenderer renderer, string text)
	{
		if (tmp.text == text) return; // ✅ Skip update if text is the same

		preRenderCamera.enabled = true;
		tmp.text = text;
		Canvas.ForceUpdateCanvases();

		// ✅ Use existing RenderTexture or create a new one
		RenderTexture existingRT = renderer.material.mainTexture as RenderTexture;
		if (existingRT == null)
		{
			existingRT = new RenderTexture(preRenderCamera.targetTexture.width, preRenderCamera.targetTexture.height, 16);
			renderer.material.mainTexture = existingRT;
		}

		// ✅ Directly set camera target texture to existingRT
		preRenderCamera.targetTexture = existingRT;

		// ✅ Render directly to the texture (NO need for Graphics.Blit)
		RenderTexture.active = existingRT;
		preRenderCamera.Render();
		RenderTexture.active = null; // ✅ Reset active texture
		renderer.enabled = true;
		preRenderCamera.enabled = false;
	}
}
