using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasManager
{
	static CanvasManager instance;
	public Canvas frontCanvas, backCanvas;
	GameObject transitioner;
	public static CanvasManager Instance { get 
		{
			if (instance == null)
			{
				instance = new CanvasManager();
				Ensure2ScreenSpaceCanvases();
				SceneManager.sceneUnloaded += (x) => { instance = null; };
			}
			return instance;
		}
	}

	public GameObject Transitioner
	{
		get
		{
			if (transitioner == null)
			{
				transitioner = GameObject.Instantiate(Resources.Load<GameObject>("Transitions/Fadeout"), frontCanvas.transform);
			}
			transitioner.transform.SetAsLastSibling();
			return transitioner;
		}
	}

	static void Ensure2ScreenSpaceCanvases()
	{
		var canvasObjs = new List<Canvas>();
		foreach (var item in GameObject.FindObjectsOfType<Canvas>())
		{
			if (item.renderMode == RenderMode.ScreenSpaceOverlay)
			{
				canvasObjs.Add(item);
			}
		}
		if (canvasObjs.Count == 0)
		{
			instance.frontCanvas = CreateDefaultCanvas();
			instance.frontCanvas.sortingOrder = 1;
			instance.backCanvas = CreateDefaultCanvas();
			instance.backCanvas.sortingOrder = -1;
			instance.backCanvas.name = "Back Canvas";
			 new GameObject("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
			return;
		}
		else if (canvasObjs.Count == 1)
		{
			instance.frontCanvas = canvasObjs[0];
			instance.frontCanvas.sortingOrder = 1;
			instance.backCanvas = CreateDefaultCanvas();
			instance.backCanvas.sortingOrder = -1;
			instance.backCanvas.name = "Back Canvas";
			return;
		}
		int frontOrder = int.MinValue, backOrder = int.MaxValue;
		int i = -1;while (++i!= canvasObjs.Count)
		{
			if (canvasObjs[i].sortingOrder > frontOrder)
			{
				frontOrder = canvasObjs[i].sortingOrder;
				instance.frontCanvas = canvasObjs[i];
			}
			if (canvasObjs[i].sortingOrder < backOrder)
			{
				backOrder = canvasObjs[i].sortingOrder;
				instance.backCanvas = canvasObjs[i];
			}
		}
		if (instance.backCanvas.sortingOrder == instance.frontCanvas.sortingOrder)
			++instance.frontCanvas.sortingOrder;
	}

	static Canvas CreateDefaultCanvas()
	{
		var canvScaler = new GameObject("Canvas", typeof(Canvas), typeof(CanvasScaler), typeof(GraphicRaycaster)).GetComponent<CanvasScaler>();
		canvScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvScaler.referenceResolution = new Vector2(1920f, 1080f);
		canvScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
		canvScaler.matchWidthOrHeight = .5f;
		var canva = canvScaler.GetComponent<Canvas>();
		canva.renderMode = RenderMode.ScreenSpaceOverlay;
		return canva;
	}
}
