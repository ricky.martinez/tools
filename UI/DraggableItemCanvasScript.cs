﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

// this class is made to work for more than 1 drag shop being open at the same time without causing errors
public class DraggableItemCanvasScript : DraggableItemScript, IPointerUpHandler, IPointerDownHandler
{
	protected override Vector3 MousePosition()
	{
		return Input.mousePosition;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		OnMouseUp();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		OnMouseDown();
	}

	protected override void MoveToFront()
	{
		transform.parent.SetAsLastSibling();
	}
}