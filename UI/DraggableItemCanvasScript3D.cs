﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

// this class is made to work for more than 1 drag shop being open at the same time without causing errors
public class DraggableItemCanvasScript3D : DraggableItemScript, IPointerUpHandler, IPointerDownHandler
{
	protected override Vector3 MousePosition()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, transform.position.z));
		float distance;
		xy.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		OnMouseUp();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		OnMouseDown();
	}

	protected override void MoveToFront()
	{
		transform.parent.SetAsLastSibling();
	}
}