﻿public enum STAT
{
	High, Medium, Low, None, Infinite, _
}


public class StatInfo : Info
{
	public STAT[] stats;
	public override void UpdateInfo(Descriptor _descriptor)
	{
		base.UpdateInfo(_descriptor);
		int i = -1;
		while (++i != stats.Length)
		{
			_descriptor.descriptions[i + descriptions.Length+ 1].text = stats[i].ToString();
		}
	}

	public void EmptyStats()
	{
		int i = -1; while (++i!= stats.Length)
		{
			stats[i] = STAT._;
		}
	}
}
