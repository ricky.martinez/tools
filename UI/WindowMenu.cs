﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WindowMenu : MonoBehaviour
{
	public static WindowMenu currInstance;
	public GameObject backDraw;
	public System.Action<GameObject> onFirstOpen, onAnyOpen, onclosedOrHid;
	public System.Action onExit, onBack;
	Stack<GameObject> openedWindows = new Stack<GameObject>();

	[SerializeField]
	UnityEvent OnBack;

	public GameObject GetCurrentOpenWindow()
	{
		if (openedWindows.Count == 0)
		{
			return null;
		}
		return openedWindows.Peek();
	}

	private void Awake()
	{
		onBack += OnBack.Invoke;
	}

	public void StackWindow(GameObject replacingWindow)
	{
		if (!openedWindows.Contains(replacingWindow))
		{
			openedWindows.Push(replacingWindow);
			onFirstOpen?.Invoke(replacingWindow);
		}
		onAnyOpen?.Invoke(replacingWindow);
		replacingWindow.SetActive(true);
		replacingWindow.transform.SetAsLastSibling();
		backDraw?.SetActive(true);
	}

	// should be called StackWinHideOldWin
	public void StackWindowHideOthers(GameObject val)
	{
		foreach (var item in openedWindows)
		{
			if (item.activeSelf)
			{
				onclosedOrHid?.Invoke(item);
				item.SetActive(false);
			}
		}
		StackWindow(val);
	}

	public void ReplaceAllWindows(GameObject replacingWindow)
	{
		while (openedWindows.Count > 0)
		{
			var _menoToClose = openedWindows.Pop();
			_menoToClose.SetActive(false);
			onclosedOrHid?.Invoke(_menoToClose);
		}
		StackWindow(replacingWindow);
	}

	public void GoBack()
	{
		if (openedWindows.Count == 0)
			return;
		onBack?.Invoke();
		var _menuToClose = openedWindows.Pop();
		_menuToClose.SetActive(false);
		onclosedOrHid?.Invoke(_menuToClose);
		if (openedWindows.Count == 0)
		{
			backDraw?.SetActive(false);
			onExit?.Invoke();
		}
		else
		{
			openedWindows.Peek().SetActive(true);
			openedWindows.Peek().transform.SetAsLastSibling();
			onAnyOpen?.Invoke(openedWindows.Peek());
		}
	}
	public void Close()
	{
		if (openedWindows.Count == 0)
			return;
		while (openedWindows.Count > 0)
		{
			var _menuToClose = openedWindows.Pop();
			onclosedOrHid?.Invoke(_menuToClose);
			_menuToClose.SetActive(false);
		}
		backDraw?.SetActive(false);
		onExit?.Invoke();
	}
	public void Exit()
	{
		Close();
	}
	public void Hide()
	{
		if (openedWindows.Count == 0)
			return;
		foreach (var item in openedWindows)
		{
			if (item.activeSelf)
			{
				item.SetActive(false);
				onclosedOrHid?.Invoke(item);
			}
		}
		backDraw?.SetActive(false);
	}
	public void Show()
	{
		if (openedWindows.Count == 0)
			return;
		foreach (var item in openedWindows)
		{
			item.SetActive(true);
			onAnyOpen?.Invoke(item);
		}
		backDraw?.SetActive(true);
	}
}
