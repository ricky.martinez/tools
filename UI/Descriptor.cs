﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Descriptor : BaseInfo
{
	public UnityEngine.UI.Text[] descriptions;
	public Info startInfo;
	private void Start()
	{
		UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(startInfo.gameObject);
		startInfo.Invoke("Selected", .1f);
	}
}
