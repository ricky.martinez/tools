﻿using System;
using System.Collections;
using UnityEngine;

public class DraggableContainer : MonoBehaviour
{
	public Transform[] setItems;
	// save set is for where to save the set index; saveType is what children check against to see if htey are unlocked.
	public SAVEKEY saveSet, saveType;
	public bool autoFill = true;
	public float minSqdDist = 100f;
	// Use this for initialization
	void Start()
	{
		sbyte index = -1;
		foreach (DraggableItemScript dragItem in GetComponentsInChildren<DraggableItemScript>())
		{
			// this is for auto-setting
			if (dragItem.index == -1)
				dragItem.index = ++index;
			if (autoFill)
			{
				int i = -1; while (++i != setItems.Length)
				{
					if (dragItem.index == PlayerPrefs.GetInt(saveSet.ToString() + i))
					{
						dragItem.transform.SetParent(setItems[i], false);
						break;
					}
				}
			}
		}
	}

	public void SaveSets()
	{
		int i = -1; while (++i != setItems.Length)
		{
			DraggableItemScript child = setItems[i].GetComponentInChildren<DraggableItemScript>();
			if (child)
				PlayerPrefs.SetInt(saveSet.ToString() + i, child.index);
			else
				PlayerPrefs.SetInt(saveSet.ToString() + i, -1);
		}
			
	}
}