﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInfo : MonoBehaviour {
	[ComponentSelector]
	public Behaviour[] enables;
	public UnityEngine.UI.Image[] images;
}
