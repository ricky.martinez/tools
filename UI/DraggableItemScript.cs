﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

// this class is made to work for multiple shops being open at the same time without causing errors
// this must be the parent of only other slots
public class DraggableItemScript : MonoBehaviour
{
	Transform oldParent;
	[Tooltip("Leave at -1 to automatically be set")]
	public sbyte index = -1;
	bool drag;

	protected virtual Vector3 MousePosition()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane xy = new Plane(Camera.main.transform.forward, new Vector3(0, 0, transform.position.z));
		float distance;
		xy.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void Update()
	{
		if (drag)
		{
			if (oldParent)
			{
				// check if the mouse is far to parent we are moving towards
				if (Vector2.SqrMagnitude(MousePosition() - transform.parent.position) > GetComponentInParent<DraggableContainer>().minSqdDist)
				{
					DraggableItemScript otherChild = oldParent.GetComponentInChildren<DraggableItemScript>();
					if (otherChild)
					{
						otherChild.transform.SetParent(transform.parent, false);
						transform.SetParent(oldParent, false);
						transform.position = MousePosition();
						otherChild.transform.position = otherChild.transform.parent.position;
						otherChild.oldParent = null;
					}
					else
					{
						transform.SetParent(oldParent, false);
						transform.position = MousePosition();
					}
					oldParent = null;
					return;
				}

			}
			else
			{
				transform.position = MousePosition();
				DraggableContainer container = GetComponentInParent<DraggableContainer>();
				// look for potential draggables in the container
				foreach (Transform slot in container.transform)
				{
					if (Vector2.SqrMagnitude(slot.position - transform.position) < container.minSqdDist)
					{
						if (transform.IsChildOf(slot))
							break;
						// if we are within distance and not ourselves:

						DraggableItemScript draggable = slot.GetComponentInChildren<DraggableItemScript>();
						if (draggable)// if there is a draggable in this slot
						{
							Unlock dragUnlock = draggable.GetComponent<Unlock>();
							if (!dragUnlock || dragUnlock.unlocked)
							{
								oldParent = transform.parent;
								draggable.oldParent = draggable.transform.parent;

								// swap the parents of the draggables
								transform.SetParent(draggable.transform.parent, false);
								transform.position = MousePosition();
								draggable.transform.SetParent(oldParent, false);

								draggable.StartCoroutine(draggable.MoveToParent());
								StartCoroutine(MoveToParent());
							}
						}
						else // this is an open slot
						{
							oldParent = transform.parent;
							transform.SetParent(slot,false);
							transform.position = MousePosition();
							StartCoroutine(MoveToParent());
						}
						break;
					}
				}
			}
		}
	}

	protected void OnMouseUp()
	{
		Debug.Log(name + "up");
		if (oldParent)
		{
			DraggableItemScript otherChild = oldParent.GetComponentInChildren<DraggableItemScript>();
			if (otherChild)
				otherChild.oldParent = null;
			oldParent = null;
		}
		else
		{
			transform.position = transform.parent.position;
		}
		DraggableContainer container = GetComponentInParent<DraggableContainer>();
		if (container)
		{
			container.SaveSets();
		}
		drag = false;
	}

	protected void OnMouseDown()
	{
		Debug.Log(name + "down");
		Unlock unlock = GetComponent<Unlock>();
		if (!unlock || unlock.unlocked)
		{
			MoveToFront();
			drag = true;
		}
	}

	protected virtual void MoveToFront()
	{
		++GetComponent<Renderer>().sortingOrder;
	}

	public IEnumerator MoveToParent()
	{
		while (oldParent && Vector2.SqrMagnitude(transform.localPosition) > .2f)
		{
			transform.localPosition *= .9f;// Vector3.Lerp(transform.position, oldParent.position, .1f);
			yield return new WaitForEndOfFrame();
		}
		transform.position = transform.parent.position;
	}
}