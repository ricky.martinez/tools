﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class AutoDragThreshold : MonoBehaviour
{
	static AutoDragThreshold _currInstance;
	public static AutoDragThreshold currInstance { get { if (!_currInstance) _currInstance = new GameObject().AddComponent<AutoDragThreshold>();return _currInstance; } }
	static public float baseDPI = 120;
	EventSystem evSys;

	private void Awake()
	{
		_currInstance = this;
		baseDPI = Screen.dpi / baseDPI;
		evSys = GetComponent<EventSystem>();
		if (!evSys)
			evSys = FindAnyObjectByType<EventSystem>();
		evSys.pixelDragThreshold = Mathf.RoundToInt(evSys.pixelDragThreshold * baseDPI);
	}

	public float RelativeDragDistance(float dis)
	{
		return dis * baseDPI;
	}
}
