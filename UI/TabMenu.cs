﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;

public class TabMenu : MonoBehaviour
{
	[System.Serializable]
	public class TabMenuS
	{
		public Transform tab, menu;
		public TabMenuS(Transform _tab, Transform _menu = null)
		{
			tab = _tab;
			menu = _menu;
		}
	}
	[Tooltip("Optional, the parent of tabs to add (this, if nothing is selected)")]
	public Transform parent;
	[Tooltip("Optional, Additional tabs")]
	public List<TabMenuS> tabs;
	public Color onColor = Color.white, offColor = new Color(.5f, .5f, .5f, .5f);
	[Tooltip("The actions for the transition")]
	public UnityEvent tabTransition, offTransition;

	public bool getTabsFromChildren = true, getMenusFromTabs, resetIfDisabled = true;
	[HideInInspector]
	public TabMenuS currOnTab;

	TabMenuS settingTab;

	private void Awake()
	{
		if (!parent)
		{
			parent = transform;
		}
		if (getTabsFromChildren)
		{
			foreach (Transform child in parent)
			{
				tabs.Add(new TabMenuS(child));
			}
		}
		if (getMenusFromTabs)
		{
			foreach (TabMenuS tab in tabs)
			{
				tab.menu = tab.tab.GetChild(0);
			}
		}
		currOnTab = null;
	}

	// this is mostly to compensate for the fact that unity ui doesnt have multpiple layers
	// for instance tabbing, you want a tab to be highlighted as if it were selected,
	// but in the tab, you might have another menu, and those buttons should be highlighted while being selected.
	public void ChangeTab(Transform _tab, bool swap = false)
	{
		if (tabs.Count == 0)
		{
			Awake();
		}
		foreach (TabMenuS tab in tabs)
		{
			settingTab = tab;
			if ((_tab == tab.tab || _tab == tab.menu))
			{
				if (currOnTab == tab && swap)
				{
					currOnTab = null;
				}
				else
				{
					currOnTab = tab;
				}
				Transition(currOnTab != null);
			}
			else
			{
				Transition(false);
			}
		}

	}

	public void CloseAllTabs()
	{
		foreach (TabMenuS tab in tabs)
		{
			settingTab = tab;
			Transition(false);
		}
		currOnTab = null;
	}

	public void OnChangeTab(Transform _tab)
	{
		ChangeTab(_tab, false);
	}

	public void OnToggleTab(Transform _tab)
	{
		ChangeTab(_tab, true);
	}

	public void TabColorOnTransition()
	{
		settingTab.tab.GetComponent<UnityEngine.UI.MaskableGraphic>().color = onColor;
	}

	public void TabColorOffTransition()
	{
		settingTab.tab.GetComponent<UnityEngine.UI.MaskableGraphic>().color = offColor;
	}

	public void BorderColorOnTransition()
	{

		settingTab.tab.GetChild(settingTab.tab.childCount - 1).gameObject.SetActive(true);
	}

	public void BorderColorOffTransition()
	{
		settingTab.tab.GetChild(settingTab.tab.childCount - 1).gameObject.SetActive(false);
	}

	public void TurnOnGOTransition()
	{

		settingTab.tab.gameObject.SetActive(true);
	}

	public void TurnOffGOTransition()
	{
		settingTab.tab.gameObject.SetActive(false);
	}

	void Transition(bool isOn)
	{
		if (isOn)
		{
			tabTransition?.Invoke();
			if (settingTab.menu)
				settingTab.menu.gameObject.SetActive(true);
		}
		else
		{
			offTransition?.Invoke();
			if (settingTab.menu)
				settingTab.menu.gameObject.SetActive(false);
		}
	}

	public void HideAllOthers()
	{
		if (currOnTab != null && currOnTab.tab)
		{
			foreach (var item in tabs)
			{
				if (currOnTab != item)
				{
					item.tab.gameObject.SetActive(false);
					if (item.menu)
					{
						item.menu.gameObject.SetActive(false);
					}
				}
			}
		}
	}

	public void ResetAll()
	{
		foreach (var item in tabs)
		{
			item.tab.gameObject.SetActive(true);
			settingTab = item;
			Transition(false);

		}
		currOnTab = null;
	}

	private void OnDisable()
	{
		if (resetIfDisabled && currOnTab != null)
		{
			OnToggleTab(currOnTab.tab);
		}
	}

}
