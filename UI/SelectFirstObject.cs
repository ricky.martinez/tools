﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectFirstObject : MonoBehaviour {

	public GameObject primaryObject;

	private void OnEnable()
	{
		if (UnityEngine.EventSystems.EventSystem.current && primaryObject)
		{
			UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(primaryObject);
			primaryObject.GetComponent<UnityEngine.UI.Selectable>().Select();
			primaryObject.GetComponent<UnityEngine.UI.Selectable>().targetGraphic.color = primaryObject.GetComponent<UnityEngine.UI.Selectable>().colors.pressedColor;
		}

	}
}
