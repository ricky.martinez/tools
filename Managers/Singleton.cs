﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class Singleton<Self> : MonoBehaviour
{
	protected static Dictionary<Type, Self> instances = new Dictionary<Type, Self>();

	public static Self instance
	{
		get
		{
			Type singletonType = typeof(Self);
			if (!instances.ContainsKey(singletonType) || instances[singletonType] == null)
			{
				GameObject manager = Resources.Load<GameObject>("Singletons/" + typeof(Self).ToString());
				if (manager)
				{
					GameObject objInstance = Instantiate(manager);
					DontDestroyOnLoad(objInstance);
					return objInstance.GetComponent<Self>();
				}
				else
					Debug.LogError("You forgot to include a prefab with name: " + typeof(Self).ToString() + " in the Resources/Singletons folder");
			}
			return instances[singletonType];
		}
	}

	protected virtual void Awake()
	{
		Type thisType = typeof(Self);
		if (instances.ContainsKey(thisType))
		{
			Debug.Log("attempted to create duplicate singletons");
			DestroyImmediate(gameObject);
		}
		else
		{
			instances[thisType] = GetComponent<Self>();
			DontDestroyOnLoad(gameObject);
		}
	}
}
