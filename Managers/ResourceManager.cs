using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : Singleton<ResourceManager>
{
	public Dictionary<string, Dictionary<int, UnityEngine.Object>> resourceFileToResourceIds;

	protected override void Awake()
	{
		base.Awake();
		resourceFileToResourceIds = new Dictionary<string, Dictionary<int, UnityEngine.Object>>();
	}

	public T GetResourceByFileAndName<T>(string objectName, string nameOfBucket) where T : UnityEngine.Object
    {
		return GetResourceByFileAndId<T>(objectName.GetHashCode(), nameOfBucket);
	}

	public T GetResourceByFileAndId<T>(int objectId, string nameOfBucket) where T : UnityEngine.Object
	{
		if (resourceFileToResourceIds.TryGetValue(nameOfBucket, out Dictionary<int, UnityEngine.Object> dict))
			return (T)dict[objectId];
		resourceFileToResourceIds[nameOfBucket] = GetDictFromResources<T>(nameOfBucket);
		return (T)resourceFileToResourceIds[nameOfBucket][objectId];
	}

	public Dictionary<int, UnityEngine.Object> GetDictFromResources<T>(string bucket) where T : UnityEngine.Object
	{
		var newDict = new Dictionary<int, UnityEngine.Object>();
		foreach (var item in Resources.LoadAll<T> (bucket))
		{
			newDict.Add(item.name.GetHashCode(), item);
		}
		return newDict;
	}
}
