﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// This class is the link between managers that dont destroy, and gameobjects in the individual scenes
/// since the link will be broken normally (i.e. when going to another scene and then coming back)
/// those objects will need an object in every scene, but this is unecessary
/// instead we put this object in every scene
/// all methods in here should be callabel by unity
/// </summary>
public class SingletonAssistant : MonoBehaviour {

	static SingletonAssistant _currInstance;
	public static SingletonAssistant currInstance { get { if (!_currInstance) _currInstance = new GameObject("SinglAss").AddComponent<SingletonAssistant>(); return _currInstance; } }
	public Slider musicSlider, sfxSlider;
	public AudioClip sceneMusic;
	[ConditionalField("sceneMusic", true)]
	public MUSIC_CLIP sceneMusicClip;
	[ParentClass(typeof(TransitionKitDelegate))]
	public ChildClassSelector transtion;

	private void Reset()
	{
		// todo: create settings if not found
	}

	private void Awake()
	{
		_currInstance = this;

	}

	private void Start()
	{
		if (musicSlider)
		{
			musicSlider.value = PlayerPrefs.GetFloat(SAVEKEY.MusicVolume.ToString());
			sfxSlider.value = PlayerPrefs.GetFloat(SAVEKEY.SfxVolume.ToString());
		}
		if (sceneMusic)
			SoundManager.instance.InstantSwitchMusic(sceneMusic);
		else
			SoundManager.instance.InstantSwitchMusic(sceneMusicClip);
	}

	//////////////////////////////////////
	#region methods that will always be useful in every game
	////////////////////////////////////
	public void LoadScene(string sceneName)
	{
		LoadScreenManager.instance.LoadScene(sceneName);
	}

	public void LoadSceneWait(string sceneName)
	{
		LoadScreenManager.instance.hideUntilReleased = true;
		LoadScreenManager.instance.LoadScene(sceneName);
	}

	/// <summary>
	/// use this to use the default transition in the TransitionKit singleton
	/// </summary>
	/// <param name="_scene">an empty name reloads the scene</param>
	public void ChangeSceneDefault(string _scene)
	{
		TransitionKit.TransitionWithDelegate(null, _scene);
	}

	public void ChangeSceneRandomTransition(string _scene)
	{
		TransitionKit.RandomTransition(_scene);
	}

	public void ChangeSceneWithTransition(string _scene)
	{
		TransitionKit.TransitionWithDelegate(transtion.storedInstance, _scene);
	}

	public void FakeSceneTransition(string _scene)
	{
		TransitionKit.FakeTransition(transtion.storedInstance, _scene);
	}

	public void FakeRandomSceneTransition(string _scene)
	{
		TransitionKit.FakeTransition(transtion.storedInstance, _scene);
	}

	public void CallToast(string _text)
	{
		ToastKit.CallToast(_text);
	}

	public void ChangeMusicInstant(MUSIC_CLIP _clip)
	{
		SoundManager.instance.InstantSwitchMusic(_clip);
	}

	public void ChangeMusicInstant(AudioClip _clip)
	{
		SoundManager.instance.InstantSwitchMusic(_clip);
	}

	public void ChangeMusicFade(int _clip)
	{
		SoundManager.instance.FadeMusic((MUSIC_CLIP)_clip, .05f);
	}

	public void SetMusicVol(float _vol)
	{
		SoundManager.instance.SetMusicVol(_vol);
	}

	public void SetSFXVol(float _vol)
	{
		SoundManager.instance.SetSFXVol(_vol);
	}

	public void PlaySFX(AudioClip clip)
	{
		SoundManager.instance.PlaySoundEffect(clip);
	}

	public void PlaySFX(SFX_CLIP clip)
	{
		SoundManager.instance.PlaySoundEffect(clip);
	}

	public static void PrepareVid(UnityEngine.Video.VideoPlayer vPlay)
	{
		if (vPlay)
		{
			vPlay.Prepare();
			vPlay.prepareCompleted += PlayVideo;
		}
	}

	static void PlayVideo(UnityEngine.Video.VideoPlayer _player)
	{
		_player.Play();
	}

	public void ShowAdd()
	{
		CallToast("Ads not set up yet, this one's on us!");
	}

	public void Quit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE || UNITY_ANDROID
		Application.Quit();
		// firgure out webgl
#elif (UNITY_WEBGL)
		// could potentially put our own website  here instead of refreshing the page
    Application.OpenURL("about:blank");
#endif
	}

	static public void Pause()
	{
		Time.timeScale = 0f;
	}

	static public void UnPause()
	{
		Time.timeScale = 1f;
	}
	#endregion




	/////////////////////////////////////
	/// methods that are particular to a game
	/////////////////////////////////////
	public void TryKey()
	{
		
	}



}