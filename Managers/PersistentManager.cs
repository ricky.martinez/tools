﻿using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

public enum SAVEKEY
{
	MusicVolume, SfxVolume, Star, CurrLevel, MaxLevel, Currency, Hearts, Keys, TotalStars, Moves, Matches2, Matches3, PowerUpsUsed, CurrencyGained, DailyStart, Gifts, Daily, TotalAchievements, TotalMatches, TotalCharacters, TotalDailyAchievements, TotalNonDailyAchieve, KeysUsed, PlayerName, Mac, intentTime, resoTime,

	DodgeW, DodgeH, MoveH, Zoom, PosX, PosY, PosZ, RotX, RotY, RotZ, RotW, Islands, Tutorial, Home, HomeLat, HomeLong,

	Total
}
public enum PATH_NAME
{
	PowerUps, Time, DailyTime, ObjectivesProgress, Games, ConnectedPlayers, MacAddresses, DeviceGames, WaterBiome, AviaryBiome, DesertBiome, HighlandsBiome, JungleBiome, AqCreatures, AviaryCreatures, DesertCreatures, HighlCreatures, JungCreatures,
	Total
}

public enum OBJECTIVES
{
	None = -1,
	Total
}

public static class PathExt
{
	public static string ToDatPath(this PATH_NAME _path)
	{
		return '/' + _path.ToString() + ".bytes";
	}
	public static string ToJSONPath(this PATH_NAME _path)
	{
		return '/' + _path.ToString() + ".json";
	}
}
// handles scene change and saving/loading and video play
public static class PersistentManager
{
	/// <summary>
	/// key, spent, leftOver
	/// </summary>
	public static System.Action<SAVEKEY, float, float> spentKey;
	/// <summary>
	/// key, gained, total
	/// </summary>
	public static System.Action<SAVEKEY, float, float> gainedKey;
	/// <summary>
	/// key, spent, leftOver
	/// </summary>
	public static System.Action<string, float, float> spentCommonKey;
	/// <summary>
	/// key, gained, total
	/// </summary>
	public static System.Action<string, float, float> gainedCommonKey;

	static FirebaseFirestore db;
	static List<Action> dbActionQueue = new List<Action>();
	static bool initializing;
	static void InitializeDB()
	{
		Debug.Log("InitializeDB called");
		if (db == null && !initializing)
		{
			initializing = true;
			FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
			{
				if (task.Result == DependencyStatus.Available)
				{
					db = FirebaseFirestore.DefaultInstance;
					Debug.Log("Firebase DB initialized. DOING TASKS: " + dbActionQueue.Count);
					foreach (var action in dbActionQueue)
					{
						action?.Invoke();
					}
				}
				else
				{
					Debug.Log("Dependency check result: " + task.Result);
					Debug.LogError("Firebase dependency error: " + task.Exception);
					//var options = new Firebase.FirebaseOptions
					//{
						
					//	ApiKey = "YOUR_API_KEY",
					//	AppId = "YOUR_APP_ID",
					//	ProjectId = "YOUR_PROJECT_ID",
					//	// Other required fields
					//};
					//Firebase.FirebaseApp app = Firebase.FirebaseApp.Create(options);
				}
			});
		}
	}

	public static void ResetKeys()
	{
		PlayerPrefs.DeleteAll();
		SetSaveKey(SAVEKEY.MusicVolume, 1f);
		SetSaveKey(SAVEKEY.SfxVolume, 1f);
	}

	public static void ResetKeysAndFiles()
	{
		ResetFiles();
		ResetKeys();
	}

	public static void ResetFiles()
	{
		foreach (string file in Directory.GetFiles(Application.persistentDataPath, "*.bytes"))
		{
			File.Delete(file);
		}
	}

	static public void SetSaveKey(SAVEKEY _saveKey, float _val)
	{
		PlayerPrefs.SetFloat(_saveKey.ToString(), _val);
	}

	static public void SaveKeyAddVal(SAVEKEY _saveKey, float _val)
	{
		float currVal = PlayerPrefs.GetFloat(_saveKey.ToString()) + _val;
		PlayerPrefs.SetFloat(_saveKey.ToString(), currVal);

		if (_val < 0f)
		{
			spentKey?.Invoke(_saveKey, _val, currVal);
		}
		else if (_val != 0f)
		{
			gainedKey?.Invoke(_saveKey, _val, currVal);
		}
	}

	static public void SaveKeyAddVal(string _saveKey, float _val)
	{
		float currVal = PlayerPrefs.GetFloat(_saveKey) + _val;
		PlayerPrefs.SetFloat(_saveKey, currVal);

		if (_val < 0)
		{
			spentCommonKey?.Invoke(_saveKey, _val, currVal);
		}
		else if (_val != 0f)
		{
			gainedCommonKey?.Invoke(_saveKey, _val, currVal);
		}
	}

	public static void SaveKeyInc(SAVEKEY saveKey)
	{
		SaveKeyAddVal(saveKey, 1f);
	}

	public static void SaveKeyInc(string saveKey)
	{
		SaveKeyAddVal(saveKey, 1f);
	}

	/// <summary>
	/// Writes new File or overwrite an existing one
	/// </summary>
	/// <param name="_pathName"></param>
	/// <param name="_serializableObject">obviously the object must be serializable or you'll get an error</param>
	public static void SaveBinFile(PATH_NAME _pathName, object _serializableObject)
	{
		SaveBinFile(_pathName.ToDatPath(), _serializableObject);
	}

	/// <summary>
	/// Writes new File or overwrite an existing one
	/// </summary>
	/// <param name="_pathName"></param>
	/// <param name="_serializableObject">obviously the object must be serializable or you'll get an error</param>
	public static void SaveBinFile(string _pathName, object _serializableObject)
	{
		using (FileStream fs = File.Create(Application.persistentDataPath + '/' + _pathName))
		{
			new BinaryFormatter().Serialize(fs, _serializableObject);

		}
	}

	/// <summary>
	/// Writes new File or overwrite an existing one
	/// </summary>
	/// <param name="_pathName"></param>
	/// <param name="_serializableObject">obviously the object must be serializable or you'll get an error</param>
	public static void SaveBinFileStream(PATH_NAME _pathName, object _serializableObject)
	{
		SaveBinFileStream(_pathName.ToDatPath(), _serializableObject);
	}

	/// <summary>
	/// Writes new File or overwrite an existing one
	/// </summary>
	/// <param name="_pathName"></param>
	/// <param name="_serializableObject">obviously the object must be serializable or you'll get an error</param>
	public static void SaveBinFileStream(string _pathName, object _serializableObject)
	{
#if UNITY_EDITOR
		using (FileStream fs = File.Create(Application.streamingAssetsPath + '/' + _pathName))
		{
			new BinaryFormatter().Serialize(fs, _serializableObject);

		}
#endif
	}

	public static void DeleteFile(string _RelPathName)
	{
		File.Delete(Application.persistentDataPath + _RelPathName);
	}

	/// <summary>
	/// Load only existing files or make a new one
	/// </summary>
	/// <param name="_pathName"></param>
	/// <returns> null if the file doesnt exist</returns>
	public static object LoadBinFile(PATH_NAME _pathName)
	{
		return LoadBinFile(_pathName.ToDatPath());
	}

	/// <summary>
	/// Load only existing files or make a new one
	/// </summary>
	/// <param name="_relativePathName"></param>
	/// <returns> null if the file doesnt exist</returns>
	public static object LoadBinFile(string _relativePathName)
	{
		return LoadBin(Application.persistentDataPath + '/' + _relativePathName);
	}
	/// <summary>
	/// Load only existing files or make a new one
	/// </summary>
	/// <param name="_relativePathName"></param>
	/// <returns> null if the file doesnt exist</returns>
	public static object LoadBinFileStream(string _relativePathName)
	{
		return LoadBin(Application.streamingAssetsPath + '/' + _relativePathName);
	}

	public static object LoadBinFileStream(PATH_NAME _relativePathName)
	{
		return LoadBinFileStream(_relativePathName.ToDatPath());
	}

	static object LoadBin(string fullPath)
	{
		using (FileStream fs = File.Open(fullPath, FileMode.OpenOrCreate))
		{
			return new BinaryFormatter().Deserialize(fs);
		}
	}

	public static void SaveToDataBase(string collection, Dictionary<string, object> jsonMap)
	{
		Action todo = () =>
		{
			CollectionReference collectionRef = db.Collection(collection);
			collectionRef.AddAsync(jsonMap).ContinueWithOnMainThread(task =>
			{
				if (task.IsCompleted)
				{
					Debug.Log("Document added successfully with auto-generated ID!");
				}
				else
				{
					Debug.LogError("Error adding document: " + task.Exception);
				}
			});
		};
		if (db == null)
		{
			dbActionQueue.Add(todo);
			InitializeDB();
		}
		else
		{
			todo.Invoke();
		}

	}
	class snapShotTimer
	{
		public QuerySnapshot cachedSnapshot;
		public float cacheTime;
	}
	static Dictionary<string, snapShotTimer> collectionSnapShot = new Dictionary<string, snapShotTimer>();
	private static float cacheDuration = 360f; // Cache data for 10 seconds
	static public void DoWithLatInRange(string collection, double minLat, double minLong, double maxLat, double maxLong, Action<Task<QuerySnapshot>> task, int limit = 100)
	{

		if (collectionSnapShot.TryGetValue(collection, out snapShotTimer timer) && Time.time - timer.cacheTime < cacheDuration)
		{
			Debug.Log("✅ Using cached Firestore data.");
			task.Invoke(Task.FromResult(timer.cachedSnapshot));
			return;
		}
		Action todo = () =>
		{
			db.Collection(collection)
			.WhereGreaterThanOrEqualTo("lat", minLat)  // Lower bound
			.WhereGreaterThanOrEqualTo("long", minLong)  // Lower bound
			.WhereLessThanOrEqualTo("lat", maxLat)   // Upper bound
			.WhereLessThanOrEqualTo("long", maxLong)   // Upper bound
			.Limit(limit)
			.GetSnapshotAsync().ContinueWithOnMainThread(t =>
			{
				if (t.IsCompleted)
				{
					collectionSnapShot[collection] = new snapShotTimer { cachedSnapshot = t.Result, cacheTime = Time.timeScale }; // Store the result
				}
				task.Invoke(t);
			});
		};
		if (db == null)
		{
			dbActionQueue.Add(todo);
			InitializeDB();
		}
		else
		{
			todo.Invoke();
		}
	}

	static public async Task<QuerySnapshot> GetDocumentsWithLatInRange(string collection, float minLat, float minLong, float maxLat, float maxLong, int limit = 50)
	{
		QuerySnapshot snapshot = await db.Collection(collection)
			.WhereGreaterThanOrEqualTo("lat", minLat)  // Lower bound
			.WhereGreaterThanOrEqualTo("long", minLong)  // Lower bound
			.WhereLessThanOrEqualTo("lat", maxLat)   // Upper bound
			.WhereLessThanOrEqualTo("long", maxLong)   // Upper bound
			.Limit(limit)
			.GetSnapshotAsync();
		return snapshot;
	}

	static public void LogAllFields()
	{
		db.Collection("CreatureTransformations").Document("cryGyb2owUyPpfmnHXLL").GetSnapshotAsync().ContinueWithOnMainThread(task =>
		{
			if (task.IsCompleted)
			{
				DocumentSnapshot snapshot = task.Result;
				foreach (string document in snapshot.GetValue<List<string>>("Trans"))
				{
					Debug.Log(document);
				}
			}
			else
			{
				Debug.LogError("Error retrieving documents: " + task.Exception);
			}
		});
	}


	public static void AddTransformation()
	{

		CollectionReference collectionRef = db.Collection("CreatureTransformations");
		Dictionary<string, object> data = new Dictionary<string, object>
			{
				{ "dustCost", 22 },
				{ "id", 1 },
				{ "specialCost", 5 }
			};

		collectionRef.AddAsync(data).ContinueWithOnMainThread(task =>
		{
			if (task.IsCompleted)
			{
				Debug.Log("Document added successfully with auto-generated ID!");
			}
			else
			{
				Debug.LogError("Error adding document: " + task.Exception);
			}
		});
	}

	public static void GetAllTransforms()
	{
		db.Collection("CreatureTransformations").GetSnapshotAsync().ContinueWithOnMainThread(task =>
		{
			if (task.IsCompleted)
			{
				QuerySnapshot snapshot = task.Result;
				foreach (DocumentSnapshot document in snapshot.Documents)
				{
					string documentID = document.Id;
					string name = document.GetValue<string>("name");
					Debug.Log("Document ID: " + documentID);
				}
			}
			else
			{
				Debug.LogError("Error getting documents: " + task.Exception);
			}
		});
	}
}
