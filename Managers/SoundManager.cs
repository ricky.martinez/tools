﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public enum MUSIC_CLIP { None = -1, Start, Map, Win, Lose, Pause, StartLevel, Level1, Level2,
	Total
}

public enum SFX_CLIP { Buy, Win, Lose, Die, Gain, Subtract, Hover, Press, Attack,
	Total
}

public class SoundManager : Singleton<SoundManager>
{
	public static AudioClip currClip;

	///////   not static for drag and dropping
	public AudioClip[] musicClips;
	public AudioClip[] sfxClips;
	static public AudioMixer mixer;
	static public AudioSource sfx, music, music2;

	private void Reset()
	{
		musicClips = new AudioClip[(int)MUSIC_CLIP.Total];
		sfxClips = new AudioClip[(int)SFX_CLIP.Total];
	}

	protected override void Awake()
	{
		base.Awake();
		mixer = Resources.Load<AudioMixer>("Main");


		List<AudioSource> sources= new List<AudioSource>( GetComponents<AudioSource>());
		while (sources.Count<3)
		{
			sources.Add(gameObject.AddComponent<AudioSource>());
		}
		sfx = sources[0];
		sfx.outputAudioMixerGroup = mixer.FindMatchingGroups("SFX")[0];
		music = sources[1];
		music.outputAudioMixerGroup = mixer.FindMatchingGroups("Music")[0];
		music2 = sources[2];
		music2.outputAudioMixerGroup = mixer.FindMatchingGroups("Music")[0];
	}

	public void SetMusicVol(float _vol)
	{
		mixer.SetFloat("Music", _vol);
		PersistentManager.SetSaveKey(SAVEKEY.MusicVolume, _vol);
	}


	public void SetSFXVol(float _vol)
	{
		mixer.SetFloat("SFX", _vol);
		PersistentManager.SetSaveKey(SAVEKEY.SfxVolume, _vol);
	}

	public AudioSource CurrMusicPlayer()
	{
		return (music.isPlaying ? music : music2);
	}

	//public AudioClip GetSong(MUSIC_CLIP _clip)
	//{
	//	return Resources.Load<AudioClip>("Music/" + _clip.ToString());
	//}

	public void InstantSwitchMusic(MUSIC_CLIP _newclip)
	{
		if(_newclip != MUSIC_CLIP.None)
			InstantSwitchMusic(musicClips[(int)_newclip]);
	}

	public void InstantSwitchMusic(AudioClip _newclip)
	{
		if (_newclip == null || _newclip == currClip)
			return;
		currClip = _newclip;
		AudioSource temp = CurrMusicPlayer();
		temp.Stop();
		temp.clip = currClip;
		temp.Play();
	}

	public void FadeMusic(AudioClip _newClip, float transSpeed = .003f)
	{
		if (_newClip == currClip || _newClip == null)
			return;
		currClip = _newClip;
		StartCoroutine(FadeInAndOut(_newClip, transSpeed));
	}

	public void FadeMusic(MUSIC_CLIP _newclip, float transSpeed = .003f)
	{
		if (_newclip == MUSIC_CLIP.None)
			return;
		FadeMusic(musicClips[(int)_newclip], transSpeed);
	}

	public void PlaySoundEffect(AudioClip _clip)
	{
		sfx.PlayOneShot(_clip);
	}

	public void PlaySoundEffect(SFX_CLIP _clip)
	{
		PlaySoundEffect(sfxClips[(int)_clip]);
	}

	//static public void PlayRandomAttack(AudioClip _clip = null)
	//{
	//	sfxPlayer.PlayOneShot(_clip ? _clip : audioAttacks[UnityEngine.Random.Range(0, audioAttacks.Length)]);
	//}

	//static public void PlayRandomDeath(AudioClip _clip = null)
	//{
	//	if (_clip)
	//	{
	//		sfxPlayer.PlayOneShot(_clip);
	//	}
	//	else
	//	{
	//		AudioClip[] deathSounds = Resources.LoadAll<AudioClip>("Attacks");
	//		sfxPlayer.PlayOneShot(deathSounds[UnityEngine.Random.Range(0, deathSounds.Length)]);
	//	}
	//}

	IEnumerator FadeInAndOut(AudioClip clip, float _speed)
	{
		AudioSource _in, _out;
		if(music.isPlaying){
			_in = music2;
			_out = music;
		}
		else
		{
			_in = music;
			_out = music2;
		}
		_in.clip = clip;
		_in.Play();
		float maxVolume = PlayerPrefs.GetFloat(SAVEKEY.MusicVolume.ToString());
		while (_in.volume < maxVolume)
		{
			_in.volume += _speed;
			_out.volume -= _speed;
			yield return null;
		}
		_in.volume = maxVolume;
		_out.volume = 0f;
		_out.Stop();
		yield break;
	}

}
