using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScreenManager : Singleton<LoadScreenManager>
{
	public GameObject backGround;
	public Slider loadingSlider;
	public Animator anim;
	public bool hideUntilReleased;

	protected override void Awake()
	{
		base.Awake();
		if (backGround == null)
		{
			backGround = new GameObject("loadBack", typeof(Image));
			backGround.transform.SetParent(transform);
			RectTransform rectTransform = backGround.GetComponent<RectTransform>();
			rectTransform.anchorMin = Vector2.zero;   // Bottom-left
			rectTransform.anchorMax = Vector2.one;    // Top-right
			rectTransform.offsetMin = Vector2.zero;   // No offset
			rectTransform.offsetMax = Vector2.zero;   // No offset
			backGround.GetComponent<Image>().color = new Color(0, 0, 0, .5f);
			anim = gameObject.AddComponent<Animator>();

		}
		backGround.SetActive(false);

	}

	public void LoadSceneOverrideAnim(string sceneName, string animPlay, float delay = 0)
	{
		backGround.SetActive(true);
		StartCoroutine(LoadSceneAsync(sceneName, animPlay, delay));
	}

	public void LoadScene(string sceneName, float delay = 0)
	{
		StartCoroutine(LoadSceneAsync(sceneName, delay));
	}

	private IEnumerator LoadSceneAsync(string sceneName, float delay)
	{
		yield return LoadSceneAsync(sceneName, sceneName, delay);
	}

	private IEnumerator LoadSceneAsync(string sceneName, string animState, float delay)
	{
		yield return new WaitForSeconds(delay);
		backGround.SetActive(true);
		anim.Play(animState);

		// Start loading the scene asynchronously
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
		operation.allowSceneActivation = false; // Prevent auto-switch at 90%

		// Update the slider while loading
		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01(operation.progress / 0.9f); // Normalize (0 to 1)
			loadingSlider.value = progress;

			// Scene is almost ready, wait for user input or auto-transition
			if (operation.progress >= 0.9f)
			{
				operation.allowSceneActivation = true; // Load scene when ready
			}

			yield return null;
		}

		while (hideUntilReleased)
		{
			yield return null;
		}
		backGround.SetActive(false);
	}
}
