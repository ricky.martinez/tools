using System.Collections.Generic;
using UnityEngine;

public class LogManager : Singleton<LogManager>
{
	private List<LogMessage> logs = new List<LogMessage>(); // Store logs with log type
	private Vector2 scrollPosition = Vector2.zero;  // Scroll position
	private GUIStyle textStyle;
	private GUIStyle boxStyle;
	private GUIStyle errorStyle;
	private bool showConsole = true; // Toggle visibility

	private struct LogMessage
	{
		public string message;
		public LogType type;
	}

	void OnEnable()
	{
		Application.logMessageReceived += HandleLog;
	}

	void OnDisable()
	{
		Application.logMessageReceived -= HandleLog;
	}

	void HandleLog(string logString, string stackTrace, LogType type)
	{
#if UNITY_EDITOR || DEVELOPMENT_BUILD
		if (type == LogType.Error || type == LogType.Exception)
		{
			logs.Add(new LogMessage { message = logString + "\n" + stackTrace, type = type });
		}
		else
		{
			//logs.Add(new LogMessage { message = logString + "\n" + stackTrace, type = type });
		}
#else
		if (type == LogType.Error || type == LogType.Exception)
		{
			logs.Add(new LogMessage { message = logString, type = type }); // No stack trace in release builds
		}
#endif

		if (logs.Count > 10) logs.RemoveAt(0);
	}

	void OnGUI()
	{
		if (!showConsole) return; // Hide console if toggled off

		if (textStyle == null)
		{
			textStyle = new GUIStyle(GUI.skin.label);
			textStyle.fontSize = 28;
			textStyle.wordWrap = true;
			textStyle.normal.textColor = Color.white;
		}

		if (boxStyle == null)
		{
			boxStyle = new GUIStyle(GUI.skin.box);
			boxStyle.fontSize = 28;
		}

		if (errorStyle == null)
		{
			errorStyle = new GUIStyle(GUI.skin.label);
			errorStyle.fontSize = 28;
			errorStyle.wordWrap = true;
			errorStyle.normal.textColor = Color.red; // Red for errors
		}

		float boxWidth = 600;
		float boxHeight = 300;
		float xPos = 10;
		float yPos = 10;

		// Draw the outer box
		GUI.Box(new Rect(xPos, yPos, boxWidth, boxHeight), "Debug Console", boxStyle);

		// Scroll view setup
		Rect scrollViewRect = new Rect(xPos + 10, yPos + 30, boxWidth - 20, boxHeight - 40);
		Rect contentRect = new Rect(0, 0, boxWidth - 40, logs.Count * 30);

		scrollPosition = GUI.BeginScrollView(scrollViewRect, scrollPosition, contentRect);

		for (int i = 0; i < logs.Count; i++)
		{
			GUIStyle style = logs[i].type == LogType.Error || logs[i].type == LogType.Exception ? errorStyle : textStyle;
			GUI.Label(new Rect(10, i * 30, boxWidth - 50, 30), logs[i].message, style);
		}

		GUI.EndScrollView();

		// Toggle button
		if (GUI.Button(new Rect(boxWidth - 50, yPos, 40, 25), "X"))
		{
			showConsole = false; // Hide console
		}
	}
}
