using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using UnityEngine.SceneManagement;
using Unity.Collections;
using Unity.Burst.CompilerServices;

public class SpawnManager : MonoBehaviour
{
	public bool isOverWorld;

	public class ExpiringCreature
	{
		public string id;
		public double expireSec;
		public MeshRenderer instance;
	}

	[System.Serializable]
	public class SavedCreatures
	{
		public string id;
		public double expireSec;
		public Vector2Ser pos;
	}

	public List<ExpiringCreature> expiringCreatures;
	public LayerMask ground;
	public MeshRenderer groundRend;
	public Terrain terrain;

	public float offset = 1.5f;
	public int maxCount = 50;
	double serverTime;
	float timer;

	void Start()
	{
		serverTime = Tools.GetNetMillisSinceGoogleEpoch()/1000;
		expiringCreatures = new List<ExpiringCreature>(maxCount);
		SpawnOldTimers();
	}

	private void Update()
	{
		MaintainMaxCreaturesTimers();
	}

	private void MaintainMaxCreaturesTimers()
	{
		if (timer < Time.time)
		{
			timer = Time.time + 1f;
			int i = -1; while (++i != expiringCreatures.Count)
			{
				var remainingSec = expiringCreatures[i].expireSec - (serverTime + Time.timeSinceLevelLoadAsDouble);// works bc we get the servertime when we load the scene
				if (remainingSec < 0|| expiringCreatures[i].instance == null)
				{
					Destroy(expiringCreatures[i].instance.transform.parent.gameObject);
					expiringCreatures.RemoveAtSwapBack(i);
					--i;
					continue;
				}

				TextToTexture.ChangeTextOfRender(expiringCreatures[i].instance, FormatTime((int)remainingSec));// display text as min:sec
			}
		}

		if (expiringCreatures.Count < maxCount)
		{
			SpawnObject();
		}
	}

	private string FormatTime(int totalSeconds)
	{
		return $"{totalSeconds / 60}:{totalSeconds%60:D2}"; // Ensures two-digit seconds
	}

	private void SpawnOldTimers()
	{
		Ray ray = new Ray();
		ray.direction = Vector3.down;
		RaycastHit hit;
		foreach (var item in GetSavedSceneScreatures())
		{
			if (item.expireSec > serverTime + Time.timeSinceLevelLoadAsDouble)
			{
				var creaData = CreatureManager.instance.GetSpawnableCreature(item.id);
				var newCreature = creaData.GetDuplicateObject();
				newCreature.transform.localPosition = ray.origin = new Vector3(item.pos.x, 500, item.pos.y);
				expiringCreatures.Add(new ExpiringCreature { expireSec = item.expireSec, id = item.id, instance = GetTextRenderFromCreature(newCreature) }) ;
				if (Physics.Raycast(ray, out hit, 1500, ground))
				{
					newCreature.transform.localPosition = hit.point + Vector3.up * offset;
				}
			}
		}
		LoadScreenManager.instance.hideUntilReleased = false;

	}

	MeshRenderer GetTextRenderFromCreature(GameObject parent)
	{
		return parent.transform.Find("Text").GetComponent<MeshRenderer>();
	}

	SavedCreatures[] GetSavedSceneScreatures()
	{
		try
		{
			return (SavedCreatures[])PersistentManager.LoadBinFile(SceneManager.GetActiveScene().name + ".dat");
		}
		catch (System.Exception)
		{
			PersistentManager.SaveBinFile(SceneManager.GetActiveScene().name + ".dat", new SavedCreatures[0]);
			return new SavedCreatures[0];
		}
	}

	void AddCustomCreatures()
	{
		//objectsToSpawn.AddRange()
	}

	void SpawnObject()
	{
		var bestCreature = CreatureManager.instance.GetBestCreatureSpawn(UnityEngine.Random.value);
		if (bestCreature == null)
			return;
		Vector3 spawnPosition = new Vector3();
		if (terrain)
		{
			// Get terrain boundaries
			Vector3 terrainPosition = terrain.transform.localPosition;
			float terrainWidth = terrain.terrainData.size.x;
			float terrainLength = terrain.terrainData.size.z;

			// Get random X and Z within terrain bounds
			float randomX = Random.Range(terrainPosition.x, terrainPosition.x + terrainWidth);
			float randomZ = Random.Range(terrainPosition.z, terrainPosition.z + terrainLength);

			// Get terrain height at that point
			float terrainY = terrain.SampleHeight(new Vector3(randomX, 0, randomZ)) + terrainPosition.y + offset;

			// Spawn the object at the computed position
			spawnPosition = new Vector3(randomX, terrainY, randomZ);
		}
		else if (groundRend)
		{
			// Get plane boundaries
			Vector3 planeSize = groundRend.bounds.size;
			Vector3 planePosition = groundRend.transform.position;

			float minX = planePosition.x - planeSize.x / 2;
			float maxX = planePosition.x + planeSize.x / 2;
			float minZ = planePosition.z - planeSize.z / 2;
			float maxZ = planePosition.z + planeSize.z / 2;
			float randomX = Random.Range(minX, maxX);
			float randomZ = Random.Range(minZ, maxZ);

			spawnPosition = new Vector3(randomX, 500, randomZ);
			if (Physics.Raycast(spawnPosition, Vector3.down, out RaycastHit hit, 1500, ground))
			{
				spawnPosition = hit.point + Vector3.up * offset;
			}

		}
		var newCreature = bestCreature.GetDuplicateObject();
		newCreature.transform.localPosition = spawnPosition;
		expiringCreatures.Add(new ExpiringCreature { expireSec = Time.timeSinceLevelLoadAsDouble + serverTime + 300 + Tools.Map(expiringCreatures.Count, 0,maxCount, 0, 1500), id = bestCreature.uniqueId, instance = GetTextRenderFromCreature(newCreature) });
	}

	void SaveGameData()
	{
		SavedCreatures[] savedCreatures = new SavedCreatures[expiringCreatures.Count];
		int i = -1;while (++i!= savedCreatures.Length)
		{
			if(expiringCreatures[i].instance)
				savedCreatures[i] = new SavedCreatures { id = expiringCreatures[i].id, pos = Tools.V2FromXZ(expiringCreatures[i].instance.transform.parent.position), expireSec = expiringCreatures[i].expireSec };

		}
		PersistentManager.SaveBinFile(SceneManager.GetActiveScene().name + ".dat", savedCreatures);
	}

	void OnApplicationPause(bool paused)
	{
		if (paused)
		{
			SaveGameData(); // Ensure data is saved when app is paused
		}
	}

	void OnDestroy()
	{
		SaveGameData(); // Called if Unity is shutting down (not always on Android)
	}
}
