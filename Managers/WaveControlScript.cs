﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class WaveControlScript : MonoBehaviour {
    enum TRACKERTYPE {
        LeftAndRight,
        Arrow,
        AreaFlash
    }
    [SerializeField]
    TRACKERTYPE trackingType;

    //the different types of things to be spawned
    [SerializeField]
    GameObject[] hazards, bosses, indicators;

    //sliders needed for wave counter and timer
    [SerializeField]
    UnityEngine.UI.Slider waveCountSlider, waveTimerSlider;

    [SerializeField]
    AudioClip nextWave, spin;

    //the chance of having a certain enemy spawn more, size of each wave and the number of waves
    [SerializeField]
    uint[] spawnRatios, waveSizes;

    //the number of pie slices
    [SerializeField]
    uint spawnPositions;

    [SerializeField]
    float startWait, waveWait, minSpawnWait, maxSpawnWait, minRadius, maxRadius;

    //set to true if they can land inside the raduis
    [SerializeField]
    bool randomAngle, oneBossAtATime;
    static public bool waitingWave;

    //game specific variables

    void Start () {
        waitingWave = false;
        waveCountSlider.maxValue = hazards.Length;
        Vector2 counterWidth = ((RectTransform)waveCountSlider.transform).sizeDelta;
        counterWidth.x *= hazards.Length;
        ( ( RectTransform )waveCountSlider.transform ).sizeDelta = counterWidth;
        
        waveTimerSlider.GetComponent<Animator> ().speed = 1/ waveWait;
        StartCoroutine ( SpawnWaves () );
    }

    //Game Specific function (only delete body)
    void BossWaiting () {
        waveTimerSlider.value = 1;
        waveTimerSlider.GetComponent<Animator> ().enabled = true;
    }

    //Game specific function (only delete body)
    void BossIncoming () {
        SoundManager.instance.PlaySoundEffect ( spin );
    }

    IEnumerator SpawnWaves () {
		yield break;
    }

    public void ContinueFlash ( Transform _dir ) {
        //foreach ( EnemyAI enemy in enemies ) {
        //    Vector3 camdir = Camera.main.WorldToScreenPoint(enemy.transform.position);
        //    if ( camdir.y < 0 || camdir.x < 0 || camdir.x > Screen.width )
        //        continue;
        //    else
        //        return;
        //}

        SelectFlash ( _dir );
    }

    void SelectFlash ( Transform _dir ) {
        GameObject indicator = null;
        switch ( trackingType ) {
            case TRACKERTYPE.LeftAndRight:
                if ( Vector3.Dot ( Camera.main.transform.right, ( _dir.position - Camera.main.transform.position ) ) > 0 )
                    indicator = indicators[1];//right
                else
                    indicator = indicators[0];//left
                break;
            case TRACKERTYPE.Arrow:
                indicator = indicators[0];
                indicator.transform.LookAt ( _dir );
                break;
            case TRACKERTYPE.AreaFlash:
                //to be finished
                //for top down view
                break;
        }
        StartCoroutine ( IndicatorFlash ( indicator, _dir ) );
    }

    IEnumerator IndicatorFlash ( GameObject _indicator, Transform _dir ) {
        _indicator.SetActive(true);
        yield return new WaitForSeconds ( .4f );
        _indicator.SetActive(false);
        yield return new WaitForSeconds ( .8f );
        ContinueFlash ( _dir );
    }
}