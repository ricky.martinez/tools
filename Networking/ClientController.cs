﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System.Net;
using System;

public class ClientController : MonoBehaviour
{
	const string serverIp = "68.207.237.51";
	public Networking.ClientSocket serverSocket;
	public Networking.Socket otherPhoneSocket;
	public UnityEngine.UI.Text txt;
	List<Vector3> opponentPositions;

	private void Start()
	{
	}
	// Use this for initialization
	public void ConnectToServer()
	{
		//NetworkTransport.Init();
		//serverSocket = new Networking.ClientSocket();
		//Networking.CreateSocket(16, 0, serverSocket, QosType.Reliable);

		//NetworkError error = serverSocket.Connect(serverIp, 2499);
		//CreateChatBox(error.ToString());
		////transform.Find("Scroll View").gameObject.SetActive(true);
		//GameObject.Find("ConnectButton").gameObject.SetActive(false);
		////transform.Find("NameInput").gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
		//if ((serverSocket != null && serverSocket.connected) || (otherPhoneSocket != null))
		//{
		//	int recHostId;
		//	int connectionId;
		//	int channelId;
		//	byte[] recBuffer = new byte[1024];
		//	int dataSize;
		//	byte error;
		//	NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, 1024, out dataSize, out error);
		//	if (error != 0)
		//	{
		//		Debug.Log(((NetworkError)error).ToString());

		//	}
		//	switch (recData)
		//	{
		//		case NetworkEventType.ConnectEvent:

		//			//((Networking.ServerSocket)otherPhoneSocket).clients[connectionId] = "player2";
		//			txt.gameObject.SetActive(true);
		//			txt.text = "Incoming Opponent";
		//			break;
		//		case NetworkEventType.DataEvent:       //3
		//			string[] msg = Networking.GetString(recBuffer, dataSize);

		//			switch (msg[0])
		//			{
		//				case "CNN":
		//					// there's already a host, connect to it
		//					Invoke("SlowDisconnect", 1f);
		//					otherPhoneSocket = new Networking.ClientSocket();
		//					Networking.CreateSocket(4, 0, otherPhoneSocket, QosType.Reliable);
		//					NetworkError netError = ((Networking.ClientSocket)otherPhoneSocket).Connect(msg[1], 2498);
		//					if (netError == NetworkError.Ok)
		//					{
		//						txt.gameObject.SetActive(true);
		//						txt.text = "Connected";
		//						CreateChatBox(((Networking.ClientSocket)otherPhoneSocket).connectionId.ToString());
		//					}
		//					else
		//					{
		//						txt.gameObject.SetActive(true);
		//						txt.text = netError.ToString();
		//					}

		//					break;
		//				case "Host":
		//					otherPhoneSocket = new Networking.ServerSocket();
		//					Networking.CreateSocket(4, 2498, otherPhoneSocket, QosType.Reliable);
		//					((Networking.ServerSocket)otherPhoneSocket).started = true;
		//					IPAddress[] ips = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList;
		//					serverSocket.SendMessage("N|" + ips[ips.Length - 1], QosType.Reliable);
		//					CreateChatBox("waiting on:" + ips[ips.Length - 1].ToString());
		//					Invoke("SlowDisconnect", 1f);
		//					break;
		//				case "V":
		//					// in coming vectors
		//					if (opponentPositions == null)
		//					{
		//						opponentPositions = new List<Vector3>();
		//					}
		//					int startIndex = 2;
		//					Vector3 newVec;
		//					newVec.z = 0f;
		//					while (startIndex != dataSize)
		//					{
		//						newVec.x = BitConverter.ToSingle(recBuffer, startIndex);
		//						startIndex += 4;
		//						newVec.y = BitConverter.ToSingle(recBuffer, startIndex);
		//						startIndex += 4;
		//						opponentPositions.Add(newVec);
		//					}

		//					break;
		//				case "R":
		//					// enemy is ready
		//					break;
		//				default:
		//					//CreateChatBox(msg[0]);
		//					break;
		//			}
		//			break;
		//		case NetworkEventType.DisconnectEvent: //4
		//			if (connectionId == serverSocket.connectionId)
		//			{
		//				//CreateChatBox("Dicnonnected");
		//				serverSocket.connected = false;
		//			}

		//			break;
		//		default:
		//			break;
		//	}
		//}
	}

	public void SendChat(UnityEngine.UI.InputField _input)
	{
		if (_input.text != string.Empty)
		{
			//serverSocket.SendMessage(_input.text, QosType.Reliable);
			//CreateChatBox(_input.text);
			_input.text = string.Empty;
		}
	}

	public void SendVerts(List<Vector3> _positions)
	{
		// can only send 1 kb (1024) bytes at a time on most routers/devices (some block partial packets).
		// we only receive 1024 bytes anyway
		// if we send a message that is larger, it will be put into multiple packets
		// the beginning of the packet will not be the signature we sent with the first packet
		// to get around this, we make sure that we send multiple messages that are no larger than 1024
		// we need 2 for the signature 'V|' . this means we have 1022
		// 4 bytes in a float, 2 floats per vector2 = 8
		// 1022/8 = 127
		List<Vector3> leftOvers = null;
		if (_positions.Count > 127)//   1024/8 -2
		{
			int count = _positions.Count - 127;
			leftOvers = new List<Vector3>(_positions.GetRange(126, count));
			_positions.RemoveRange(127, count);
		}
		byte[] vectorBytes = new byte[2 + _positions.Count * 8];// not necessarily 127
		System.Text.Encoding.Unicode.GetBytes("V|").CopyTo(vectorBytes, 0);
		Vector3sToBytes(_positions, vectorBytes, 2);
		//((Networking.ServerSocket)otherPhoneSocket).SendMessage(vectorBytes, 1, QosType.Reliable);
		if (leftOvers != null)
		{
			SendVerts(leftOvers);
		}

	}

	public void SendVertsClient(List<Vector3> _positions)
	{
		// can only send 1 kb (1024) bytes at a time on most routers/devices (some block partial packets).
		// we only receive 1024 bytes anyway
		// if we send a message that is larger, it will be put into multiple packets
		// the beginning of the packet will not be the signature we sent with the first packet
		// to get around this, we make sure that we send multiple messages that are no larger than 1024
		// we need 2 for the signature 'V|' . this means we have 1022
		// 4 bytes in a float, 2 floats per vector2 = 8
		// 1022/8 = 127
		List<Vector3> leftOvers = null;
		if (_positions.Count > 127)//   1024/8 -2
		{
			int count = _positions.Count - 127;
			leftOvers = new List<Vector3>(_positions.GetRange(126, count));
			_positions.RemoveRange(127, count);
		}
		byte[] vectorBytes = new byte[2 + _positions.Count * 8];// not necessarily 127
		System.Text.Encoding.Unicode.GetBytes("V|").CopyTo(vectorBytes, 0);
		Vector3sToBytes(_positions, vectorBytes, 2);
		//((Networking.ClientSocket)otherPhoneSocket).SendMessage(vectorBytes, QosType.Reliable);
		if (leftOvers != null)
		{
			SendVertsClient(leftOvers);
		}
	}

	void Vector3sToBytes(List<Vector3> _pos, byte[] _array, int _offset)
	{
		byte[] copyBytes;
		foreach (Vector3 vec in _pos)
		{
			copyBytes = BitConverter.GetBytes(vec.x);
			copyBytes.CopyTo(_array, _offset);
			_offset += 4;
			copyBytes = BitConverter.GetBytes(vec.y);
			copyBytes.CopyTo(_array, _offset);
			_offset += 4;
		}
	}

	static public void CreateChatBox(string _msg)
	{
		GameObject temp = Instantiate(Resources.Load<GameObject>("ChatBox"), GameObject.Find("Content").transform);
		UnityEngine.UI.Text chatText = temp.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>();
		chatText.text = System.DateTime.Now + " - " + _msg;
		Canvas.ForceUpdateCanvases();
		temp.GetComponent<UnityEngine.UI.LayoutElement>().preferredHeight = 30 * chatText.cachedTextGenerator.lineCount;
		Canvas.ForceUpdateCanvases();
		GameObject slideBar = GameObject.Find("Scrollbar Vertical");
		if (slideBar)
		{
			slideBar.GetComponent<UnityEngine.UI.Scrollbar>().value = 0;
		}
	}

	void SlowDisconnect()
	{
		//serverSocket.Disconnect();
		serverSocket = null;
	}
}
