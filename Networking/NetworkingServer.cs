﻿using UnityEngine.Networking;
using UnityEngine;
using System.Net;
using UnityEngine.UI;

public class NetworkingServer : MonoBehaviour
{
	Networking.ServerSocket socket;

	bool waitingForHost = true;
	string ipForClient;

	// Use this for initialization
	void Start()
	{
		//NetworkTransport.Init();
		socket = new Networking.ServerSocket();
		//Networking.CreateSocket(16, 2499, socket, QosType.Reliable);
		//socket.started = true;

		IPAddress[] ips = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList;
		ClientController.CreateChatBox(ips[ips.Length - 1].ToString());
		ClientController.CreateChatBox(ips[ips.Length - 2].ToString());
		ClientController.CreateChatBox(ips[ips.Length - 3].ToString());
		ClientController.CreateChatBox(ips[ips.Length - 4].ToString());
	}

	private void Update()
	{
		//if (socket.started)
		//{
		//	int recHostId;
		//	int connectionId;
		//	int channelId;
		//	byte[] recBuffer = new byte[1024];
		//	int dataSize;
		//	byte error;
		//	NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, 1024, out dataSize, out error);
		//	if (error != 0)
		//	{

		//		Debug.Log(((NetworkError)error).ToString());
		//	}
		//	switch (recData)
		//	{
		//		case NetworkEventType.Nothing:         //1
		//											   //Debug.Log("receiving nothing");
		//			break;

		//		case NetworkEventType.ConnectEvent:    //2
		//			ClientController.CreateChatBox("Client: " + connectionId + " connected");
		//			if (waitingForHost)
		//			{
		//				// tell client to create a host
		//				socket.SendMessage("Host", connectionId, QosType.Reliable);
		//			}
		//			else
		//			{
		//				// tell client to connect to another host
		//				socket.clients[connectionId] = "NA";
		//				socket.SendMessage("CNN|" + ipForClient, connectionId, QosType.Reliable);
		//			}
		//			waitingForHost = !waitingForHost;
		//			break;

		//		case NetworkEventType.DataEvent:       //3
		//			string[] msg = Networking.GetString(recBuffer, dataSize);
		//			switch (msg[0])
		//			{
		//				case "N":
		//					ClientController.CreateChatBox(connectionId + " Is Hosting");
		//					ipForClient = msg[1];

		//					//ClientController.CreateChatBox(msg[1] + " Joined the room");
		//					//socket.SendToAll(msg[1] + " Joined the room", QosType.Reliable, connectionId);
		//					break;
		//				default:
		//					ClientController.CreateChatBox(msg[0]);
		//					socket.SendToAll(msg[0], QosType.Reliable, connectionId);
		//					break;
		//			}
		//			break;

		//		case NetworkEventType.DisconnectEvent: //4
		//			ClientController.CreateChatBox(connectionId + " Left the room");
		//			//socket.SendToAll(socket.clients[connectionId] + " Left the room", QosType.Reliable, connectionId);
		//			//if (socket.clients.ContainsKey(connectionId))
		//			//{
		//			//	socket.clients.Remove(connectionId);
		//			//}
		//			break;
		//	}
		//}
	}

	public void SendToAll(InputField _txt)
	{
		if (_txt.text != string.Empty)
		{
			//socket.SendToAll(_txt.text, QosType.Reliable);
		}
	}
}
