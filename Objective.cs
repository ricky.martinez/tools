﻿using System.Runtime.InteropServices;

public delegate void AnyFunction(object _param);
public delegate void ObjectiveFunction(Objective _param);
public delegate void AllFunction();
public delegate bool ConditionAutoDelg(object _first, params object[] _second);

[UnityEngine.CreateAssetMenu(fileName ="Objectives", menuName ="New Objective")]
public abstract class ICondition : UnityEngine.ScriptableObject
{
	abstract public bool MeetCondition(AnyObject[] _first, params AnyObject[] _second);
}

[System.Serializable]
public struct AnyObject
{
	public float _float;
	public UnityEngine.Object _object;

	public AnyObject(UnityEngine.Object _obj)
	{
		_float = 0;
		_object = _obj;
	}

	public AnyObject(float _float)
	{
		_object = null;
		this._float = _float;
	}
}

[System.Serializable]
public class Comparison
{
	
	public ICondition condToMeet;
	public AnyObject[] stored;
	public Comparison(ICondition _condition, params UnityEngine.Object[] _objs)
	{
		int i = _objs.Length;
		stored = new AnyObject[i];
		while (--i!=-1)
		{
			stored[i]._object = _objs[i];
		}
		condToMeet = _condition;
	}

	public Comparison(ICondition _condition, params float[] _objs)
	{
		int i = _objs.Length;
		stored = new AnyObject[i];
		while (--i != -1)
		{
			stored[i]._float = _objs[i];
		}
		condToMeet = _condition;
	}

	public bool CheckCondition(params AnyObject[] _objs)
	{
		return condToMeet.MeetCondition(stored, _objs);
	}
}

[System.Serializable]
public class AutoCondition
{
	public ConditionAutoDelg conditionMet;
	public object first;
	public object[] second;

	public AutoCondition(ConditionAutoDelg conditionMet, object first, params object[] second)
	{
		this.conditionMet = conditionMet;
		this.first = first;
		this.second = second;
	}
}

public class Objective : UnityEngine.MonoBehaviour
{
	public Comparison[] conditions;
	public ObjectiveFunction conditionsMet;

	public void CheckCondition(params AnyObject[] _values)
	{
		bool allmet = true;
		foreach (Comparison condition in conditions)
		{
			if (condition.CheckCondition(_values))
			{
				continue;
			}
			allmet = false;
		}
		if (allmet)
		{
			conditionsMet?.Invoke(this);
		}
	}

	public virtual void OnDestroy()
	{
		conditionsMet = null;
	}
}