﻿using UnityEngine;
using System.Collections;
#pragma warning disable 0649

public class CameraMovement : MonoBehaviour {
	enum CURSORTYPE {
		Raycast,
		XandZMove,
		FollowMouse
	}
	[SerializeField]
	CURSORTYPE cursorType;
	enum ZOOMTYPE {
		UpAndDown,
		InAndOut
	}
	[SerializeField]
	ZOOMTYPE zoomType;
	enum FOLLOWTYPE {
		Edge,
		Lirp,
		Rotation,
		ClickAndDrag
	}
	[SerializeField]
	FOLLOWTYPE followType;
	[SerializeField]
	float whlZmSpd, scrollSpeedX, scrollSpeedY, rotationSpeed, wScrlSpcRight, wScrlSpcLeft, wScrlSpcTop, wScrlSpcBtm, cursorSpd, lirpSpd, dragSpeed;
	[SerializeField]
	Transform cursor, target, cameraPlane;
	//might be a TerrainCollider
	[SerializeField]
	MeshCollider terrainCollider;
	[SerializeField]
	Texture2D mousePic;
	Vector3 dir,cursorDir, cursorScrnPos, startPos, deltaPos;
	Quaternion startAngle;

	void Start () {
		startAngle = Camera.main.transform.rotation;
		wScrlSpcLeft *= Screen.width;
		wScrlSpcRight = Screen.width * ( 1f - wScrlSpcRight );
		wScrlSpcTop = Screen.height * ( 1f - wScrlSpcTop );
		wScrlSpcBtm *= Screen.height;
		if(cursorType == CURSORTYPE.FollowMouse)
			Cursor.SetCursor (mousePic,Vector2.zero,CursorMode.Auto);
		//Cursor.visible = false;
		//Cursor.lockState = CursorLockMode.Locked;
	}

	void FixedUpdate () {
	}

	void Update () {
		dir = Vector3.zero;
		switch ( zoomType ) {
			case ZOOMTYPE.UpAndDown:
				dir.y = Input.GetAxisRaw ( "WheelZoom" ) * scrollSpeedY * 10f;
				break;
			case ZOOMTYPE.InAndOut:
				Camera.main.fieldOfView -= Input.GetAxis ( "WheelZoom" ) * whlZmSpd;
				break;
			default:
				break;
		}
		if ( Input.GetButton ( "RotateButton" ) )
			//if you have a 3d cursor object make it a child of this object
			transform.Rotate ( transform.up, Input.GetAxis ( "MouseX" ) * rotationSpeed );
		else {
			transform.Rotate ( transform.up, Input.GetAxis ( "Rotation" ) * rotationSpeed );
			switch ( cursorType ) {
				case CURSORTYPE.Raycast:
					//this requires a terrain collider, and you may need a layer mask to avoid certain objects
					RaycastHit hitOut;
					terrainCollider.Raycast ( Camera.main.ScreenPointToRay ( Input.mousePosition ), out hitOut, 1000f );
					cursor.position = hitOut.point;
					break;
				case CURSORTYPE.XandZMove:
					cursorDir = ( Input.GetAxis ( "MouseX" ) * transform.right + transform.forward * Input.GetAxis ( "MouseY" ) ) * cursorSpd;
					cursor.position += cursorDir;
					//boundaries: make colliders on the edge of the camera or...
					break;
				case CURSORTYPE.FollowMouse:
					//this means the cursor is a ui object
					//this is taking care of in the start function
					break;
				default:
					break;
			}
		}
		switch ( followType ) {
			case FOLLOWTYPE.Edge:
				cursorScrnPos = CURSORTYPE.FollowMouse != cursorType ? Camera.main.WorldToScreenPoint ( cursor.position ) : Input.mousePosition;
				if ( cursorScrnPos.x < wScrlSpcLeft )
					dir.x = ( cursorScrnPos.x - wScrlSpcLeft ) * scrollSpeedX;
				else if ( cursorScrnPos.x > wScrlSpcRight )
					dir.x = ( cursorScrnPos.x - wScrlSpcRight ) * scrollSpeedX;
				if ( cursorScrnPos.y < wScrlSpcBtm )
					//change to dir.z for 3d
					dir.y = ( cursorScrnPos.y - wScrlSpcBtm ) * scrollSpeedY;
				else if ( cursorScrnPos.y > wScrlSpcTop )
					dir.y = ( cursorScrnPos.y - wScrlSpcTop ) * scrollSpeedY;
				break;
			case FOLLOWTYPE.Lirp:
				dir += ( target.position - transform.position ) * lirpSpd;
				//remove for 2d
				dir.y = 0f;
				break;
			case FOLLOWTYPE.Rotation:
				cursorScrnPos = CURSORTYPE.FollowMouse != cursorType ? Camera.main.WorldToScreenPoint ( cursor.position ) : Input.mousePosition;
				if ( cursorScrnPos.x < wScrlSpcLeft )
					transform.Rotate ( transform.up, ( cursorScrnPos.x - wScrlSpcLeft ) * rotationSpeed );
				else if ( cursorScrnPos.x > wScrlSpcRight )
					transform.Rotate ( transform.up, ( cursorScrnPos.x - wScrlSpcRight ) * rotationSpeed );
				//if you have a 3d cursor object make it a child of this object
				break;
			case FOLLOWTYPE.ClickAndDrag:
				if ( Input.GetMouseButton ( 0 ) ) {
					if ( Input.GetMouseButtonDown ( 0 ) )
						startPos = Input.mousePosition;
					Plane playerPlane = new Plane(cameraPlane.forward, cameraPlane.position);
					deltaPos.x = Screen.width * .5f;
					deltaPos.y = Screen.height * .5f;
					deltaPos += ( Input.mousePosition - startPos ) * dragSpeed;
					startPos = Input.mousePosition;
					Ray ray = Camera.main.ScreenPointToRay (deltaPos);
					float hitdist = 0.0f;
					playerPlane.Raycast ( ray, out hitdist );
					startAngle = Quaternion.LookRotation ( ray.GetPoint ( hitdist ) - Camera.main.transform.position );
					Camera.main.transform.rotation = Quaternion.Slerp ( Camera.main.transform.rotation, startAngle, rotationSpeed );
				}
				break;
			default:
				break;
		}
		transform.position += dir;
	}

	static public void Win () {

	}

	static public void Lose () {

	}
}
