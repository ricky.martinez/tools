﻿using JetBrains.Annotations;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ThirdPersonMovement : MonoBehaviour
{
	static public ThirdPersonMovement currInstance;
	public bool canMove = true, canRotate = true, isFlying, isFreeAiming, isTouchControls = true, savePos;
	bool _isFirstPerson, isTap;
	public bool isFirstPerson
	{
		get { return _isFirstPerson; }
		set
		{
			_isFirstPerson = value;
			if (_isFirstPerson)
			{
				lastLocalCamPos = camera.transform.localPosition;
				Tools.LirpToSpot(camera.transform, firstPersonCamPos.position, camera.transform.rotation);
				if (CameraObstacleAvoider.currInstance)
					CameraObstacleAvoider.currInstance.enabled = false;
			}
			else
			{
				Tools.LirpToLocal(camera.transform, lastLocalCamPos, camera.transform.localRotation);
				if (CameraObstacleAvoider.currInstance)
					CameraObstacleAvoider.currInstance.enabled = true;
			}
		}
	}

	public bool isDdging { get; set; }
	public bool isGrounded{get;set;}
	public int maxDblJumps = 1;
	public Transform camRotation, playerToMove;
	new public Camera camera;
	public Animator anim;
	public Animation petAnim;
	public AudioClip land;
	public Transform firstPersonCamPos;
	[HideInInspector]
	public Rigidbody rigidBody;

	public float Speed = 6f, RunSpeed = 11f, flightSpd = 11f, dTapSpd = .3f, minTapDisSqd = 100, JumpForce = 50f, dJumpForce = 50f, cameraRotSpdFreeAiming = 4, cameraRotSpd = 1, dodgeSpd, dodgeHght, maxSpdDragDis = 5, fingerScrollSpd = .5f;
	public LayerMask ground;
	float currSpd, tapTime, mag, fingerDis;
	int vXHash, vYHash, offGroundCount, dblJumpCount;
	Vector2 direction, lastTouchPos, firstTouchPos;
	Vector3 moveDir, lastLocalCamPos;
	Quaternion cameraRot;
	Ray groundRay;

	private void Reset()
	{
		playerToMove = transform;
	}

	private void Awake()
	{
		isGrounded = true;
		if (!playerToMove)
		{
			playerToMove = transform;
		}
		currInstance = this;
	}
	// Start is called before the first frame update
	void Start()
	{
		rigidBody = playerToMove.GetComponentInChildren<Rigidbody>();
		vXHash = Animator.StringToHash("Velocity");
		vYHash = Animator.StringToHash("VelocityY");
		maxSpdDragDis = AutoDragThreshold.currInstance.RelativeDragDistance(maxSpdDragDis);
		minTapDisSqd = AutoDragThreshold.currInstance.RelativeDragDistance(minTapDisSqd);
		groundRay.direction = Vector3.down;

		if (PlayerPrefs.HasKey(SAVEKEY.RotZ.ToString()))
		{
			playerToMove.position = new Vector3(PlayerPrefs.GetFloat(SAVEKEY.PosX.ToString()), PlayerPrefs.GetFloat(SAVEKEY.PosY.ToString()), PlayerPrefs.GetFloat(SAVEKEY.PosZ.ToString()));
			camRotation.rotation = new Quaternion(PlayerPrefs.GetFloat(SAVEKEY.RotX.ToString()), PlayerPrefs.GetFloat(SAVEKEY.RotY.ToString()), PlayerPrefs.GetFloat(SAVEKEY.RotZ.ToString()), PlayerPrefs.GetFloat(SAVEKEY.RotW.ToString()));
		}
		cameraRot = camRotation.rotation = anim.transform.rotation;
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (((1 << collision.collider.gameObject.layer) & ground) != 0)// ground
		{
			if (!isGrounded)
			{
				anim.Play("Land");
				isGrounded = true;
				SoundManager.instance.PlaySoundEffect(land);
			}
		}
	}

	private void FixedUpdate()
	{
		groundRay.origin = transform.position + Vector3.up * 2;
		if (!isFlying && !Physics.Raycast(groundRay, 27, ground))
		{
				Debug.Log("No ground below");
			groundRay.origin = transform.position + Vector3.up * 1000f;
			if (Physics.Raycast(groundRay, out RaycastHit hitInfo, 10000f, ground))
			{
				transform.position = hitInfo.point;
			}
			else
			{
				transform.position = new Vector3(500, 5, 500);
			}
		}

		if (++offGroundCount == 10)
		{
			GetComponentInChildren<Collider>().enabled = true;
		}
	}

	// Update is called once per frame
	void Update()
	{
		UpdateVYHash();
		if (Input.mouseScrollDelta.y != 0 && CameraObstacleAvoider.currInstance)
		{
			CameraObstacleAvoider.currInstance.IncOriginY(Input.mouseScrollDelta.y*maxSpdDragDis);
		}

		if (isTouchControls)
			DoTouchControls();
		else
			DoWasdControls();

		if (CameraObstacleAvoider.currInstance)
			CameraObstacleAvoider.currInstance.enabled = true;

		if (isFreeAiming && canRotate)
			DoFreeAiming();
		else if (isFlying && canMove)
			DoFlying();
		else if (mag >= 0.9f)
		{
			if(petAnim)
			petAnim.enabled = true;
			if (canMove)
				moveDir = (direction.y * camRotation.forward + direction.x * camRotation.right).normalized;
			else
				moveDir = (direction.y * camRotation.up + direction.x * camRotation.right).normalized;
			if (!isDdging)
			{
				if (canMove)
				{
					anim.transform.rotation = Quaternion.LookRotation(moveDir);
					playerToMove.localPosition += (moveDir * currSpd * Time.deltaTime);
					anim.SetFloat(vXHash, Mathf.Max(2f, currSpd * .45f));
				}
			}
			if (Input.GetMouseButton(0) && canRotate)
				camRotation.rotation = cameraRot = Quaternion.LerpUnclamped(cameraRot, Quaternion.LookRotation(moveDir), Time.deltaTime * cameraRotSpd);
		}
		else
		{
			anim.SetFloat(vXHash, 0f);
			if(petAnim)
			petAnim.enabled = false;
		}

	}

	private void DoFlying()
	{
		if (Input.GetMouseButton(0))
		{
			moveDir = (camRotation.forward * flightSpd + direction.y * camRotation.up + direction.x * camRotation.right).normalized;
			playerToMove.localPosition += (moveDir * currSpd * Time.deltaTime);
			anim.transform.rotation = Quaternion.LookRotation(moveDir);
			if (canRotate)
			{
				camRotation.rotation = cameraRot = Quaternion.LerpUnclamped(cameraRot, anim.transform.rotation, Time.deltaTime * cameraRotSpd);
				moveDir.y = 0;
				anim.transform.rotation = Quaternion.LookRotation(moveDir);
			}
		}
	}

	private void DoFreeAiming()
	{
		if (Input.GetMouseButton(0))
		{
			camRotation.Rotate(Vector3.up, direction.x * cameraRotSpdFreeAiming, Space.World);
			camRotation.Rotate(Vector3.right, -direction.y * cameraRotSpdFreeAiming, Space.Self);
			if (CameraObstacleAvoider.currInstance)
				CameraObstacleAvoider.currInstance.enabled = false;
			var camForw = camRotation.transform.forward;
			camForw.y = 0;
			anim.transform.rotation = Quaternion.LookRotation(camForw);
		}
	}

	private void DoWasdControls()
	{
		if (InputExt.GetAxisOnDown("Jump"))
			Jump();
		direction.x = Input.GetAxisRaw("Horizontal");
		direction.y = Input.GetAxisRaw("Vertical");
		currSpd = Input.GetButton("Run") ? RunSpeed : Speed;
		mag = direction.magnitude;
	}

	private void DoTouchControls()
	{
		mag = 0f;
		if (NoTapsOrPinches())
		{
			if (Input.GetMouseButtonDown(0) && !isTap)
				firstTouchPos = Input.mousePosition;
			if (Input.GetMouseButton(0))
			{
				direction = ((Vector2)Input.mousePosition - firstTouchPos) * maxSpdDragDis;
			}
			else
			{
				direction = Vector2.Lerp(direction, Vector2.zero, 3f * Time.deltaTime);
			}
			if (Input.GetMouseButtonUp(0))
			{
				tapTime = Time.time + dTapSpd;
				lastTouchPos = Input.mousePosition;
				direction = Vector2.ClampMagnitude(direction, currSpd);// ensure the slowdown starts at our currspd
			}
			currSpd = mag = isFlying ? RunSpeed : Mathf.Min(direction.magnitude, RunSpeed);
		}
	}

	void UpdateVYHash()
	{
		if (rigidBody.velocity.y < .2f)
		{
			anim.SetFloat(vYHash, 0f);
		}
		else
		{
			anim.SetFloat(vYHash, rigidBody.velocity.y);
		}
	}

	public void Jump()
	{
		if (!isFlying && canMove)
		{
			if (isGrounded)
			{
				rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0f, rigidBody.velocity.z);
				rigidBody.AddForce(new Vector3(0f, JumpForce, 0f));
				anim.Play("Jump");
				isGrounded = false; dblJumpCount = -1;
				GetComponentInChildren<Collider>().enabled = false;
				offGroundCount = 0;
			}
			else if (++dblJumpCount < maxDblJumps)
			{
				rigidBody.velocity = new Vector3(0f, 0f, 0f);
				rigidBody.AddForce(new Vector3(0f, dJumpForce, 0f));
				anim.Play("DJump");
				GetComponentInChildren<Collider>().enabled = false;
				offGroundCount = 0;
			}
		}
	}

	public void DodgeLeft()
	{
		Dodge(-camRotation.right);
	}

	public void DodgeRight()
	{
		Dodge(camRotation.right);
	}

	public void Dodge(Vector3 dir)
	{
		if (!isDdging)
		{
			anim.transform.forward = dir;
			dir += Vector3.up * dodgeHght;
			rigidBody.velocity = Vector3.zero;
			rigidBody.AddForce(dir * dodgeSpd);
			anim.Play("Dodge", 0, 0f);
			isDdging = true;
			Invoke("SetDdg", 2);
		}
	}

	void SetDdg()
	{
		isDdging = false;
	}


	bool NoTapsOrPinches()
	{
		if (Input.touchCount == 2)
		{
			if (Input.touches[1].phase == TouchPhase.Began)
			{
				fingerDis = (Input.touches[0].position - Input.touches[1].position).magnitude;
			}
			else
			{
				if (CameraObstacleAvoider.currInstance)
				{
					CameraObstacleAvoider.currInstance.IncOriginY((fingerDis - (Input.touches[0].position - Input.touches[1].position).magnitude)*fingerScrollSpd);
				}
			}
			return false;
		}
		else if(Input.GetMouseButtonDown(0))
		{
			isTap = tapTime > Time.time && Vector2.SqrMagnitude((Vector2)Input.mousePosition - lastTouchPos) < minTapDisSqd;
			// if we double tapped
			if (isTap)
			{
				// right doodge
				if (Input.mousePosition.x > Screen.width * .85f)
				{
					DodgeRight();
				}
				else if (Input.mousePosition.x < Screen.width * .15f)
				{
					DodgeLeft();
				}
				else
				{
					Jump();
				}

				return false;
			}
			return true;
		}
		return true;
	}

	public void ExitFreeAim()
	{
		isFreeAiming = false;
		rigidBody.isKinematic = false;
		canMove = true;
		anim.Play("Jump");
		isGrounded = false;
	}

	public void SetFreeAim()
	{
		if (isFreeAiming)
			ExitFreeAim();
		else
		{
			isFreeAiming = true;
			rigidBody.isKinematic = true;
			canMove = false;
		}
	}

	public void SetIsFlying()
	{
		if (isFlying)
		{
			ExitFlying();
		}
		else
		{
			isFlying = true;
			rigidBody.useGravity = false;
			isGrounded = false;
		}
	}

	public void ExitFlying()
	{
		isFlying = false;
		rigidBody.useGravity = true;
		isGrounded = false;
	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			OnApplicationQuit();
		}
	}
	private void OnApplicationQuit()
	{
		if (savePos)
		{
			PersistentManager.SetSaveKey(SAVEKEY.PosX, transform.position.x);
			PersistentManager.SetSaveKey(SAVEKEY.PosY, transform.position.y);
			PersistentManager.SetSaveKey(SAVEKEY.PosZ, transform.position.z);
			PersistentManager.SetSaveKey(SAVEKEY.RotW, camRotation.rotation.w);
			PersistentManager.SetSaveKey(SAVEKEY.RotX, camRotation.rotation.x);
			PersistentManager.SetSaveKey(SAVEKEY.RotY, camRotation.rotation.y);
			PersistentManager.SetSaveKey(SAVEKEY.RotZ, camRotation.rotation.z);
		}
	}
}
