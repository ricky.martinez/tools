﻿using JetBrains.Annotations;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ThirdPersonMovementShip : MonoBehaviour
{
	static public ThirdPersonMovementShip currInstance;
	[field: SerializeField] public bool canMove { get; set; } = true; [field: SerializeField] public bool canRotate { get; set; } = true;
	public bool isFlying, isFreeAiming, isTouchControls = true, savePos;
	bool _isFirstPerson;
	public bool isFirstPerson
	{
		get { return _isFirstPerson; }
		set
		{
			_isFirstPerson = value;
			if (_isFirstPerson)
			{
				lastLocalCamPos = camera.transform.localPosition;
				lastLocalCamRot = camera.transform.localRotation;
				Tools.LirpToSpot(camera.transform, firstPersonCamPos.position, firstPersonCamPos.rotation);
				if (CameraObstacleAvoider.currInstance)
					CameraObstacleAvoider.currInstance.enabled = false;
			}
			else
			{
				Tools.LirpToLocal(camera.transform, lastLocalCamPos, lastLocalCamRot);
				if (CameraObstacleAvoider.currInstance)
					CameraObstacleAvoider.currInstance.enabled = true;
			}
		}
	}
	public Transform playerToMove, world, cameraRot;
	new public Camera camera;
	public Animator anim;
	public Transform firstPersonCamPos;

	public float Speed = 6f, RunSpeed = 11f, cameraRotSpd = 1, screenSizeDragFactor = 5, fingerScrollSpd = .5f, minTapDisSqd = 100, dTapSpd = .3f, maxZoomDis = 10, minZoomDis = .1f;
	float currSpd, mag, fingerDis, tapTime;
	[HideInInspector]
	public int vXHash;
	Vector2 dragDirection, lastTouchPos, firstTouchPos;
	Vector3 moveDir, lastLocalCamPos;
	Quaternion lastLocalCamRot;

	private void Reset()
	{
		playerToMove = transform;
	}

	private void Awake()
	{
		if (!playerToMove)
		{
			playerToMove = transform;
		}
		currInstance = this;
	}
	// Start is called before the first frame update
	void Start()
	{
		vXHash = Animator.StringToHash("Velocity");
		minTapDisSqd = AutoDragThreshold.currInstance.RelativeDragDistance(minTapDisSqd);
		screenSizeDragFactor = AutoDragThreshold.currInstance.RelativeDragDistance(screenSizeDragFactor);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.mouseScrollDelta.y != 0)
		{
			PinchZoom(Input.mouseScrollDelta.y * 2);
		}

		if (isTouchControls)
			DoTouchControls();
		else
			DoWasdControls();

		if (CameraObstacleAvoider.currInstance)
			CameraObstacleAvoider.currInstance.enabled = true;

		if (isFreeAiming && canRotate)
			DoFreeAiming();
		else if (mag >= 0.9f)
		{
			moveDir = (dragDirection.y * cameraRot.forward + dragDirection.x * cameraRot.right).normalized;
			if (canMove)
			{
				playerToMove.localPosition += (moveDir * currSpd * Time.deltaTime);
				playerToMove.up = playerToMove.position - world.position;

				anim.SetFloat(vXHash, currSpd * .45f);// to slow down the rate a bit
			}
			if (Input.GetMouseButton(0) && canRotate)
			{
				anim.transform.rotation = Quaternion.LookRotation(moveDir, playerToMove.up);
				cameraRot.rotation = Quaternion.LerpUnclamped(cameraRot.rotation, anim.transform.rotation, Time.deltaTime * cameraRotSpd);
			}
		}
		else
		{
			anim.Play("Sway", 0, 0);
			anim.SetFloat(vXHash, 0f);
		}

	}

	private void DoFreeAiming()
	{
		if (Input.GetMouseButton(0))
		{

		}
	}

	private void DoWasdControls()
	{
		dragDirection.x = Input.GetAxisRaw("Horizontal");
		dragDirection.y = Input.GetAxisRaw("Vertical");
		currSpd = Input.GetButton("Run") ? RunSpeed : Speed;
		mag = dragDirection.magnitude;
	}

	private void DoTouchControls()
	{
		mag = 0f;
		if (Input.touchCount != 2)
		{
			if (Input.GetMouseButtonDown(0) && (tapTime < Time.time || Vector2.SqrMagnitude((Vector2)Input.mousePosition - lastTouchPos) > minTapDisSqd))
				firstTouchPos = Input.mousePosition;

			if (Input.GetMouseButton(0))
				dragDirection = ((Vector2)Input.mousePosition - firstTouchPos);
			else
				dragDirection = Vector2.Lerp(dragDirection, Vector2.zero, 3f * Time.deltaTime);

			if (Input.GetMouseButtonUp(0))
			{
				tapTime = Time.time + dTapSpd;
				lastTouchPos = Input.mousePosition;
				dragDirection = Vector2.ClampMagnitude(dragDirection, currSpd);// ensure the slowdown starts at our currspd
			}
			currSpd = mag = isFlying ? RunSpeed : Mathf.Min(dragDirection.magnitude * screenSizeDragFactor, RunSpeed);
		}
		else
		{
			DoPinch();
		}
	}



	void DoPinch()
	{
		if (Input.touches[1].phase == TouchPhase.Began)
			fingerDis = (Input.touches[0].position - Input.touches[1].position).magnitude;
		else
			PinchZoom((fingerDis - (Input.touches[0].position - Input.touches[1].position).magnitude));
	}

	void PinchZoom(float yDis)
	{
		var dis = (firstPersonCamPos.transform.localPosition - camera.transform.localPosition).magnitude;
		if (dis < maxZoomDis && dis > minZoomDis)
		{
			camera.transform.localPosition += yDis * fingerScrollSpd * (firstPersonCamPos.transform.localPosition - camera.transform.localPosition);

		}
	}

	public void ExitFreeAim()
	{
		isFreeAiming = false;
		canMove = true;
		anim.Play("Jump");
	}

	public void SetFreeAim()
	{
		if (isFreeAiming)
			ExitFreeAim();
		else
		{
			isFreeAiming = true;
			canMove = false;
		}
	}

	private void OnApplicationPause(bool pause)
	{
		if (pause)
		{
			OnApplicationQuit();
		}
	}
	private void OnApplicationQuit()
	{
		if (savePos)
		{
			PersistentManager.SetSaveKey(SAVEKEY.PosX, playerToMove.position.x);
			PersistentManager.SetSaveKey(SAVEKEY.PosY, playerToMove.position.y);
			PersistentManager.SetSaveKey(SAVEKEY.PosZ, playerToMove.position.z);
			PersistentManager.SetSaveKey(SAVEKEY.RotW, playerToMove.rotation.w);
			PersistentManager.SetSaveKey(SAVEKEY.RotX, playerToMove.rotation.x);
			PersistentManager.SetSaveKey(SAVEKEY.RotY, playerToMove.rotation.y);
			PersistentManager.SetSaveKey(SAVEKEY.RotZ, playerToMove.rotation.z);
		}
	}
}
