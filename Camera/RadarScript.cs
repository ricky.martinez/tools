﻿#undef REMOVE_EFFECTS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarScript : MonoBehaviour
{
	public float radarSize = .3f, scrollSpd = 10f, maxZoom = 500, minZoom = 10;
	public int offset = 3;
	Camera cam;
	public bool removeEffects = true, allowZoom = true;
	private void Start()
	{
		cam = GetComponent<Camera>();
		
		cam.pixelRect = new Rect(offset, offset, Screen.height* radarSize,Screen.height* radarSize);
	}

	private void Update()
	{
		if (allowZoom && Input.mousePosition.x < cam.pixelRect.width + offset && Input.mousePosition.y < cam.pixelRect.height + offset)
		{
			cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - Input.GetAxis("Mouse ScrollWheel") * scrollSpd, minZoom, maxZoom);
		}
	}
#if REMOVE_EFFECTS

	private void OnPreRender()
	{
		RenderSettings.fog = false;
	}

	private void OnPostRender()
	{
		RenderSettings.fog = true;
	}
#endif

}
