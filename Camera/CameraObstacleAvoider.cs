using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreePosInd
{
	public bool verified;
	public int index;
	public TreeInstance inst;
}

public class CameraObstacleAvoider : MonoBehaviour
{
	public static CameraObstacleAvoider currInstance;
	public bool hideTrees;
	public Transform target, centerCast;
	[ConditionalField("hideTrees", true)]
	public Terrain terrain;
	public GameObject debugSphere;
	public float radius = 1f, minDis = 2, maxDis = 16;
	[Range(.001f, 1)]
	public float avoidSpd = .1f;
	float playerDis, disCheck, invMapWidth, originY;
	int treesHit, totalTrees, c;
	public LayerMask ground, tree;
	TreePosInd[] treePosInds;
	Vector3 localPos, boxExtents, rayhitPoint, faceVect;
	Ray ray;
	RaycastHit rayHit;

	void Awake()
	{
		currInstance = this;
	}

	private void Start()
	{
		localPos = centerCast.localPosition;
		localPos.y = 100f;
		localPos.z = transform.localPosition.z * .5f;
		centerCast.localPosition = localPos;
		localPos = transform.localPosition;
		if (PlayerPrefs.HasKey(SAVEKEY.Zoom.ToString()))
			localPos.y = originY = PlayerPrefs.GetFloat(SAVEKEY.Zoom.ToString());
		transform.localPosition = localPos;
		playerDis = Tools.DisXZ(target.position, transform.position);
		if (hideTrees)
		{
			invMapWidth = 1f / terrain.terrainData.alphamapWidth;
			disCheck = 5f / terrain.terrainData.alphamapWidth * invMapWidth;
			totalTrees = terrain.terrainData.treeInstanceCount;

		}
		treePosInds = new TreePosInd[3] { new TreePosInd(), new TreePosInd(), new TreePosInd() };
		if (debugSphere)
		{
			debugSphere = Instantiate(debugSphere);
			debugSphere.transform.localScale = Vector3.one * radius * 2;
		}
		boxExtents.z = playerDis * .49f;
		boxExtents.x = boxExtents.y = radius;
	}

	public void IncOriginY(float val)
	{
		originY += val;
		originY = Mathf.Clamp(originY, minDis, maxDis);
		PersistentManager.SetSaveKey(SAVEKEY.Zoom, originY);
	}

	float CalculateHeightAtStadium(Vector3 velocity)
	{
		// Compute height at that time (no gravity, so linear motion in y)
		return velocity.y * playerDis / Mathf.Sqrt(velocity.x * velocity.x + velocity.z * velocity.z);
	}

	private void FixedUpdate()
	{
		Physics.BoxCast(centerCast.position, boxExtents, Vector3.down, out rayHit, centerCast.rotation, 100, ground);
		rayhitPoint = rayHit.point;
		rayhitPoint.y += radius;
		if (debugSphere)
			debugSphere.transform.position = rayhitPoint;
		rayhitPoint = rayhitPoint - target.position;
		faceVect = target.position - transform.position;
		if (Vector3.Dot(faceVect, rayhitPoint) < -100)
		{
			localPos.y = Mathf.LerpUnclamped(localPos.y, Mathf.Min(maxDis, Mathf.Max(originY, CalculateHeightAtStadium(rayhitPoint) + target.localPosition.y)), avoidSpd);
		}
		else
		{
			localPos.y = Mathf.LerpUnclamped(localPos.y, originY, avoidSpd);
		}

		transform.localPosition = localPos;
		transform.forward = faceVect;
		if (hideTrees)
		{
			int i = -1; while (++i <= treesHit && i < treePosInds.Length)
			{
				if (Physics.Raycast(ray, out rayHit, playerDis, tree))
				{
					ray.origin = rayHit.point + ray.direction * .3f;
					rayHit.point *= invMapWidth;
					if (treesHit == i)// we hae a new tree
					{
						var tData = terrain.terrainData;
						c = totalTrees;
						while (--c != -1)
						{
							treePosInds[treesHit].inst = tData.GetTreeInstance(c);
							if (Tools.SqdDisXZ(treePosInds[treesHit].inst.position, rayHit.point) < disCheck)
							{
								treePosInds[treesHit].index = c;
								treePosInds[treesHit].inst.heightScale = 0;
								treePosInds[treesHit].verified = true;
								tData.SetTreeInstance(c, treePosInds[treesHit++].inst);
								break;
							}
						}
					}
					else
					{
						// find the treeposind to verify
						int j = -1; while (++j != treesHit)
						{
							if (!treePosInds[j].verified)
							{
								if (Tools.SqdDisXZ(treePosInds[j].inst.position, rayHit.point) < disCheck)
								{
									treePosInds[i].verified = true;
								}
							}
						}
					}
				}
			}
			i = -1; while (++i < treesHit)
			{
				if (!treePosInds[i].verified)
				{
					treePosInds[i].inst.heightScale = 1;
					terrain.terrainData.SetTreeInstance(treePosInds[i].index, treePosInds[i].inst);
					var copy = treePosInds[i];
					treePosInds[i] = treePosInds[--treesHit];
					treePosInds[treesHit] = copy;
				}
				else
				{
					treePosInds[i].verified = false;
				}
			}
		}

		//if (Physics.SphereCast(ray, outerRadius, out rayHit, playerDis, tree))
		//{
		//	white.transform.position = rayHit.point;
		//	pos.x = transform.localPosition.x;
		//	if (Physics.SphereCast(ray, innerRadius, out rayHit, playerDis, tree))
		//	{
		//		red.transform.position = rayHit.point;
		//		Vector2 invDir = new Vector2(ray.direction.z, -ray.direction.x);
		//		Vector3 pointDir = rayHit.point - ray.origin;
		//		pointDir.y = pointDir.z;

		//		if (Vector2.Dot(invDir, pointDir) < 0)
		//		{
		//			// right
		//			pos.x += .2f;
		//		}
		//		else
		//		{
		//			// left
		//			pos.x -= .2f;
		//		}
		//	}
		//}

	}
}
