using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class FPSControls : MonoBehaviour
{
	public static FPSControls Instance { get; private set; }

	public bool canFloat, canMove = true, canRotate = true;
	public Transform camTrans { get { return cam.transform; } }
	[SerializeField]
	Camera cam;
	public Camera Cam { get { return cam; } }
	public Transform floorPos, camReturnPos;
	[Range(100f, 500f)]
	public float camXSpd = 300f, camYSpd = 300f;
	[Range(.1f, 100f)]
	public float moveSpd = 100f, floatSpd = 10, zoomSpd = 5f, crouchSpd = 5f;
	Animator anim;
	Rigidbody rBody;

	Stack<ZoomInInfo> zoomPoints;
	GameObject zoomOutBtn;
	SphereCollider currLimiter;

	private void Reset()
	{
		if (!GetComponent<Rigidbody>())
		{
			gameObject.AddComponent<Rigidbody>();
		}
	}
	void Awake()
	{
		Instance = this;
		anim = GetComponent<Animator>();
		rBody = GetComponent<Rigidbody>();
		zoomPoints = new Stack<ZoomInInfo>();
		foreach (var item in FindObjectsOfType<Button>(true))
		{
			if (item.name == "ZoomOutBtn")
			{
				zoomOutBtn = item.gameObject;
				if (item.onClick.GetPersistentEventCount() == 0)
					item.onClick.AddListener(UnZoom1);
				break;
			}
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (canRotate)
		{
			if (Input.GetMouseButtonDown(1))
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else if (Input.GetMouseButton(1))
			{
				camTrans.Rotate(Vector3.up, Input.GetAxis("Mouse X") * Time.deltaTime * camXSpd, Space.World);
				var rotBeforeRot = camTrans.localRotation;
				camTrans.Rotate(Vector3.right, -Input.GetAxis("Mouse Y") * Time.deltaTime * camYSpd);
				if (Vector3.Dot(Vector3.down, camTrans.up) > -.05)
					camTrans.localRotation = rotBeforeRot;
			}
			if (EventSystem.current && !EventSystem.current.IsPointerOverGameObject())
			{
				if (Input.mouseScrollDelta.y > 0 && cam.fieldOfView > 10)
				{
					cam.fieldOfView -= zoomSpd;
				}
				else if (Input.mouseScrollDelta.y < 0 && cam.fieldOfView < 90)
				{
					cam.fieldOfView += zoomSpd;
				}
			}
		}
		if (Input.GetMouseButtonUp(1))
		{
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
		Vector3 forw = camTrans.forward * Input.GetAxis("Vertical");

		if (canMove)
		{
			forw.y = 0f;
			forw = forw.normalized;
			rBody.AddForce((forw + camTrans.right * Input.GetAxis("Horizontal")) * transform.localScale.x * moveSpd);
			if (!Physics.CheckSphere(floorPos.position, .1f))
			{
				rBody.AddForce(Vector3.down * 100);
			}
		}
		else if (canFloat)
		{
			camTrans.position += (forw + camTrans.right * Input.GetAxis("Horizontal")) * transform.localScale.x * floatSpd * Time.deltaTime;
			if (currLimiter)
			{
				var toFromVect = (camTrans.position - (currLimiter.center + currLimiter.transform.position));
				var mag = toFromVect.magnitude;
				if (mag > currLimiter.radius)
				{
					camTrans.position = (currLimiter.center + currLimiter.transform.position) + toFromVect / mag * currLimiter.radius;
				}
			}
		}

		if (zoomPoints.Count > 0 && Input.GetKeyDown(KeyCode.Escape))
		{
			UnZoom1();
		}
		if (Input.GetKey(KeyCode.Q) && camTrans.localPosition.y > -.8f)
		{
			camTrans.position += Vector3.down*Time.deltaTime*crouchSpd;
		}
		else if (Input.GetKey(KeyCode.E) && camTrans.localPosition.y < 0)
		{
			camTrans.position += Vector3.up*Time.deltaTime*crouchSpd;
		}
	}

	public void ZoomToPoint(ZoomInInfo incomingZoomInInfo)
	{
		incomingZoomInInfo.enterEvent?.Invoke();
		if (zoomPoints.TryPeek(out ZoomInInfo currentZoomInfo))
		{
			currentZoomInfo.exitEvent?.Invoke();
		}
		//if (incomingZoomInInfo.onlyZoomedInteractions)
		//	InteractionManager.instance.SetLayerMaskToZoom();
		//else
		//	InteractionManager.instance.SetLayerMaskToRegular();

		DoTransitionAndMove(incomingZoomInInfo);


		if (incomingZoomInInfo.canReturnToPreviousPoint)
		{
			ZoomInInfo toSetZoomInfo = incomingZoomInInfo;
			toSetZoomInfo.pos = camTrans.position;
			toSetZoomInfo.rot = camTrans.rotation;
			toSetZoomInfo.canMove = canMove;
			toSetZoomInfo.canFloat = canFloat;
			toSetZoomInfo.canRotate = canRotate;
			toSetZoomInfo.fov = cam.fieldOfView;
			toSetZoomInfo.moveLimiter = currLimiter;
			zoomPoints.Push(toSetZoomInfo);
		}
		else
			UnZoomAll(true, false);

		currLimiter = incomingZoomInInfo.moveLimiter;
		canMove = incomingZoomInInfo.canMove;
		cam.fieldOfView = incomingZoomInInfo.fov;
		canFloat = incomingZoomInInfo.canFloat;
		rBody.useGravity = canMove;
		canRotate = incomingZoomInInfo.canRotate;
		zoomOutBtn?.SetActive(incomingZoomInInfo.canReturnToPreviousPoint);
	}

	void DoTransitionAndMove(ZoomInInfo zoomInInfo)
	{

		// if position is outside of constraint, place position inside of constraint
		if (zoomInInfo.canFloat && zoomInInfo.moveLimiter)
		{
			var toFromVect = (zoomInInfo.pos - (zoomInInfo.moveLimiter.center + zoomInInfo.moveLimiter.transform.position));
			var mag = toFromVect.magnitude;
			if (mag > zoomInInfo.moveLimiter.radius)
			{
				zoomInInfo.pos = (zoomInInfo.moveLimiter.center + zoomInInfo.moveLimiter.transform.position) + toFromVect / mag * zoomInInfo.moveLimiter.radius;
			}
		}
		switch (zoomInInfo.transition)
		{
			case Transition.Fadeout:
				StartCoroutine(FadeScreen());
				if (zoomInInfo.canMove || (!zoomInInfo.canReturnToPreviousPoint && !zoomInInfo.canFloat))
					MoveFullCharacter(zoomInInfo);
				else
				{
					camTrans.position = zoomInInfo.pos;
					camTrans.rotation = zoomInInfo.rot;
				}
				break;
			case Transition.lirp:
				StartCoroutine(LirpCharacter(zoomInInfo));
				break;
			default:
				break;
		}
	}

	IEnumerator LirpCharacter(ZoomInInfo zoomInInfo)
	{
		yield return Tools.LirpToSpot(camTrans, zoomInInfo.pos, zoomInInfo.rot);
		if (zoomInInfo.canMove || !zoomInInfo.canReturnToPreviousPoint)
			MoveFullCharacter(zoomInInfo);
	}

	void MoveFullCharacter(ZoomInInfo zoomInInfo)
	{
		transform.position = zoomInInfo.pos - camReturnPos.localPosition;
		transform.rotation = zoomInInfo.rot;
		Vector3 forw = transform.forward;
		forw.y = 0;
		transform.forward = forw;
		camTrans.localPosition = Vector3.zero;
		camTrans.rotation = zoomInInfo.rot;
	}

	public IEnumerator FadeScreen()
	{
		cam.enabled = false;
		//CanvasManager.Instance.Transitioner.SetActive(true);
		yield return new WaitForSeconds(2f);
        cam.enabled = true;
		yield return new WaitForSeconds(1f);
        //CanvasManager.Instance.Transitioner.SetActive(false);
    }

	public void ZoomToPoint(bool _canFloat, bool _canRotate, Transform lirpSpot, UnityEvent _exitEvent = null, bool _canMove = false)
	{
		ZoomToPoint(new ZoomInInfo { canFloat = _canFloat, canRotate = _canRotate, canMove = _canMove, pos = lirpSpot.position, rot = lirpSpot.rotation, exitEvent = _exitEvent });
	}

	public void ZoomToPoint(bool _canFloat, bool _canRotate, Vector3 lirpPos, Quaternion lirpRot, UnityEvent _exitEvent, bool _canMove = false)
	{
		ZoomToPoint(new ZoomInInfo { canFloat = _canFloat, canRotate = _canRotate, canMove = _canMove, pos = camTrans.position, rot = camTrans.rotation, exitEvent = _exitEvent });
	}

	public void UnZoom1()
	{
		if (zoomPoints.Count == 0)
		{
			Debug.Log("Trying to zoom out, but you weren't zoomed.");
			return;
		}
		var zoomInInfo = zoomPoints.Pop();
		if (zoomPoints.Count == 0)
		{
			//InteractionManager.instance.SetLayerMaskToRegular();
			zoomOutBtn?.SetActive(false);
		}
		else
		{
			zoomOutBtn?.SetActive(true);
			zoomPoints.Peek().enterEvent?.Invoke();
			//if (zoomPoints.Peek().onlyZoomedInteractions)
			//	InteractionManager.instance.SetLayerMaskToZoom();
			//else
			//	InteractionManager.instance.SetLayerMaskToRegular();
		}

		DoTransitionAndMove(zoomInInfo);
		currLimiter = zoomInInfo.moveLimiter;
		canMove = zoomInInfo.canMove;
		canFloat = zoomInInfo.canFloat;
		rBody.useGravity = canMove;
		canRotate = zoomInInfo.canRotate;
		zoomInInfo.exitEvent?.Invoke();
	}

	public void UnZoomAll(bool doAllExitActions = true, bool useLastPos = true)
	{
		//InteractionManager.instance.SetLayerMaskToRegular();
		zoomOutBtn?.SetActive(false);
		if (zoomPoints.Count == 0)
		{
			return;
		}
		// not sure if this is the best way to get last item from stack
		ZoomInInfo zoomInInfo = default;
		while (zoomPoints.Count != 0)
		{
			if (doAllExitActions)
				zoomInInfo.exitEvent?.Invoke();
			zoomInInfo = zoomPoints.Pop();
		}
		if (useLastPos)
		{
			DoTransitionAndMove(zoomInInfo);
			canMove = zoomInInfo.canMove;
			canFloat = zoomInInfo.canFloat;
			canRotate = zoomInInfo.canRotate;
			rBody.useGravity = canMove;
			currLimiter = zoomInInfo.moveLimiter;
		}
		if (doAllExitActions)
			zoomInInfo.exitEvent?.Invoke();
	}

	bool tempMove, tempRotate, tempFloat;
	public void TemporaryLockMove()
	{
		tempMove = canMove;
		tempRotate = canRotate;
		tempFloat = canFloat;
		canMove = false;
		canRotate = false;
		canFloat = false;
	}
	public void TemporaryLockMove(bool _canMove, bool _canRotate, bool _canFloat)
	{
		tempMove = canMove;
		tempRotate = canRotate;
		tempFloat = canFloat;
		canMove = _canMove;
		canRotate = _canRotate;
		canFloat = _canFloat;
	}

	public void UnlockMove()
	{
		canMove = tempMove;
		canRotate = tempRotate;
		canFloat = tempFloat;
	}
}

[Serializable]
public struct ZoomInInfo
{
	public bool canMove, canRotate, canFloat, canReturnToPreviousPoint, onlyZoomedInteractions;
	public float fov;
	public Vector3 pos;
	public Quaternion rot;
	public UnityEvent enterEvent, exitEvent;
	public Transition transition;
	public SphereCollider moveLimiter;
}

public enum Transition
{
	lirp, Fadeout
}