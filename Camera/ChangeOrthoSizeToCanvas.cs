﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOrthoSizeToCanvas : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Camera>().orthographicSize = Screen.height / 2;
	}
}
