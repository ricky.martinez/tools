Shader "Unlit/BillboardText_Cutout"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Cutoff ("Alpha Cutoff", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "Queue"="AlphaTest" "RenderType"="Opaque" }
        Pass
        {
            Cull Off
            Lighting Off
            ZWrite On   // Enable depth writing (like opaque geometry)
            ZTest LEqual // Correct depth sorting
            AlphaToMask On // Enables MSAA-friendly alpha testing

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _Color;
            float _Cutoff;

            v2f vert (appdata_t v)
            {
                v2f o;
                float3 viewDir = normalize(ObjSpaceViewDir(float4(0,0,0,1)));
                float3 upDir = float3(0,1,0);
                float3 rightDir = normalize(cross(upDir, viewDir));

                // Billboard effect
                float3 billboardPos = rightDir * v.vertex.x + upDir * v.vertex.y;
                o.vertex = UnityObjectToClipPos(float4(billboardPos, 1.0));
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                
                // Alpha cutout (clip pixels below threshold)
                if (col.a < _Cutoff)
                    discard; // Removes fully transparent pixels
                
                return col;
            }
            ENDCG
        }
    }
}
