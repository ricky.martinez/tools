﻿Shader "Custom/LightEmission"
{
	Properties
	{
		_Emission("Emision", Color) = (0,0,0,0)
		_Color("Color", Color) = (0, 0, 0, 1)
	}

	SubShader
	{

		Pass
		{
			Tags {"LightMode" = "ForwardBase" "RenderType" = "Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha

CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"
	#include "UnityLightingCommon.cginc" // for _LightColor0

		// these are your actual variables
			uniform fixed4 _Color;
			uniform fixed4 _Emission;

			// this is what gets passed from the vert shader to the color shader
			struct v2f {
				float4 color : COLOR0; // diffuse lighting color
				float4 vertex : SV_POSITION;
			};


			v2f vert(appdata_base v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = _LightColor0 * max(0, dot(UnityObjectToWorldNormal(v.normal), _WorldSpaceLightPos0.xyz))*_Color + _Emission*_Emission.a;
				o.color.a = _Color.a;
				//o.color.a = _Color.a;
				return o;
			}


			fixed4 frag(v2f i) :COLOR
			{
				return i.color;
			}

		ENDCG
			}
	}

	FallBack off
}
