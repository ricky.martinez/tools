﻿Shader "Custom/FluidFlow"
{
	Properties
	{
		_Color("Color", Color) = (0, 0, 0, 1)
		_Color2("Color2", Color) = (1, 1, 1, 1)
		_Spread("Spread", Float) = 5
		_Speed("Speed", Float) = 1
	}

	SubShader
	{

		Pass
		{
			Tags { "Queue" = "Transparent" "RenderType" = "Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha

CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag

		// these are your actual variables
			uniform fixed4 _Color;
			uniform fixed4 _Color2;
			float _Spread;
			float _Speed;

			struct appdata
			{
				float2 uv : TEXCOORD0;
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{// negative bc the flow direction; _time.y bc that's x1 time. 
				return lerp(_Color, _Color2,  (-i.uv.y+_Time.y*_Speed)%_Spread);
			}

		ENDCG
			}
	}

	FallBack off
}
