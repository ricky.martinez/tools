Shader "Custom/TransparentZDepth"
{
    Properties
    {
        _MainTex ("Albedo (RGB) Transparency (A)", 2D) = "white" {}
        _Color ("Tint Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        // Pass 1: Depth pre-pass (writes to depth buffer)
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        ZWrite On
        ColorMask 0

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 pos : SV_POSITION;
            };

            v2f vert(appdata_t v) {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 frag(v2f i) : SV_Target {
                return float4(0,0,0,0);
            }
            ENDCG
        }

        // Pass 2: Normal surface rendering with transparency
        CGPROGRAM
        #pragma surface surf Standard alpha:blend

        sampler2D _MainTex;
        fixed4 _Color;

        struct Input
        {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = tex.rgb * _Color.rgb;
            o.Alpha = tex.a * _Color.a;
        }
        ENDCG
    }

    FallBack "Transparent/VertexLit"
}
