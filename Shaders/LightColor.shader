﻿Shader "Custom/LightColor"
{
	Properties
	{
		_Color("Color", Color) = (0, 0, 0, 0)
	}

	SubShader
	{

		Pass
		{
			Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True"}
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag

		// these are your actual variables
			uniform fixed4 _Color;
			uniform fixed4 _LightDirection;

			// this is what gets passed from the vert shader to the color shader
			struct v2f {
				float4 color : COLOR0;
				float4 vertex : SV_POSITION;
			};
			// this is what gets passed from the vert shader to the color shader
			struct inV {
				float3 normal : NORMAL;
				float4 vertex : POSITION;
			};


			v2f vert(inV v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color.xyz = max(.3f, dot(v.normal, _LightDirection))*_Color.xyz;
				o.color.a = _Color.a;
				return o;
			}


			fixed4 frag(v2f i) :COLOR
			{
				//i.color.a = _Color.a;
				return i.color;
			}

		ENDCG
			}
	}

	FallBack off
}