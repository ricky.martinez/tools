﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastKit : MonoBehaviour
{

	public float timer = 2.3f;// generally tool tips are read for more than 2 sec. any longer is annoying, however htey can click to close it.

	private static ToastKit _instance;
	private void Start()
	{
		if (_instance && _instance != this)
		{
			Destroy(gameObject);
		}
	}
	public static ToastKit instance
	{
		get
		{
			if (!_instance)
			{
				_instance = FindObjectOfType<Canvas>().GetComponentInChildren<ToastKit>(true);
				if (_instance)
					return _instance;
				Debug.LogWarning("You called toast without having a toast object, a default was created");
				// this means there is not a default object, so we need to create one.
				RectTransform obj = new GameObject("ToastKit", typeof(RectTransform), typeof(CanvasRenderer)).transform as RectTransform;
				obj.SetParent(FindObjectOfType<Canvas>().transform);// gots to be in canvas for phones/changing resolutions
				obj.anchorMin = Vector2.zero;
				obj.anchorMax = Vector2.one;
				obj.localScale = Vector3.one;

				// add a back drop and color it
				UnityEngine.UI.Image comp = obj.gameObject.AddComponent<UnityEngine.UI.Image>();
				comp.color = new Color(0f, 0f, 0f, .5f);// black faded by half

				// add a click event that will close the tooltip
				UnityEngine.EventSystems.EventTrigger et = obj.gameObject.AddComponent<UnityEngine.EventSystems.EventTrigger>();
				et.triggers.Add(new UnityEngine.EventSystems.EventTrigger.Entry() { eventID = UnityEngine.EventSystems.EventTriggerType.PointerClick });
				et.triggers[0].callback.AddListener(x => _instance.gameObject.SetActive(false));

				// add a text child
				RectTransform obj2 = new GameObject("txtChild", typeof(RectTransform), typeof(CanvasRenderer)).transform as RectTransform;
				obj2.SetParent(obj);
				obj2.anchorMin = new Vector2(0, .3333333f);// take up the middle 1/3 of the screen stretched width wide.
				obj2.anchorMax = new Vector2(1, .666667f);

				// add the text component with usable values
				UnityEngine.UI.Text txtComp = obj2.gameObject.AddComponent<UnityEngine.UI.Text>();
				txtComp.alignment = TextAnchor.MiddleCenter;
				txtComp.fontSize = 72;
				txtComp.resizeTextForBestFit = true;
				txtComp.font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");

				// make sure to set the instance so this doesnt get ran again.
				_instance = obj.gameObject.AddComponent<ToastKit>();
			}
			return _instance;
		}
	}

	public static void CallToast(string _message)
	{
		instance.GetComponentInChildren<UnityEngine.UI.Text>().text = _message;
		_instance.transform.SetAsLastSibling();// toast should always be on top of everything
		_instance.gameObject.SetActive(true);
		_instance.Invoke("SetInactive", _instance.timer);
	}

	public void SetInactive()
	{
		gameObject.SetActive(false);
	}

	public void OnDisable()
	{
		CancelInvoke();
	}

}