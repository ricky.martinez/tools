﻿using System;
using UnityEngine;

public class TagSelectorAttribute : PropertyAttribute
{
}

public class RemoveExpanderAttribute : PropertyAttribute
{
}

public class ComponentSelectorAttribute : PropertyAttribute
{
	public int ind;
	public GameObject obj;
}

public class RequiredComponentAttribute : PropertyAttribute
{
}

public class ParentClassAttribute : PropertyAttribute
{
	public Type t;

	/// <summary>
	///   Create a new attribute and initialize a certain type.
	/// </summary>
	/// <param name = "dynamicType">The type to initialize.</param>
	/// <param name = "constructorArguments">
	///   The arguments to pass to the constructor of the type.
	/// </param>
	public ParentClassAttribute(Type dynamicType)
	{
		t = dynamicType;
	}
}
/// <summary>
/// give this class teh ParentClass attribute to allow it to draw correctly
/// </summary>
[Serializable]
public class ChildClassSelector
{
	// prevents anyeone from trying to make one of these. Unity can still use it in the editor window (probably bc of Reflection overriding protection)
	private ChildClassSelector() { }
	public int selectedClassNameInd;
	public int storedClassNameInd = -1;
	// has to be instantiable object or unity cant recognize it.
	[SerializeReference]
	[SerializeField]
	public object storedInstance;
}

public class Add2Dor3DColliderAttribute : PropertyAttribute
{
}

[AttributeUsage(AttributeTargets.Field)]
public class ConditionalFieldAttribute : PropertyAttribute
{
	public string PropertyToCheck;
	public bool reverse;

	public ConditionalFieldAttribute(string propertyToCheck, bool reverse = false)
	{
		PropertyToCheck = propertyToCheck;
		this.reverse = reverse;
	}
}