﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
#pragma warning disable 0649
#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(TagSelectorAttribute))]
public class TagSelectorPropertyDrawer : PropertyDrawer
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if (property.propertyType == SerializedPropertyType.String)
		{
			EditorGUI.BeginProperty(position, label, property);
			property.stringValue = EditorGUI.TagField(position, label, property.stringValue);

			EditorGUI.EndProperty();
		}
		else
		{
			EditorGUI.PropertyField(position, property, label);
		}
	}
}

[CustomPropertyDrawer(typeof(RemoveExpanderAttribute))]
public class RemoveExpandedDrawer : PropertyDrawer
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);
		position.height = 18;
		foreach (SerializedProperty prop in property)
		{
			EditorGUI.PropertyField(position, prop);
			position.y += position.height;
		}
		EditorGUI.EndProperty();
	}
}


[CustomPropertyDrawer(typeof(ChildClassSelector))]
public class NoParent : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		Debug.LogError("Trying to use ChildClassSelector without ParentClass attribute");
	}
}

[CustomPropertyDrawer(typeof(ParentClassAttribute))]
public class ChildSelectorDrawer : PropertyDrawer
{
	//public override VisualElement CreatePropertyGUI(SerializedProperty property)
	//{
	//	VisualElement vi = new VisualElement();
	//	vi.Add(new PropertyField(property.FindPropertyRelative("hi"),"suck it"));
	//	return vi;
	//}
	SerializedProperty incomingNameProp, storedInstanceProp;
	List<string> allClasses;
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		try
		{
			// get initial properties here so we dont call them a bunch
			incomingNameProp = property.FindPropertyRelative("selectedClassNameInd");
			storedInstanceProp = property.FindPropertyRelative("storedInstance");
			SerializedProperty storedName = property.FindPropertyRelative("storedClassNameInd");

			// get the type. it's important for the height
			Type typeStoredInAttr = ((ParentClassAttribute)attribute).t;
			List<Type> types = new List<Type>(Assembly.GetAssembly(typeStoredInAttr).GetTypes().Where(theType => theType.IsClass && !theType.IsAbstract && theType.IsSubclassOf(typeStoredInAttr)));
			allClasses = new List<string>( types.Select(t => t.ToString()));
			allClasses.Insert(0, "null");
			// get the enum we show
			int incoming = incomingNameProp.intValue;
			if (incoming == -1)
			{
				storedName.intValue = (incoming);
				storedInstanceProp.SetValue(null);
				return 18f;
			}
			Type childClassType = types[incoming];
			if (storedName.intValue != incoming)
			{
				storedName.intValue = (incoming);
				//storedInstanceProp.managedReferenceValue = null;
				storedInstanceProp.managedReferenceValue = (Activator.CreateInstance(childClassType));
				storedInstanceProp.serializedObject.ApplyModifiedProperties();
			}
			// height is +1 for the enum that alwasy remains
			return (childClassType.GetFields().Length + 2) * 18f;
		}
		catch (Exception)
		{
			Debug.Log("this too");
			return 18;
		}
		
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		try
		{
			EditorGUI.BeginProperty(position, label, property);
			position.height = 18;// height is set to full class, but we only need sections, since we will increase its y
								 //// draw the enum	
			incomingNameProp.intValue = EditorGUI.Popup(position, incomingNameProp.intValue + 1, allClasses.ToArray()) - 1;
			if (incomingNameProp.intValue == -1)
			{
				EditorGUI.EndProperty();
				return;
			}
			position.y += position.height;

			//object storedInstance = storedInstanceProp.GetValue();
			//transition = transition.GetType().GetMethod("CastInto").MakeGenericMethod(transitionType).Invoke(null, new[] { transition });
			//// get the transition from the serializedProperty and then cast it
			foreach (SerializedProperty prop in storedInstanceProp)
			{
				//prop.FieldFromField(setObj, position);
				if (prop.name != "x" && prop.name != "y")
				{
					EditorGUI.PropertyField(position, prop);
					position.y += position.height;
				}

			}

			EditorGUI.EndProperty();
		}
		catch (Exception e )
		{
			Debug.Log(e);
		}
		
	}
}

[CustomPropertyDrawer(typeof(ComponentSelectorAttribute))]
public class ComponenentSelectorDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if (((ComponentSelectorAttribute)attribute).obj)
		{
			MonoBehaviour[] components = ((ComponentSelectorAttribute)attribute).obj.GetComponents<MonoBehaviour>();

			// set the index to what is already selected
			int setInd = Array.FindIndex(components, (x) => x == property.objectReferenceValue);
			if (setInd != -1)
			{
				((ComponentSelectorAttribute)attribute).ind = setInd;
			}

			setInd = components.Length;
			System.Collections.Generic.List<string> componentNames = new System.Collections.Generic.List<string>(setInd + 1);
			// used to differentiate componenet names when there are multiple (bc they always have the same name even when there are multiple)
			int[] compCounts = new int[setInd];

			// create the list of component names
			int i = -1;
			while (++i != setInd)
			{
				string name = components[i].ToString();
				componentNames.Add(componentNames.Contains(name) ? name + ++compCounts[i] : name);
			}
			componentNames.Add("removeObject");

			// make dropdown
			((ComponentSelectorAttribute)attribute).ind = Mathf.Max(
				0
				, EditorGUI.Popup(
					position
					, property.displayName
					, ((ComponentSelectorAttribute)attribute).ind
					, componentNames.ToArray()));

			// if they selected the last option (remove)
			if (((ComponentSelectorAttribute)attribute).ind == setInd)
			{
				((ComponentSelectorAttribute)attribute).obj = null;
				property.objectReferenceValue = null;
				return;
			}

			// link to component
			property.objectReferenceValue = components[((ComponentSelectorAttribute)attribute).ind];
		}
		else
		{// allow the initial component to come from ANY object
			if (property.objectReferenceValue)
				((ComponentSelectorAttribute)attribute).obj = ((MonoBehaviour)property.objectReferenceValue).gameObject;
			else
				EditorGUI.PropertyField(position, property);
		}
	}
}

static class SerPropExt
{
	public static string AsStringValue(this SerializedProperty property)
	{
		switch (property.propertyType)
		{
			case SerializedPropertyType.String:
				return property.stringValue;
			case SerializedPropertyType.Character:
			case SerializedPropertyType.Integer:
				if (property.type == "char") return System.Convert.ToChar(property.intValue).ToString();
				return property.intValue.ToString();
			case SerializedPropertyType.ObjectReference:
				return property.objectReferenceValue != null ? property.objectReferenceValue.ToString() : "null";
			case SerializedPropertyType.Boolean:
				return property.boolValue.ToString();
			case SerializedPropertyType.Enum:
				return property.enumNames[property.enumValueIndex];
			default:
				return string.Empty;
		}
	}
}

[CustomPropertyDrawer(typeof(ConditionalFieldAttribute))]
public class ConditionalFieldAttributeDrawer : PropertyDrawer
{

	private ConditionalFieldAttribute _attribute;
	private bool _toShow;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		_attribute = ((ConditionalFieldAttribute)attribute);
		if (!(_attribute.PropertyToCheck == null || _attribute.PropertyToCheck == string.Empty))
		{
			var conditionProperty = FindPropertyRelative(property, _attribute.PropertyToCheck);
			if (conditionProperty != null)
			{
				if( conditionProperty.propertyType == SerializedPropertyType.Boolean)
				{
					_toShow = _attribute.reverse?!conditionProperty.boolValue: conditionProperty.boolValue;
				}
				else if (conditionProperty.propertyType == SerializedPropertyType.ObjectReference)
				{
					_toShow = _attribute.reverse?! conditionProperty.objectReferenceValue: conditionProperty.objectReferenceValue;
				}
				else
				{
					Debug.LogWarning("Dot know how to handle type: " + conditionProperty.propertyType);
					
				}

				if(_toShow)
					return EditorGUI.GetPropertyHeight(property);
			}
			else
			{
				Debug.LogWarning("Property name: " + _attribute.PropertyToCheck + " is not a field in the current object.");
			}
		}
		else
		{
			Debug.LogWarning("Property to check was not set");
		}
		return 0;
	}

	// TODO: Skip array fields
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if(_toShow)
		EditorGUI.PropertyField(position, property, label, true);
	}

	private SerializedProperty FindPropertyRelative(SerializedProperty property, string toGet)
	{
		if (property.depth == 0) return property.serializedObject.FindProperty(toGet);

		var path = property.propertyPath.Replace(".Array.data[", "[");
		var elements = path.Split('.');
		SerializedProperty parent = null;


		for (int i = 0; i < elements.Length - 1; i++)
		{
			var element = elements[i];
			int index = -1;
			if (element.Contains("["))
			{
				index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
				element = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
			}

			parent = i == 0 ?
				property.serializedObject.FindProperty(element) :
				parent.FindPropertyRelative(element);

			if (index >= 0) parent = parent.GetArrayElementAtIndex(index);
		}

		return parent.FindPropertyRelative(toGet);
	}

}

[CustomPropertyDrawer(typeof(RequiredComponentAttribute))]
public class RequiredComponentDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.PropertyField(position, property);

		MonoBehaviour mono = property.serializedObject.targetObject as MonoBehaviour;
		//if(fieldInfo.FieldType.IsSubclassOf(typeof(Component)) )
		if (typeof(Component).IsAssignableFrom(fieldInfo.FieldType))
		{
			if (property.objectReferenceValue == null)
			{
				Component comp = mono.GetComponent(fieldInfo.FieldType);

				if (comp == null)
				{
					comp = mono.gameObject.AddComponent(fieldInfo.FieldType);
				}

				property.objectReferenceValue = comp;
				//property.serializedObject.ApplyModifiedProperties(); // not sure if this is really needed
			}
		}
		else
		{
			Debug.LogError("Field <b>" + fieldInfo.Name + "</b> of " + mono.GetType() + " is not a component!", mono);
		}
	}
}

public enum COLLIDER_TYPE
{
	None,
	Box,
	Box2D
}

[CustomPropertyDrawer(typeof(Add2Dor3DColliderAttribute))]
public class Add2Dor3DColliderDrawer : PropertyDrawer
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.PropertyField(position, property);

		MonoBehaviour mono = property.serializedObject.targetObject as MonoBehaviour;
		switch ((COLLIDER_TYPE)property.enumValueIndex)
		{
			case COLLIDER_TYPE.Box:
				if (mono.GetComponent(typeof(BoxCollider)) == null)
				{
					GameObject.DestroyImmediate(mono.gameObject.GetComponent<Collider2D>());
					mono.gameObject.AddComponent(typeof(BoxCollider));
				}
				break;
			case COLLIDER_TYPE.Box2D:
				if (mono.GetComponent(typeof(BoxCollider2D)) == null)
				{
					GameObject.DestroyImmediate(mono.gameObject.GetComponent<Collider>());
					mono.gameObject.AddComponent(typeof(BoxCollider2D));
				}
				break;
			case COLLIDER_TYPE.None:
			default:
				break;
		}
	}
}

public static class SerializedPropertyExt
{
	/// <summary>
	/// Get the object the serialized property holds by using reflection
	/// </summary>
	/// <typeparam name="T">The object type that the property contains</typeparam>
	/// <param name="property"></param>
	/// <returns>Returns the object type T if it is the type the property actually contains</returns>
	public static object GetValue(this SerializedProperty property)
	{
		if (property != null)
			return GetNestedObject(property.propertyPath, GetSerializedPropertyRootComponent(property));
		return null;
	}

	/// <summary>
	/// Set the value of a field of the property with the type T
	/// </summary>
	/// <typeparam name="T">The type of the field that is set</typeparam>
	/// <param name="property">The serialized property that should be set</param>
	/// <param name="value">The new value for the specified property</param>
	/// <returns>Returns if the operation was successful or failed</returns>
	public static bool SetValue(this SerializedProperty property, object value)
	{
		object obj = GetSerializedPropertyRootComponent(property);
		//Iterate to parent object of the value, necessary if it is a nested object
		string[] fieldStructure = property.propertyPath.Split('.');
		for (int i = 0; i < fieldStructure.Length - 1; i++)
		{
			obj = GetFieldOrPropertyValue(fieldStructure[i], obj);
		}
		string fieldName = fieldStructure.Last();

		return SetFieldOrPropertyValue(fieldName, obj, value);

	}

	/// <summary>
	/// Get the component of a serialized property
	/// </summary>
	/// <param name="property">The property that is part of the component</param>
	/// <returns>The root component of the property</returns>
	public static Component GetSerializedPropertyRootComponent(SerializedProperty property)
	{
		if (property?.serializedObject != null)
			return (Component)property.serializedObject.targetObject;
		else return null;
	}

	/// <summary>
	/// Iterates through objects to handle objects that are nested in the root object
	/// </summary>
	/// <typeparam name="T">The type of the nested object</typeparam>
	/// <param name="path">Path to the object through other properties e.g. PlayerInformation.Health</param>
	/// <param name="obj">The root object from which this path leads to the property</param>
	/// <param name="includeAllBases">Include base classes and interfaces as well</param>
	/// <returns>Returns the nested object casted to the type T</returns>
	public static object GetNestedObject(string path, object obj, bool includeAllBases = false)
	{
		foreach (string part in path.Split('.'))
		{
			obj = GetFieldOrPropertyValue(part, obj, includeAllBases);
		}
		return obj;
	}

	public static object GetFieldOrPropertyValue(string fieldName, object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
	{
		FieldInfo field = obj.GetType().GetField(fieldName, bindings);
		if (field != null) return field.GetValue(obj);

		PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
		if (property != null) return property.GetValue(obj, null);

		if (includeAllBases)
		{

			foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType()))
			{
				field = type.GetField(fieldName, bindings);
				if (field != null) return field.GetValue(obj);

				property = type.GetProperty(fieldName, bindings);
				if (property != null) return property.GetValue(obj, null);
			}
		}

		return default;
	}

	public static bool SetFieldOrPropertyValue(string fieldName, object obj, object value, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
	{
		FieldInfo field = obj.GetType().GetField(fieldName, bindings);
		if (field != null)
		{
			field.SetValue(obj, value);
			return true;
		}

		PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
		if (property != null)
		{
			property.SetValue(obj, value, null);
			return true;
		}

		if (includeAllBases)
		{
			foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType()))
			{
				field = type.GetField(fieldName, bindings);
				if (field != null)
				{
					field.SetValue(obj, value);
					return true;
				}

				property = type.GetProperty(fieldName, bindings);
				if (property != null)
				{
					property.SetValue(obj, value, null);
					return true;
				}
			}
		}
		return false;
	}

	public static IEnumerable<Type> GetBaseClassesAndInterfaces(this Type type, bool includeSelf = false)
	{
		List<Type> allTypes = new List<Type>();

		if (includeSelf) allTypes.Add(type);

		if (type.BaseType == typeof(object))
		{
			allTypes.AddRange(type.GetInterfaces());
		}
		else
		{
			allTypes.AddRange(
					Enumerable
					.Repeat(type.BaseType, 1)
					.Concat(type.GetInterfaces())
					.Concat(type.BaseType.GetBaseClassesAndInterfaces())
					.Distinct());
		}

		return allTypes;
	}

	public static void FieldFromField(this FieldInfo field, object obj, Rect position, Component targetObj = null)
	{
		if (obj != null)
		{
			field.SetValue(obj, MakeFieldFromType(field.GetValue(obj), position, field));
		}
	}

	public static object MakeFieldFromType(object value, Rect position, FieldInfo field, Component targetObj = null)
	{
		switch (value)
		{
			case float f:
				return EditorGUI.FloatField(position, new GUIContent(field.Name, ""), f);
			case Color c:
				return EditorGUI.ColorField(position, new GUIContent(field.Name, ""), c);
			case string t:
				return EditorGUI.TextField(position, new GUIContent(field.Name, ""), t);
			case Vector2 v2:
				return EditorGUI.Vector2Field(position, new GUIContent(field.Name, ""), v2);
			case Vector3 v3:
				return EditorGUI.Vector3Field(position, new GUIContent(field.Name, ""), v3);
			case bool b:
				return EditorGUI.Toggle(position, new GUIContent(field.Name, ""), b);
			case int i:
				return EditorGUI.IntField(position, new GUIContent(field.Name, ""), i);
			case Enum e:
				return EditorGUI.EnumPopup(position, new GUIContent(field.Name, ""), e);
			default:
				return EditorGUI.ObjectField(position, new GUIContent(field.Name, ""), (UnityEngine.Object)value, field.FieldType, false);
		}
	}
}
#endif