using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ThirdPersonMovement))]
public class ThirdPMovementEditor : Editor
{

	public override void OnInspectorGUI()
	{
		// Draw the default inspector (all serialized fields, etc.)
		DrawDefaultInspector();


		// Add a space (optional)
		EditorGUILayout.Space();

		// Get a reference to the target object.
		var thirdPersonScript = (ThirdPersonMovement)target;
		// Add your custom button at the end.
		if (GUILayout.Button("Switch flying"))
		{
			thirdPersonScript.SetIsFlying();
		}
	}
}
