using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class AutoEnhancer : MonoBehaviour
{
	static LOD[] defLod =  new LOD[] {
		new LOD{screenRelativeTransitionHeight = .35f },
		new LOD{screenRelativeTransitionHeight = .028f}
	};
	public GameObject ss;
#if UNITY_EDITOR
	[MenuItem("Impostor/Prepare All Children to be baked")]
	static public void AddDefaultComponentEditor()
	{
		if (Selection.activeGameObject != null)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<MeshFilter>())
			{
				if (item.gameObject.name != "impostor" && item.sharedMesh.vertexCount > 400)
				{
					if(!item.GetComponent<LODGroup>())
					item.gameObject.AddComponent<LODGroup>();
					defLod[0].renderers = new Renderer[] { item.GetComponent<MeshRenderer>() };
					item.GetComponent<LODGroup>().SetLODs(defLod);
					if(!item.GetComponent<AmplifyImpostors.AmplifyImpostor>())
					item.gameObject.AddComponent<AmplifyImpostors.AmplifyImpostor>();
				}
			}
		}
	}

	[MenuItem("Impostor/Prepare Object to be baked")]
	static public void AddLODAndAmpl()
	{
		if (Selection.activeGameObject != null)
		{
			if (Selection.activeGameObject.name != "impostor")
			{
				if (!Selection.activeGameObject.GetComponent<LODGroup>())
					Selection.activeGameObject.AddComponent<LODGroup>().SetLODs(defLod);
				defLod[0].renderers = Selection.activeGameObject.GetComponentsInChildren<MeshRenderer>();
				if (!Selection.activeGameObject.GetComponent<AmplifyImpostors.AmplifyImpostor>())
					Selection.activeGameObject.AddComponent<AmplifyImpostors.AmplifyImpostor>();
			}
		}
	}

	[MenuItem("Impostor/Remove Unused LOD")]
	static public void FixLOD()
	{
		if (Selection.activeGameObject != null)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<LODGroup>())
			{
				if (item.lodCount < 2 || item.GetLODs()[0].renderers == null || item.GetLODs()[0].renderers.Length == 0 || item.GetLODs()[0].renderers[0] == null
					|| item.GetLODs()[1].renderers == null || item.GetLODs()[1].renderers.Length == 0 || item.GetLODs()[1].renderers[0] == null)
				{
					DestroyImmediate(item.GetComponent<AmplifyImpostors.AmplifyImpostor>());
					DestroyImmediate(item);
				}
			}
		}
	}

	[MenuItem("Impostor/Print highest Poly mesh in children")]
	static public void PrintHighestPoly()
	{

		if (Selection.activeGameObject != null)
		{
			List<MeshFilter> sharedMeshes = new List<MeshFilter>();
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<MeshFilter>())
			{
				if (!item.GetComponent<LODGroup>())
				{
					sharedMeshes.Add(item);
				}
			}
			sharedMeshes.Sort((x, y) => { if (!x) return 1;if (!y) return -1; if (x.sharedMesh.vertexCount < y.sharedMesh.vertexCount) return 1; else return -1; });
			int i = -1; while (++i!= 10)
			{
				if (sharedMeshes.Count > i)
				{
					Debug.Log(sharedMeshes[i].gameObject.name);
				}
			}
		}
	}

	[MenuItem("Impostor/Revert Instanced meshes")]
	static public void RevertMeshInstance()
	{
		if (Selection.activeGameObject != null)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<MeshFilter>())
			{
				var sp = new SerializedObject(item).FindProperty("m_Mesh");
				if(sp.isInstantiatedPrefab)
					PrefabUtility.RevertPropertyOverride(sp, InteractionMode.UserAction);
			}
		}
	}

	[MenuItem("Impostor/Select instanced meshses")]
	static public void SelectInstanced()
	{
		List<GameObject> objs = new List<GameObject>(); 
		if (Selection.activeGameObject != null)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<MeshFilter>())
			{
				if (item.sharedMesh == null || item.sharedMesh.name == null)
				{
					objs.Add(item.gameObject);
				}
				else if (item.sharedMesh.name.Contains("Instance"))
				{
					Debug.Log(item.sharedMesh.name);
					objs.Add(item.gameObject);
				}
			}
		}
		Selection.objects = objs.ToArray();
	}

	[MenuItem("Impostor/Assign Impostor Data to duplicate impostors")]
	static public void DuplicateImpostor()
	{
		if (Selection.activeGameObject != null)
		{
			Dictionary<string, Transform> parentNameToImpostor = new Dictionary<string, Transform>();
			foreach (Transform item in Selection.activeTransform)
			{
				Transform child = item;
				while(child.childCount!=0)
				{
					child = child.GetChild(0);
				}
				parentNameToImpostor.Add(child.parent.name, child);
			}

			foreach (Transform item in Selection.activeTransform.parent)
			{
				duplicateRec(item, parentNameToImpostor);
			}

		}
	}

	static void duplicateRec(Transform child, Dictionary<string, Transform> parentNameToImpostor)
	{
		if (child.childCount !=0)
		{
			foreach (Transform item in child)
			{
				duplicateRec(item, parentNameToImpostor);
			}
		}
		if (parentNameToImpostor.TryGetValue(child.parent.name, out Transform impostor))
		{

			if (child.GetComponent<MeshFilter>())
			{
				child.GetComponent<MeshFilter>().sharedMesh = impostor.GetComponent<MeshFilter>().sharedMesh;
			}
			if (child.GetComponent<MeshRenderer>())
			{
				child.GetComponent<MeshRenderer>().sharedMaterial = impostor.GetComponent<MeshRenderer>().sharedMaterial;
			}
		}
	}

	[MenuItem("Tools/RemoveWhiteSpaceFromChildren")]
	static void RemoveWhiteSpace()
	{
		if (Selection.activeGameObject != null)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<Transform>())
			{
				item.gameObject.name = item.gameObject.name.Trim();
			}
		}
	}
#endif
}
