﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class Remover
{

    [MenuItem("Tools/Remove Dulicate Components")]
    private static void NewMenuOption()
    {
		foreach (Transform item in GameObject.FindObjectsOfType<Transform>())
		{
			HashSet<System.Type> compTypes = new HashSet<System.Type>();
			HashSet<System.Type> typesToIgnore = new HashSet<System.Type>();
			foreach (var mono in item.GetComponents<Component>())
			{
				System.Type monoType = mono.GetType();
				if (!typesToIgnore.Contains(monoType) &&!compTypes.Add(monoType))
				{
					Selection.activeObject = item.gameObject;// if only this worked...
					if (EditorUtility.DisplayDialog("Duplicate Found", item.name + " has multiple " + mono + ". Would you like to delete 1?", "Yes", "No"))
					{
						EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
						GameObject.DestroyImmediate(mono);
						Debug.Log("Deleted " + mono + " From " + item);
					}
					else
					{
						typesToIgnore.Add(monoType);
					}
				}
			}
		}
	}

	[MenuItem("Tools/Remove null components")]
	private static void RemoveNullComponents()
	{
		foreach (Transform item in Resources.FindObjectsOfTypeAll<Transform>())
		{
			foreach (var mono in item.GetComponents<Component>())
			{
				if (mono == null)
				{
					Selection.activeGameObject = item.gameObject;
					return;
				}
			}
		}
	}

	[MenuItem("Tools/Print names of selected objects")]
	private static void PrintNames()
	{
		if (Selection.activeGameObject)
		{
			List<string> names = new List<string>();
			foreach (var item in Selection.gameObjects)
			{
				names.Add(item.name);
			}

			Debug.Log(string.Join("\n", names));
		}
	}

	[MenuItem("Tools/Find duplicate names in children")]
	private static void FindDuplicateNames()
	{
		Dictionary<string, GameObject> names = new Dictionary<string, GameObject>(1000);
		List<GameObject> objs = new List<GameObject>();
		if (Selection.activeGameObject)
		{
			foreach (var item in Selection.activeGameObject.GetComponentsInChildren<Transform>(true))
			{
				if (names.ContainsKey(item.name))
					{
						Debug.Log(item.name);
						objs.Add(item.gameObject);
						objs.Add(names[item.name]);
					}
					else
					{
						names.Add(item.name, item.gameObject);
					}
			}
			Selection.objects = objs.ToArray();
		}
	}

	[MenuItem("Tools/Find layer")]
	private static void FindByLayer()
	{
		if (Selection.activeGameObject)
		{
			foreach (var coll in Selection.activeGameObject.GetComponentsInChildren<Collider>(true))
			{
				if (coll.gameObject.layer == 1)
				{
					Debug.Log(coll.gameObject.name);
				}
			}
		}
	}
}