﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using System.Text;
using TMPro;
using System.Net.Sockets;
using System.Net;
using UnityEngine.Networking;

public static class Tools
{
	static public long TestPerformance(Action action, int funcCount)
	{
		System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
		int i = -1;
		sw.Start();
		while (++i != funcCount)
		{
			action.Invoke();
		}
		sw.Stop();
		UnityEngine.Debug.Log(sw.ElapsedTicks);
		return sw.ElapsedTicks;
	}
	public static string ReplaceFirst(this string text, string search, string replace)
	{
		int pos = text.IndexOf(search);
		if (pos < 0)
		{
			return text;
		}
		return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
	}

	static public string GetReplacedDHash(this string s, string val)
	{
		return s.ReplaceFirst("##", val);
	}

	static public string GetReplacedDHashWithNumber(this string s, float num)
	{
		return s.ReplaceFirst("##", num.ToString("f0"));
	}

	static public void ReplaceDHash(ref string s, string val)
	{
		s = s.ReplaceFirst("##", val);
	}

	static public void ReplaceDHashWithNumber(ref string s, float num)
	{
		s= s.ReplaceFirst("##", num.ToString("f0"));
	}

	static public T[] DeepCloneArray<T>(this T[] array) where T : class, ICloneable
	{
		if (array == null) return null;
		T[] clone = new T[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			clone[i] = (T)array[i].Clone();
		}
		return clone;
	}
	public static T[] InsertIntoArray<T>(this T[] array, int index, T item)
	{
		if (array == null)
			throw new ArgumentNullException(nameof(array));
		if (index < 0 || index > array.Length)
			throw new ArgumentOutOfRangeException(nameof(index));

		// Create a new array with one additional slot
		T[] newArray = new T[array.Length + 1];

		// Copy elements before the insertion point
		Array.Copy(array, 0, newArray, 0, index);

		// Insert the new item
		newArray[index] = item;

		// Copy elements after the insertion point
		Array.Copy(array, index, newArray, index + 1, array.Length - index);
		return newArray;
	}

	static public float Clamp(float input, float min, float max) { return (input < min) ? min : (input > max) ? max : input; }
	static public int Clamp(int input, int min, int max) { return (input < min) ? min : (input > max) ? max : input; }

	static public float Map(float value, float minlow, float minHigh, float maxLow, float maxHigh)
	{

		return (maxLow + (maxHigh - maxLow) * (value - minlow) / (minHigh - minlow));
	}

	static public GameObject GetNearestObject(GameObject point, params GameObject[] objects)
	{
		return GetNearestObject(point.transform, objects).gameObject;
	}

	static public GameObject GetNearestObject(GameObject point, params Transform[] objects)
	{
		return GetNearestObject(point.transform, objects).gameObject;
	}

	static public Transform GetNearestObject(Transform point, params GameObject[] objects)
	{
		return GetNearestObject(point, objects.Select(obj => obj.transform) as Transform[]);
	}

	static public float SqdDisXZ(Vector3 p1, Vector3 p2)
	{
		return DirVectXZ(p1, p2).sqrMagnitude;
	}

	static public float DisXZ(Vector3 p1, Vector3 p2)
	{
		return DirVectXZ(p1, p2).magnitude;
	}

	static public Vector2 DirVectXZ(Vector3 to, Vector3 from)
	{
		return (V2FromXZ(to) - V2FromXZ(from));
	}

	static public Vector2 V2FromXZ(Vector3 v3)
	{
		return new Vector2(v3.x, v3.z);
	}

	static public Vector3 XZFromV2(Vector3 v2)
	{
		return new Vector3(v2.x, 0f, v2.y);
	}

	static public Transform GetNearestObject(Transform point, params Transform[] objects)
	{
		if (objects == null || objects.Length == 0)
			return null;
		float min = float.MaxValue;
		Transform closest = objects[0];
		foreach (Transform obj in objects)
		{
			float dis = Vector3.SqrMagnitude(obj.position - point.position);
			if (dis < min)
			{
				min = dis;
				closest = obj;
			}
		}
		return closest;
	}

	public static IEnumerator LirpScrollToBottOfTrans(ScrollRect sv, RectTransform target, float spd = 500f)
	{
		if (sv.content.rect.height < sv.viewport.rect.height)
		{
			yield break;
		}
		yield return null;
		Canvas.ForceUpdateCanvases();
		yield return null;
		Vector2 curr = new Vector2(0, sv.content.anchoredPosition.y);
		float targetPos = ((Vector2)sv.transform.InverseTransformPoint(sv.content.position)).y - ((Vector2)sv.transform.InverseTransformPoint(target.position)).y - sv.viewport.rect.height + target.rect.height;
		float sign = MathF.Sign(targetPos - curr.y);
		while (Mathf.Abs(Mathf.Abs(curr.y) - Mathf.Abs(targetPos)) > (spd * Time.deltaTime) && sv.verticalNormalizedPosition > 0f && sv.verticalNormalizedPosition < 1f)
		{
			curr.y += spd * Time.deltaTime * sign;
			sv.content.anchoredPosition = curr;
			if (sv.verticalNormalizedPosition > 1)
			{
				sv.verticalNormalizedPosition = 1;
				break;
			}
			else if (sv.verticalNormalizedPosition < 0)
			{
				sv.verticalNormalizedPosition = 0;
				break;
			}
			yield return null;
		}
		curr.y = targetPos;
		sv.content.anchoredPosition = curr;
		sv.velocity = Vector2.zero;
		if (sv.verticalNormalizedPosition > 1)
		{
			sv.verticalNormalizedPosition = 1;
		}
		else if (sv.verticalNormalizedPosition < 0)
		{
			sv.verticalNormalizedPosition = 0;
		}
	}

	/// <summary>
	/// This is not a method and must be used as a Coroutine
	/// </summary>
	/// <param name="movingPiece"></param>
	/// <param name="pos"></param>
	/// <param name="rot"></param>
	/// <param name="seconds"></param>
	/// <returns></returns>
	public static IEnumerator LirpToMovingSpot(Transform movingPiece, Transform targetTrans, float seconds = 1f)
	{
		float endTime = Time.time + seconds - Time.deltaTime;
		while (Time.time < endTime)
		{
			float percent = (endTime - Time.time) / seconds;
			if (!movingPiece || !targetTrans)
				yield break;
			movingPiece.position = Vector3.LerpUnclamped(targetTrans.position, movingPiece.position, percent);
			movingPiece.rotation = Quaternion.LerpUnclamped(targetTrans.rotation, movingPiece.rotation, percent);
			yield return null;
		}
		movingPiece.position = targetTrans.position;
		movingPiece.rotation = targetTrans.rotation;
	}

	/// <summary>
	/// This is not a method and must be used as a Coroutine
	/// </summary>
	/// <param name="movingPiece"></param>
	/// <param name="pos"></param>
	/// <param name="rot"></param>
	/// <param name="seconds"></param>
	/// <returns></returns>
	public static IEnumerator LirpToSpot(Transform movingPiece, Vector3 pos, Quaternion rot, float seconds = 1f)
	{
		float endTime = Time.time + seconds - Time.deltaTime;
		while (Time.time < endTime)
		{
			float percent = (endTime - Time.time) / seconds;
			movingPiece.position = Vector3.LerpUnclamped(pos, movingPiece.position, percent);
			movingPiece.rotation = Quaternion.LerpUnclamped(rot, movingPiece.rotation, percent);
			yield return null;
		}
		movingPiece.position = pos;
		movingPiece.rotation = rot;
	}

	/// <summary>
	/// This is not a method and must be used as a Coroutine
	/// </summary>
	/// <param name="movingPiece"></param>
	/// <param name="pos"></param>
	/// <param name="rot"></param>
	/// <param name="seconds"></param>
	/// <returns></returns>
	public static IEnumerator LirpToLocal(Transform movingPiece, Vector3 localPos, Quaternion localRot, float seconds = 1f)
	{
		float endTime = Time.time + seconds - Time.deltaTime;// adds 1 more frame for the yield null after the lerp
		while (Time.time < endTime)
		{
			float percent = (endTime - Time.time) / seconds;
			movingPiece.localPosition = Vector3.LerpUnclamped(localPos, movingPiece.localPosition, percent);
			movingPiece.localRotation = Quaternion.LerpUnclamped(localRot, movingPiece.localRotation, percent);
			yield return null;
		}
		movingPiece.localPosition = localPos;
		movingPiece.localRotation = localRot;
	}

	/// <summary>
	/// This is not a method and must be used as a Coroutine
	/// </summary>
	/// <param name="movingPiece"></param>
	/// <param name="pos"></param>
	/// <param name="rot"></param>
	/// <param name="seconds"></param>
	/// <returns></returns>
	public static IEnumerator LirpToSpotBySpd(Transform movingPiece, Vector3 pos, Quaternion rot, float metersPerSec = 1f, float anglePerSec = 50f)
	{
		Vector3 dir = (pos - movingPiece.position).normalized * metersPerSec;
		Quaternion startRot = movingPiece.rotation;
		float currAngle = 0;
		anglePerSec /= Quaternion.Angle(startRot, rot);
		while (true)
		{
			bool canExit = true;
			if (Vector3.SqrMagnitude(movingPiece.position - pos) > metersPerSec * metersPerSec * Time.deltaTime)
			{
				canExit = false;
				movingPiece.position += dir * metersPerSec * Time.deltaTime;
			}
			if (Quaternion.Angle(movingPiece.rotation, rot) > .1f)
			{
				canExit = false;
				movingPiece.rotation = Quaternion.Lerp(startRot, rot, (currAngle += anglePerSec * Time.deltaTime));
			}
			if (canExit)
			{
				break;
			}
			yield return null;
		}
		movingPiece.position = pos;
		movingPiece.rotation = rot;
	}

	public static IEnumerator LocalLirpToSpot(Transform movingPiece, Vector3 localPos, Quaternion localRot, float seconds = 1f)
	{
		float endTime = Time.time + seconds - Time.deltaTime;
		while (Time.time < endTime)
		{
			float percent = (endTime - Time.time) / seconds;
			movingPiece.localPosition = Vector3.LerpUnclamped(localPos, movingPiece.localPosition, percent);
			movingPiece.localRotation = Quaternion.LerpUnclamped(localRot, movingPiece.localRotation, percent);
			yield return null;
		}
		movingPiece.localPosition = localPos;
		movingPiece.localRotation = localRot;
	}

	public static Quaternion QuaternionLirp(Quaternion p, Quaternion q, float t, bool shortWay)
	{
		if (shortWay)
		{
			return Quaternion.Lerp(p, q, t);
		}

		Quaternion r = Quaternion.identity;
		r.x = p.x * (1f - t) + q.x * (t);
		r.y = p.y * (1f - t) + q.y * (t);
		r.z = p.z * (1f - t) + q.z * (t);
		r.w = p.w * (1f - t) + q.w * (t);
		return r;
	}

	public static IEnumerator SpawnRandomsOnCanvas(GameObject _object, int _count/*, int _gems*/, float _timeSeperation = .2f)
	{
		Camera main = Camera.main;
		Canvas canvas = GameObject.FindObjectOfType<Canvas>();
		bool overlayCanvas = canvas.renderMode != RenderMode.ScreenSpaceOverlay ? false : true;
		while (--_count != -1)
		{
			Vector3 randScreenPos = new Vector3(UnityEngine.Random.value, UnityEngine.Random.value);
			GameObject.Instantiate(_object
				, overlayCanvas ? main.ViewportToScreenPoint(randScreenPos) : main.ViewportToWorldPoint(randScreenPos)
				, Quaternion.identity
				, canvas.transform);
			yield return new WaitForSeconds(_timeSeperation);
		}

		yield break;
	}

	public static IEnumerator Move2Pos(Transform _target, Vector3 _endPos, float _dur = .4f)
	{
		Vector3 toFromVector = (_endPos - _target.position) * Time.fixedDeltaTime / _dur;
		_dur += Time.time;
		while (Time.time < _dur)
		{
			_target.position += toFromVector;
			yield return new WaitForSeconds(Time.fixedDeltaTime);
		}
		_target.position = _endPos;

		yield break;
	}

	public static IEnumerator SwapConstSpd(Transform _first, Transform _second, float _dur = .4f)
	{
		Vector3 start = _first.position, end = _second.position, toFromVector = (end - start) * Time.fixedDeltaTime / _dur;

		_dur += Time.time;
		while (Time.time < _dur)
		{
			_first.position += toFromVector;
			_second.position -= toFromVector;
			yield return new WaitForSeconds(Time.fixedDeltaTime);
		}
		_first.position = end;
		_second.position = start;

		yield break;
	}

	public static void SetTextObjectTxt(GameObject _object, string _txt)
	{
		foreach (var graphic in _object.GetComponents<Component>())
		{
			var txtProp = graphic.GetType().GetProperty("text");
			if (txtProp != null)
			{
				txtProp.SetValue(graphic, _txt);
			}
		}
	}

	public static void SetPropOfObj(GameObject _object, string _propName, System.Object _value)
	{
		foreach (var graphic in _object.GetComponents<Component>())
		{
			var txtProp = graphic.GetType().GetProperty(_propName);
			if (txtProp != null)
			{
				txtProp.SetValue(graphic, _value);
			}
		}
	}

	public static float GetDirctionalDistance(Vector3 from, Vector3 to, Vector3 dir)
	{
		return Vector3.Dot(to - from, dir);
	}

	public static float GetDirctionalDistanceXZ(Vector3 from, Vector3 to, Vector3 dir)
	{
		return Vector2.Dot(V2FromXZ(to) - V2FromXZ(from), V2FromXZ(dir));
	}

	public static float GetDirctionalDistanceXZ(Vector3 fromTo, Vector3 dir)
	{
		return Vector2.Dot(fromTo, V2FromXZ(dir));
	}

	public static float GetDirctionalDistance(Vector2 from, Vector2 to, Vector2 dir)
	{
		return Vector2.Dot(to - from, dir);
	}

	public static float GetDirctionalDistance(Vector2 fromTo, Vector2 dir)
	{
		return Vector2.Dot(fromTo, dir);
	}

	public static Vector2 V2Perpendicular(Vector2 vec)
	{
		return new Vector2(vec.y, -vec.x);
	}

	public static DateTime GetGoogleEpoch()
	{
		return new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc);

	}

	public static double GetNetMillisSinceGoogleEpoch()
	{
		const string ntpServer = "time.google.com";
		byte[] ntpData = new byte[48];
		ntpData[0] = 0x1B; // NTP request header

		try
		{
			// Get only IPv4 addresses
			IPAddress[] addresses = Dns.GetHostAddresses(ntpServer);
			IPAddress ipv4Address = Array.Find(addresses, a => a.AddressFamily == AddressFamily.InterNetwork);

			if (ipv4Address == null)
				throw new Exception("No IPv4 address found for the NTP server.");

			IPEndPoint endPoint = new IPEndPoint(ipv4Address, 123);

			using (UdpClient client = new UdpClient(AddressFamily.InterNetwork))
			{
				client.Connect(endPoint);
				client.Send(ntpData, ntpData.Length);
				ntpData = client.Receive(ref endPoint);
			}

			ulong intPart = BitConverter.ToUInt32(ntpData, 40);
			ulong fractPart = BitConverter.ToUInt32(ntpData, 44);

			intPart = SwapEndianness(intPart);
			fractPart = SwapEndianness(fractPart);

			return (intPart * 1000.0) + ((fractPart * 1000.0) / 0x100000000L);

		}
		catch (Exception ex)
		{
			UnityEngine.Debug.LogError("NTP time error: " + ex.Message);
			return (DateTime.Now - GetGoogleEpoch()).Milliseconds; // Fallback to device time
		}
	}

	public static DateTime GetNetworkTime()
	{
		const string ntpServer = "time.google.com";
		byte[] ntpData = new byte[48];
		ntpData[0] = 0x1B; // NTP request header

		try
		{
			// Get only IPv4 addresses
			IPAddress[] addresses = Dns.GetHostAddresses(ntpServer);
			IPAddress ipv4Address = Array.Find(addresses, a => a.AddressFamily == AddressFamily.InterNetwork);

			if (ipv4Address == null)
				throw new Exception("No IPv4 address found for the NTP server.");

			IPEndPoint endPoint = new IPEndPoint(ipv4Address, 123);

			using (UdpClient client = new UdpClient(AddressFamily.InterNetwork))
			{
				client.Connect(endPoint);
				client.Send(ntpData, ntpData.Length);
				ntpData = client.Receive(ref endPoint);
			}

			ulong intPart = BitConverter.ToUInt32(ntpData, 40);
			ulong fractPart = BitConverter.ToUInt32(ntpData, 44);

			intPart = SwapEndianness(intPart);
			fractPart = SwapEndianness(fractPart);

			double milliseconds = (intPart * 1000.0) + ((fractPart * 1000.0) / 0x100000000L);

			return GetGoogleEpoch().AddMilliseconds(milliseconds).ToLocalTime();
		}
		catch (Exception ex)
		{
			UnityEngine.Debug.LogError("NTP time error: " + ex.Message);
			return DateTime.Now; // Fallback to device time
		}
	}

	public static uint SwapEndianness(ulong x)
	{
		return (uint)(((x & 0x000000ff) << 24) + ((x & 0x0000ff00) << 8) +
					  ((x & 0x00ff0000) >> 8) + ((x & 0xff000000) >> 24));
	}

	public static IEnumerator GetTimeHTTP()
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Get("http://worldtimeapi.org/api/timezone/Etc/UTC"))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.result == UnityWebRequest.Result.Success)
			{
				string json = webRequest.downloadHandler.text;
				DateTime time = ParseTime(json);
				Debug.Log("Time from API: " + time);
			}
			else
			{
				Debug.LogError("Failed to fetch time: " + webRequest.error);
			}
		}
	}

	public static DateTime ParseTime(string json)
	{
		int startIndex = json.IndexOf("\"utc_datetime\":\"") + 15;
		int endIndex = json.IndexOf("\",", startIndex);
		string timeString = json.Substring(startIndex, endIndex - startIndex);
		return DateTime.Parse(timeString).ToUniversalTime();
	}

	public static Texture2D OverlayTexture(Texture2D bottomImage, Texture2D topImage)
	{
		Texture2D newtexture = new Texture2D(bottomImage.width, bottomImage.height);
		if (bottomImage == null || topImage == null)
		{
			return null;
		}

		int x = -1; while (++x != topImage.width)
		{
			int y = -1; while (++y != topImage.height)
			{
				int botY = bottomImage.height - topImage.height + y;
				newtexture.SetPixel(x, botY, CombinePixel(topImage.GetPixel(x, y), bottomImage.GetPixel(x, botY)));
			}
		}
		return newtexture;
	}

	private static Color CombinePixel(Color topColor, Color bottomColor)
	{
		if (topColor.a == 1)
		{
			return topColor;
		}
		else if (topColor.a == 0)
		{
			return bottomColor;
		}
		else
		{
			return topColor * (1 - bottomColor.a) * topColor.a + bottomColor * bottomColor.a;
		}
	}
}

public static class ComponentExt
{
	static public string NameWithout_(this Enum toName)
	{
		return toName.ToString().Replace('_', ' ');
	}

	public static bool HasAnswer(this Selectable item)
	{
		if (item is Toggle)
		{
			return ((Toggle)item).isOn;
		}
		else if (item is TMP_InputField)
		{
			return !string.IsNullOrEmpty(((TMP_InputField)item).text);
		}
		else
		{
			UnityEngine.Debug.LogWarning("Unrecognized Selectable UI Type when checking HasAnswer");
			return false;
		}
	}
	public static void ChangeDisabledColor(this Selectable item, Color color)
	{
		var colrBlock = item.colors;
		colrBlock.disabledColor = color;
		item.colors = colrBlock;
	}
	public static void SetTextValues(this Text _textComp, string _txt, Color _color, float _fadeDur = 1f)
	{
		_textComp.text = _txt;
		_textComp.color = _color;
		_textComp.CrossFadeAlpha(0f, _fadeDur, false);
	}

	public static Transform Clear(this Transform _trans)
	{
		foreach (Transform child in _trans)
		{
			GameObject.Destroy(child.gameObject);
		}
		return _trans;
	}

	public static IEnumerator FadeColor(this MaskableGraphic img, float start = 1f, float end = 0f, float sec = 1f)
	{
		var color = img.color;
		color.a = start;
		sec = 1 / sec;
		while (color.a > end)
		{
			img.color = color;
			color.a -= Time.deltaTime * sec;
			yield return null;
		}
		color.a = end;
		img.color = color;
	}

	public static bool IsNullOrEmpty(this string str)
	{
		return string.IsNullOrEmpty(str);
	}
	public static T NextEnum<T>(this T src) where T : struct
	{
		if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

		T[] Arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf<T>(Arr, src) + 1;
		return (Arr.Length == j) ? Arr[0] : Arr[j];
	}

	public static T PrevEnum<T>(this T src) where T : struct
	{
		if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

		T[] Arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf<T>(Arr, src) - 1;
		return (Arr.Length == -1) ? Arr[Arr.Length - 1] : Arr[j];
	}
}

[System.Serializable]
public class SceneSelection
{
	public int index;
	public override string ToString()
	{
		return System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(index));
	}
}

[System.Serializable]
public struct Vector2Ser
{
	public float x, y;


	// Implicit conversion from Vector3 to SerializableVector3
	public static implicit operator Vector2Ser(Vector2 v)
	{
		return new Vector2Ser { x= v.x,y = v.y };
	}

	// Implicit conversion from SerializableVector3 to Vector3
	public static implicit operator Vector2(Vector2Ser v)
	{
		return new Vector2(v.x, v.y);
	}
}

[System.Serializable]
public struct Vector3Ser
{
	float x, y, z;


	// Implicit conversion from Vector3 to SerializableVector3
	public static implicit operator Vector3Ser(Vector3 v)
	{
		return new Vector3Ser { x = v.x, y = v.y, z = v.z };
	}

	// Implicit conversion from SerializableVector3 to Vector3
	public static implicit operator Vector3(Vector3Ser v)
	{
		return new Vector3(v.x, v.y, v.z);
	}
}