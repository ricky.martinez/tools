﻿using UnityEngine;

public class destroytimer : MonoBehaviour
{

	public float timer = -1;
	[Tooltip("Set to true if you want to go inactive INSTEAD of destroying")]
	public bool setInavtice;
	//public float collidertimer;
	// Use this for initialization
	void OnEnable()
	{
		if (timer != -1)
			Invoke("DestroyInSeconds",timer);
		SendMessage("SetTimer", timer, SendMessageOptions.DontRequireReceiver);
	}

	public void DestroyInSeconds()
	{
		if (setInavtice)
			gameObject.SetActive(false);
		else
			Destroy(gameObject);
	}

	private void OnDisable()
	{
		CancelInvoke();
	}
}
