﻿using UnityEngine;

public class ChooseRandomSprite : MonoBehaviour
{
	public Sprite[] Sprites;
	public SpriteRenderer spriteRenderer;


	void OnEnable()
	{
		spriteRenderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
	}

}
