﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This class handles all transitions (with scene changes or not).
/// If you are going to be transitioning a lot, consider keeping an instance of this and changing keepTransitionInstance to true
/// </summary>
public class TransitionKit : Singleton<TransitionKit>
{
	#region Fields
	/// <summary>
	/// flag for not changing the scene
	/// </summary>
	static bool fakeTransition;
	static bool meshIsDirty = true;
	/// <summary>
	/// Screen is covered
	/// </summary>
	public static Action onScreenObscured;
	/// <summary>
	/// fired when the transition is complete and TransitionKit has destroyed all of its objects
	/// </summary>
	static Action _onTranstionComplete;
	public static Action onTransitionComplete
	{
		get
		{
			if(!instances.ContainsKey(typeof(TransitionKit)))
			{
				// they are trying to access me without me. we are going to call it anyway, for deelopment purposes.
				instance.Invoke("FakeComplete", .1f);
			}
			return _onTranstionComplete;
		}

		set
		{
			_onTranstionComplete = value;
		}
	}
	void FakeComplete() { _onTranstionComplete?.Invoke(); }
	/// <summary>
	/// scene is fully loaded
	/// </summary>
	public static Action sceneLoaded;
	/// <summary>
	/// the default transition made if nothing is set in
	/// </summary>
	[ParentClass(typeof(TransitionKitDelegate))]
	public ChildClassSelector defaultTransition;

	static object _transitionKitDelegate;

	/// <summary>
	/// provides easy access to the camera used to obscure the screen. Handy when you want to change the clear flags for example.
	/// </summary>
	static public Camera transitionKitCamera;

	/// <summary>
	/// material access for delegates so they can mess with shader/material properties
	/// </summary>
	static MeshFilter backMeshF;
	static MeshRenderer backMeshRend;
	static MeshFilter meshFilter;
	public static MeshFilter MeshFilter
	{
		get
		{
			meshIsDirty = true;
			return meshFilter;
		}
	}
	static public MeshRenderer rend;

	/// <summary>
	/// helper property for use by all TransitionKitDelegates so they use the proper deltaTime
	/// </summary>
	/// <value>The delta time.</value>
	public static float DeltaTime
	{
		get { return ((TransitionKitDelegate)_transitionKitDelegate).useUnscaledTime ? Time.unscaledDeltaTime : Time.deltaTime; }
	}


	/// <summary>
	/// holds the instance while we are transitioning
	/// </summary>
	#endregion

	#region SETUP
	
	void Start()
	{
		sceneLoaded += TurnBackOff;
	}

	void initialize(string nextScene)
	{
		if (meshIsDirty)
		{
			meshFilter.mesh = generateQuadMesh();
			backMeshF.mesh = meshFilter.mesh;
			meshIsDirty = false;
		}

		// create the Material
		rend.material.shader = ((TransitionKitDelegate)_transitionKitDelegate).ShaderForTransition() ?? Shader.Find("Transitions/Texture With Alpha");
		//material.color = Color.white; // reset to fully white

		// snapshot the main camera before proceeding
		instance.StartCoroutine(instance.setupCameraAndTexture(nextScene));
	}

	public static Vector2 CameraToScreenSize(Camera cam)
	{
		float height = cam.orthographicSize*2;
		return new Vector2(height * Screen.width / Screen.height, height);
	}


	Mesh generateQuadMesh()
	{
		var halfHeight = 5f; // 5 is the camera.orthoSize which is the half height
		var halfWidth = halfHeight * (Screen.width / (float)Screen.height);

		var mesh = new Mesh();
		mesh.vertices = new Vector3[]
		{
				new Vector3( -halfWidth, -halfHeight),
				new Vector3( -halfWidth, halfHeight),
				new Vector3( halfWidth, -halfHeight),
				new Vector3( halfWidth, halfHeight)
		};
		mesh.uv = new Vector2[]
		{
				new Vector2( 0, 0 ),
				new Vector2( 0, 1 ),
				new Vector2( 1, 0 ),
				new Vector2( 1, 1 )
		};
		mesh.triangles = new int[] { 0, 1, 2, 3, 2, 1 };

		return mesh;
	}


	IEnumerator setupCameraAndTexture(string nextScene)
	{
		// load up the texture
		TransitionKitDelegate thisTrans = (TransitionKitDelegate)_transitionKitDelegate;
		if ((thisTrans).useScreenShot)
			yield return ApplyScreenShot();

		// create our camera to cover the screen
		SetCamera();

		onScreenObscured?.Invoke();
		// for transitions that need inital setup
		thisTrans.Init();

		// Load next scene if they want to change scenes
		LoadOrFakeScene(nextScene);
		yield return StartCoroutine(thisTrans.DoTransition(nextScene));
		if (thisTrans.endDelay != 0)
			yield return new WaitForSeconds(thisTrans.endDelay);

		cleanup();
	}

	void SetCamera()
	{
		// always reset these in case a transition messed with them
		transitionKitCamera.orthographic = true;
		transitionKitCamera.nearClipPlane = -1f;
		transitionKitCamera.farClipPlane = 1f;
		transitionKitCamera.depth =	100;
		transitionKitCamera.clearFlags = CameraClearFlags.Nothing;
	}

	/// <summary>
	/// get a Texture with a screen shot from the screen
	/// </summary>
	public static Texture2D getScreenshotTexture()
	{
		var screenSnapshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false, false);
		screenSnapshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
		screenSnapshot.Apply();

		return screenSnapshot;
	}

	public IEnumerator ApplyScreenShot()
	{
		yield return new WaitForEndOfFrame();
		rend.material.mainTexture = getScreenshotTexture();
		backMeshRend.material.mainTexture = rend.material.mainTexture;
		transform.GetChild(0).gameObject.SetActive(true);
		yield break;
	}

	/// <summary>
	/// this method signals a cleanup (duh) and notifies event listeners
	/// </summary>
	private void cleanup()
	{
		onTransitionComplete?.Invoke();
		gameObject.SetActive(false);
		_transitionKitDelegate = null;
	}

	void OnDestroy()
	{
		sceneLoaded -= TurnBackOff;
	}

	#endregion

	#region TRANSITIONS

	public static void RandomTransition(string _scene)
	{
		Type allTransType = typeof(TransitionKitDelegate);
		List<Type> types = new List<Type>(Assembly.GetAssembly(allTransType).GetTypes().Where(theType => theType.IsClass && !theType.IsAbstract && theType.IsSubclassOf(allTransType)));
		TransitionWithDelegate(Activator.CreateInstance(types[UnityEngine.Random.Range(0, types.Count)], new object[] { true }), _scene);
	}

	/// <summary>
	/// starts up a transition with the given delegate. use null to use default transition
	/// </summary>
	/// <param name="transitionKitDelegate">Transition kit delegate.</param>
	/// <param name="nextScene">empty string will reload scene</param>
	public static void TransitionWithDelegate(object transitionKitDelegate, string nextScene)
	{
		if (nextScene == "")
		{
			fakeTransition = true;
			nextScene = SceneManager.GetActiveScene().name;
		}
		if (transitionKitDelegate == null)
		{
			if ((instance).defaultTransition.storedInstance == null)
			{
				LoadOrFakeScene(nextScene);
				return;
			}

			transitionKitDelegate = (instance).defaultTransition.storedInstance;
		}
		
		instance.gameObject.SetActive(true);
		_transitionKitDelegate = transitionKitDelegate;
		instance.initialize(nextScene);
	}

	public static void FakeTransition(object transitionKitDelegate, string _name)
	{
		fakeTransition = true;
		TransitionWithDelegate(transitionKitDelegate, _name);
	}

	public static void FakeRandomTransition(string _name)
	{
		fakeTransition = true;
		RandomTransition(_name);
	}

	#endregion

	#region Tools
	static void LoadOrFakeScene(string _name)
	{
		if (!fakeTransition)
			SceneManager.LoadSceneAsync(_name);
		fakeTransition = false;
	}

	/// <summary>
	/// makes a single pixel Texture2D with a transparent pixel and sets it on the current Material. Useful for fading from obscured to a
	/// new scene. Note that of course your shader must support transparency for this to be useful
	/// </summary>
	public void makeTextureTransparent()
	{
		var tex = new Texture2D(1, 1);
		tex.SetPixel(0, 0, Color.clear);
		tex.Apply();

		rend.material.mainTexture = tex;
	}
	
	public void TurnBackOff()
	{
		transform.GetChild(0).gameObject.SetActive(false);
	} 

	/// <summary>
	/// helper for delegates that returns control back when the given level has loaded. Very handy when using async loading.
	/// </summary>
	/// <returns>The for level to load.</returns>
	/// <param name="level">Level.</param>
	public IEnumerator waitForLevelToLoad(string level)
	{
		while (SceneManager.GetActiveScene().name != level)
			yield return null;
	}


	/// <summary>
	/// the most common type of transition seems to be one that ticks progress from 0 - 1. This method takes care of that for you
	/// if your transition needs to have a _Progress property ticked after the scene loads.
	/// </summary>
	/// <param name="duration">duration</param>
	/// <param name="reverseDirection">if true, _Progress will go from 1 to 0. If false, it goes form 0 to 1</param>
	public IEnumerator tickProgressPropertyInMaterial(float duration, bool reverseDirection = false, string property = "_Progress")
	{
		var start = reverseDirection ? 1f : 0f;
		var end = reverseDirection ? 0f : 1f;

		var elapsed = 0f;
		while (elapsed < duration)
		{
			elapsed += DeltaTime;
			var step = Mathf.Lerp(start, end, Mathf.Pow(elapsed / duration, 2f));
			rend.material.SetFloat(property, step);

			yield return null;
		}
	}

	#endregion

}