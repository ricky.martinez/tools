﻿using System.Collections;
using UnityEngine;

public partial class DelegateInfo
{
	public float perspective = 1.5f;
	public float depth = 3.0f;
	public bool runEffectInReverse = false;
}

/// <summary>
/// the Doorway shader can have _Progress run from 0 to -1 or 0 to 1. The runEffectInReverse controls that.
/// </summary>
[System.Serializable]
public class DoorwayTransition : BasicTransition
{
	public float perspective = 1.5f;
	public float depth = 3.0f;
	public bool runEffectInReverse = false;


	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Doorway");
	}

	public override IEnumerator Transition()
	{
		//throw new System.NotImplementedException();
		yield break;
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));
	}

	public override void Init()
	{
		TransitionKit.rend.material.SetFloat("_Perspective", perspective);
		TransitionKit.rend.material.SetFloat("_Depth", depth);
		TransitionKit.rend.material.SetInt("_Direction", runEffectInReverse ? 1 : 0);
	}
	#endregion

}