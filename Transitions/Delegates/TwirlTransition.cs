﻿using UnityEngine;
using System.Collections;

public partial class DelegateInfo
{
	public float endAngle = 1800f;
	public float angle = 0f;
	public Vector2 center = new Vector2(0.5f, 0.5f);
	public Vector2 radius = new Vector2(0.3f, 0.3f);
}

/// <summary>
/// this is an example of how you can use a standard MonoBehaviour subclass to be able to edit values via the Inspector.
/// the twirl shader is pulled directly from Unity's image effects.
/// </summary>
[System.Serializable]
public class TwirlTransition : BasicTransition
{
	public float endAngle = 1800f;
	public float angle = 0f;
	public Vector2 center = new Vector2(0.5f, 0.5f);
	public Vector2 radius = new Vector2(0.3f, 0.3f);
	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/TwirlEffect");
	}

	public override IEnumerator Transition()
	{
		var elapsed = 0f;
		while (elapsed < duration)
		{
			elapsed += Time.deltaTime;
			var step = Mathf.Pow(elapsed / duration, 2f);
			angle = Mathf.Lerp(0f, endAngle, step);

			var rotationMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, angle), Vector3.one);
			TransitionKit.rend.material.SetMatrix("_RotationMatrix", rotationMatrix);
			TransitionKit.rend.material.SetVector("_CenterRadius", new Vector4(center.x, center.y, radius.x, radius.y));
			TransitionKit.rend.material.SetFloat("_Angle", angle * Mathf.Deg2Rad);

			yield return null;
		}
	}

	public override IEnumerator TransitionBack()
	{
		yield break;
	}

	#endregion

}
