﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// this is the interface an object must conform to for use with TransitionKit. Delegates can return custom shaders, meshes
/// and textures if needed.
/// </summary>
public abstract class TransitionKitDelegate
{
	/// <summary>
	/// sets whether TransitionKit will use unscaledDeltaTime or standard deltaTime
	/// </summary>
	public bool useUnscaledTime;
	public bool useScreenShot = true;
	/// <summary>
	/// useful for drawing over the 
	/// </summary>
	public float duration = 2, reverseDuration = 2, startDelay, midDelay, endDelay;
	/// <summary>
	/// if the transition needs a custom shader return it here otherwise return null which will use the TextureWithAlpha shader
	/// </summary>
	/// <returns>The for transition.</returns>
	public virtual Shader ShaderForTransition()
	{
		return shader;
	}
	[SerializeField]
	protected Shader shader;


	/// <summary>
	/// called when the screen is fully obscured. You can now load a new scene or modify the current one and it will be fully obscured from view.
	/// Note that when control returns from this method TransitionKit will kill itself.
	/// </summary>
	public IEnumerator DoTransition(string nextScene)
	{
		if(startDelay !=0)
			yield return new WaitForSeconds(startDelay);

		// the actual transition
		yield return TransitionKit.instance.StartCoroutine(Transition());
		
		//makes sure that we are actually waiting until the scene loads
		float time = Time.time;
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.waitForLevelToLoad(nextScene));
		// this will allow the delay to include the time it takes to load the scene
		TransitionKit.sceneLoaded?.Invoke();
		midDelay = Mathf.Max(0, midDelay - ( Time.time - time));

		// how long to wait once mid point (transition occured, but we still need to un transition)
		if(midDelay != 0)
		yield return new WaitForSeconds(midDelay);

		yield return TransitionKit.instance.StartCoroutine(TransitionBack());
	}

	public virtual void Init() { }

	public abstract IEnumerator Transition();
	public abstract IEnumerator TransitionBack();
}

public class BasicTransition : TransitionKitDelegate
{
	public override IEnumerator Transition()
	{
		throw new System.NotImplementedException();
	}

	public override IEnumerator TransitionBack()
	{
		throw new System.NotImplementedException();
	}
}