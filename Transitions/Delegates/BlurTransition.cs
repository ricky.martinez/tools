﻿using System.Collections;
using UnityEngine;

public partial class DelegateInfo
{
	public float blurMin = 0.0f;
	public float blurMax = 0.01f;
}

[System.Serializable]
public class BlurTransition : BasicTransition
{
	public float blurMin = 0.0f;
	public float blurMax = 0.01f;
	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Blur");
	}

	public override IEnumerator Transition()
	{
		var elapsed = 0f;
		while (elapsed < duration)
		{
			elapsed += TransitionKit.DeltaTime;
			var step = Mathf.Pow(elapsed / duration, 2f);
			var blurAmount = Mathf.Lerp(blurMin, blurMax, step);

			TransitionKit.rend.material.SetFloat("_BlurSize", blurAmount);

			yield return null;
		}
	}

	public override IEnumerator TransitionBack()
	{
		//throw new System.NotImplementedException();
		yield break;
	}

	#endregion

}