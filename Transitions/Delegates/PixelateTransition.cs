﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class PixelateTransition : BasicTransition
{
	// settings for the pixellate shade
	public float pixellateMin = 0.001f;
	public float pixellateMax = 0.08f;
	public float pixelatedDelay = 0f;
	public PixelateFinalScaleEffect finalScaleEffect = PixelateFinalScaleEffect.ToPoint;
	public enum PixelateFinalScaleEffect
	{
		ToPoint,
		Zoom,
		Horizontal,
		Vertical,
		Random
	}

	public PixelateFinalScaleEffect GetRandomEffect()
	{
		return (PixelateFinalScaleEffect)Random.Range(0, (int)PixelateFinalScaleEffect.Random);
	}

	#region TransitionKitDelegate

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Pixelate");
	}

	public override IEnumerator Transition()
	{
		TransitionKit.rend.material.SetFloat("_WidthAspectMultiplier", 1f / Camera.main.aspect);

		var elapsed = 0f;
		while (elapsed < duration)
		{
			elapsed += TransitionKit.DeltaTime;
			var step = Mathf.Pow(elapsed / duration, 2f);
			TransitionKit.rend.material.SetFloat("_CellSize", Mathf.Lerp(pixellateMin, pixellateMax, step));

			yield return new WaitForSeconds(pixelatedDelay);
		}
	}

	public override IEnumerator TransitionBack()
	{
		TransitionKit transitionKit = TransitionKit.instance;
		var desiredScale = Vector3.zero;
		if (finalScaleEffect == PixelateFinalScaleEffect.Random)
			finalScaleEffect = GetRandomEffect();
		switch (finalScaleEffect)
		{
			case PixelateFinalScaleEffect.ToPoint:
				desiredScale = new Vector3(0f, 0f, transitionKit.transform.localScale.z);
				break;
			case PixelateFinalScaleEffect.Zoom:
				desiredScale = new Vector3(transitionKit.transform.localScale.x * 5f, transitionKit.transform.localScale.y * 5f, transitionKit.transform.localScale.z);
				break;
			case PixelateFinalScaleEffect.Horizontal:
				desiredScale = new Vector3(transitionKit.transform.localScale.x, 0, transitionKit.transform.localScale.z);
				break;
			case PixelateFinalScaleEffect.Vertical:
				desiredScale = new Vector3(0, transitionKit.transform.localScale.y, transitionKit.transform.localScale.z);
				break;
			default:
				break;
		}

		yield return transitionKit.StartCoroutine(animateScale(transitionKit, reverseDuration * 0.5f, desiredScale));
	}
	#endregion


	public IEnumerator animateScale(TransitionKit transitionKit, float duration, Vector3 desiredScale)
	{
		var originalScale = transitionKit.transform.localScale;

		var elapsed = 0f;
		while (elapsed < duration)
		{
			elapsed += TransitionKit.DeltaTime;
			var step = Mathf.Pow(elapsed / duration, 2f);
			TransitionKit.instance.transform.localScale = Vector3.Lerp(originalScale, desiredScale, step);

			yield return null;
		}
	}
}