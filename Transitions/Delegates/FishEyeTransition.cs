﻿using System.Collections;
using UnityEngine;

public partial class DelegateInfo
{
	public float zoom = 100.0f;
	public float colorSeparation = 0.2f;
}

[System.Serializable]
public class FishEyeTransition : BasicTransition
{
	public float zoom = 100.0f;
	public float colorSeparation = 0.2f;
	public float size = 2f;
	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Fish Eye");
	}

	public override void Init()
	{
		TransitionKit.rend.material.SetFloat("_Size", size);
		TransitionKit.rend.material.SetFloat("_Zoom", zoom);
		TransitionKit.rend.material.SetFloat("_ColorSeparation", colorSeparation);
	}

	public override IEnumerator Transition()
	{
		//throw new System.NotImplementedException();
		yield break;
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));
	}

	#endregion

}