﻿using System.Collections;
using UnityEngine;

/// <summary>
/// the maskTexture will show the background image (screen grab) where it is transparent and the backgroundColor where it is not.
/// it zooms to a point in the center of the screen then fades back in after the new scene loads.
/// </summary>
[System.Serializable]
public class ImageMaskTransition : BasicTransition
{
	public Texture2D maskTexture;
	public Color outsideColor = Color.black;
	public Color insideColor = Color.white;
	#region TransitionKitDelegate

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Mask");
	}

	public override void Init()
	{
		TransitionKit.rend.material.SetColor("_Color", outsideColor);
		TransitionKit.rend.material.SetTexture("_MaskTex", maskTexture);
		TransitionKit.rend.material.SetColor("_InsideColor", insideColor);
	}

	public override IEnumerator Transition()
	{
		// this does the zoom/rotation
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));
	}

	public override IEnumerator TransitionBack()
	{
		// now that the new scene is loaded we zoom the mask back out
		//TransitionKit.instance.makeTextureTransparent();

		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(reverseDuration, true));
	}

	#endregion

}
//}