﻿using System.Collections;
using UnityEngine;

public partial class DelegateInfo
{
	/// <summary>
	/// if true, the CurvedWind shader will be used which has the wind come from the top-left to bottom-left then across to the right.
	/// </summary>
	public bool useCurvedWind = false;
	/// <summary>
	/// how much of the screen horizontally should the transition encompass? Higher numbers mean a wider transition.
	/// </summary>
	public float size = 0.3f;
	/// <summary>
	/// how many vertical sections of "wind" should we use? Higher numbers mean more whispy wind.
	/// </summary>
	public float windVerticalSegments = 100.0f;
}

[System.Serializable]
public class WindTransition : BasicTransition
{
	/// <summary>
	/// if true, the CurvedWind shader will be used which has the wind come from the top-left to bottom-left then across to the right.
	/// </summary>
	public bool useCurvedWind = false;
	/// <summary>
	/// how much of the screen horizontally should the transition encompass? Higher numbers mean a wider transition.
	/// </summary>
	public float size = 0.3f;
	/// <summary>
	/// how many vertical sections of "wind" should we use? Higher numbers mean more whispy wind.
	/// </summary>
	public float windVerticalSegments = 100.0f;
	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		return useCurvedWind ? Shader.Find("Transitions/CurvedWind") : Shader.Find("Transitions/Wind");
	}

	public override IEnumerator Transition()
	{

		yield break;
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));

	}

	public override void Init()
	{
		TransitionKit.rend.material.SetFloat("_Size", size);
		TransitionKit.rend.material.SetFloat("_WindVerticalSegments", windVerticalSegments);
	}

	#endregion

}