﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class FadeTransition : BasicTransition
{
	public Color fadeToColor = Color.black;
	/// <summary>
	/// the effect looks best when it pauses before fading back. When not doing a scene-to-scene transition you may want
	/// to pause for a breif moment before fading back.
	/// </summary>


	#region TransitionKitDelegate

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Fader");
	}

	public override void Init()
	{
		TransitionKit.rend.material.color = fadeToColor;
	}

	public override IEnumerator Transition()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));

		TransitionKit.instance.makeTextureTransparent();
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration, true));
	}

	#endregion

}