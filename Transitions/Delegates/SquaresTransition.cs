﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class DelegateInfo
{
	public Color squareColor = Color.black;
	public Vector2 squareSize = new Vector2(13f, 9f);
	public float smoothness = 0.5f;
}

public class SquaresTransition : BasicTransition
{
	public Color squareColor = Color.black;
	public Vector2 squareSize = new Vector2(13f, 9f);
	public float smoothness = 0.5f;
	#region TransitionKitDelegate

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Squares");
	}

	public override IEnumerator Transition()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));

		TransitionKit.instance.makeTextureTransparent();
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(reverseDuration, true));

	}

	public override void Init()
	{
		TransitionKit.rend.material.color = squareColor;
		TransitionKit.rend.material.SetFloat("_Smoothness", smoothness);
		TransitionKit.rend.material.SetVector("_Size", squareSize);
	}

	#endregion

}