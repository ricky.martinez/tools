﻿using System.Collections;
using UnityEngine;

public partial class DelegateInfo
{
	public float speed = 50.0f;
	public float amplitude = 100.0f;
}

[System.Serializable]
public class RippleTransition : BasicTransition
{
	public float speed = 50.0f;
	public float amplitude = 100.0f;


	#region TransitionKitDelegate implementation

	override public Shader ShaderForTransition()
	{
		if (shader)
			return shader;
		return Shader.Find("Transitions/Ripple");
	}

	public override IEnumerator Transition()
	{
		yield break;
	}

	public override IEnumerator TransitionBack()
	{
		yield return TransitionKit.instance.StartCoroutine(TransitionKit.instance.tickProgressPropertyInMaterial(duration));
	}

	public override void Init()
	{
		TransitionKit.rend.material.SetFloat("_Speed", speed);
		TransitionKit.rend.material.SetFloat("_Amplitude", amplitude);
	}
	#endregion

}