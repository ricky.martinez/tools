﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEditor.Events;
using static Unity.Burst.Intrinsics.X86;
using UnityEditor;
using UnityEngine.Serialization;
using System.Collections.Generic;

    [System.Serializable]
    public struct RangeOfIntegers
    {
        public int Minimum;
        public int Maximum;
    }

    [System.Serializable]
    public struct RangeOfFloats
    {
        public float Minimum;
        public float Maximum;
    }

    public class CollidingParticle : MonoBehaviour, ICollisionHandler
    {
		public bool DestroysOnCollision = true, fadeOutNonCollParticles = true, collideEventEveryTime;
		private bool didCollideEvent;
		public Rigidbody IsProjectile;
		[Tooltip("Optional audio source to play once when the script starts.")]
        public AudioSource AudioSource;

        [Tooltip("used to fade in animations and sounds, etc.")]
        public float FadeInTime = 1.0f;

        [Tooltip("used to fade out animations and sounds, etc.")]
        public float FadeOutTime = 3.0f;

        [Tooltip("How long the effect lasts. Once the duration ends, the script lives for FadeOutTime and then the object is destroyed.")]
        public float Duration = 2.0f;

        [Tooltip("How much force to create at the center (explosion), 0 for none.")]
        public float ImpactForceAmount = 2f;

        [Tooltip("The radius of the force, 0 for none.")]
        public float ImpactForceRadius = 1;

        [Tooltip("Particle systems that must be manually started and will not be played on start.")]
        public ParticleSystem[] CollisionParticles;
		List<ParticleSystem> nonCollParticles;

        private float startTimeMultiplier=1;
        private float startTimeIncrement=1;

        private float stopTimeMultiplier=1;
        private float stopTimeIncrement=1;

		public UnityEvent AfterFadeOut;

		private IEnumerator CleanupEverythingCoRoutine()
        {
            // 2 extra seconds just to make sure animation and graphics have finished ending
            yield return new WaitForSeconds(FadeOutTime + .2f);
			AfterFadeOut?.Invoke();
        }

        static public void StartParticleSystems(ParticleSystem[] particles)
        {
            foreach (ParticleSystem p in particles)
            {
                    if (p.main.startDelay.constant == 0.0f)
                    {
                        // wait until next frame because the transform may change
                        var m = p.main;
                        var d = p.main.startDelay;
                        d.constant = 0.01f;
                        m.startDelay = d;
                    }
                    p.Play();
            }
        }
#if UNITY_EDITOR
		protected virtual void Reset()
		{
			// Ensure the event is initialized.
			if (OnCollide == null)
				OnCollide = new ColliderEvent();

			// Add HandleCollider as a persistent listener so it appears in the Inspector.
			 UnityEditor.Events.UnityEventTools.AddPersistentListener(OnCollide, CreateBaseExplosion);
		}
#endif

		protected virtual void Awake()
        {
            Starting = true;
            int fireLayer = UnityEngine.LayerMask.NameToLayer("FireLayer");
            UnityEngine.Physics.IgnoreLayerCollision(fireLayer, fireLayer);
        }

        protected virtual void Start()
        {
            // precalculate so we can multiply instead of divide every frame
            stopTimeMultiplier = 1.0f / FadeOutTime;
            startTimeMultiplier = 1.0f / FadeInTime;
        }

        protected virtual void Update()
        {
            // reduce the duration
            Duration -= Time.deltaTime;
            if (Stopping)
            {
                // increase the stop time
                stopTimeIncrement += Time.deltaTime;
                if (stopTimeIncrement < FadeOutTime)
                {
                    StopPercent = stopTimeIncrement * stopTimeMultiplier;
                }
				if (fadeOutNonCollParticles)
				{

				}
            }
            else if (Starting)
            {
                // increase the start time
                startTimeIncrement += Time.deltaTime;
                if (startTimeIncrement < FadeInTime)
                {
                    StartPercent = startTimeIncrement * startTimeMultiplier;
                }
                else
                {
                    Starting = false;
                }
            }
            else if (Duration <= 0.0f)
            {
                // time to stop, no duration left
                Stop();
            }
        }

		public void CreateBaseExplosion(Collision c )
		{
			CreateExplosion(c.contacts[0].point, ImpactForceRadius, ImpactForceAmount, CollisionParticles);
		}

        public static void CreateExplosion(Vector3 pos, float radius, float force, ParticleSystem[] particles)
        {
			StartParticleSystems(particles);
            if (force <= 0.0f || radius <= 0.0f)
            {
                return;
            }

			// find all colliders and add explosive force
			Collider[] objects = UnityEngine.Physics.OverlapSphere(pos, radius);
            foreach (Collider h in objects)
            {
                Rigidbody r = h.GetComponentInParent<Rigidbody>();
                if (r != null)
                {
                    r.AddExplosionForce(force, pos, radius);
                }
            }
        }

        public virtual void Stop()
        {
			Duration = 0;
            if (Stopping)
            {
                return;
            }
            Stopping = true;

			if (fadeOutNonCollParticles)
			{
				nonCollParticles = new List<ParticleSystem>();
				foreach (ParticleSystem p in gameObject.GetComponentsInChildren<ParticleSystem>())
				{
					if (!System.Array.Exists(CollisionParticles, x => x == p))
					{
						nonCollParticles.Add(p);
					}
				}
			}

            StartCoroutine(CleanupEverythingCoRoutine());
        }

		public void Throw(bool useGravity, Vector3 vel)
		{
			IsProjectile.velocity = vel;
			IsProjectile.useGravity = useGravity;
		}

		public void OnCollisionEnter(Collision collision)
		{
			if (DestroysOnCollision)
				Stop();
			if(collideEventEveryTime || !didCollideEvent)
			{
				OnCollide?.Invoke(collision);
				didCollideEvent = true;
			}
		}

		public bool Starting
        {
            get;
            private set;
        }

        public float StartPercent
        {
            get;
            private set;
        }

        public bool Stopping
        {
            get;
            private set;
        }

        public float StopPercent
        {
            get;
            private set;
        }

		[field: SerializeField]
		public ColliderEvent OnCollide { get; set; }
	}


// Create a custom UnityEvent that accepts a Collider parameter.
[System.Serializable]
public class ColliderEvent : UnityEvent<Collision> { }