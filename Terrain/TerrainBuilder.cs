using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainBuilder : MonoBehaviour
{
	public float[,,] map;
    public Terrain t;
	// Start is called before the first frame update
	void Start()
	{
		map = new float[t.terrainData.alphamapWidth, t.terrainData.alphamapHeight, 5];
		// For each point on the alphamap...
		for (int y = 0; y < t.terrainData.alphamapHeight; y++)
		{
			for (int x = 0; x < t.terrainData.alphamapWidth; x++)
			{
				// Get the normalized terrain coordinate that
				// corresponds to the the point.
				float normX = x * 1.0f / (t.terrainData.alphamapWidth - 1);
				float normY = y * 1.0f / (t.terrainData.alphamapHeight - 1);

				// Get the steepness value at the normalized coordinate.
				float angle = t.terrainData.GetSteepness(normX, normY);

				// Steepness is given as an angle, 0..90 degrees. Divide
				// by 90 to get an alpha blending value in the range 0..1.
				float frac = angle / 90.0f;
				float height = t.terrainData.GetHeight(x, y) / 500;
				// low tar (based on height)
				// flat i want 50% green, and mix of other grass no ground
				// small curve, no green, mix of grass, small amount of ground
				// large curve, ground, amll amount of grass based on height

				if (height > .045f)// tar line
				{
					float rockVal = Mathf.Clamp01(frac - .3f);
					float half = Random.Range(0f, 1f - rockVal);
					map[y, x, 0] = half;
					map[y, x, 1] = 1f - rockVal - half;
					map[y, x, 2] = 0;
					map[y, x, 3] = rockVal;
					map[y, x, 4] = Mathf.Clamp01(.6f * (.3f - frac) * (1 - height));
				}
				else
				{
					map[y, x, 0] = 0;
					map[y, x, 1] = 0;
					map[y, x, 2] = 1;
					map[y, x, 3] = 0;
					map[y, x, 4] = 0;
				}
			}
		}
		t.terrainData.SetAlphamaps(0, 0, map);
	}
}
