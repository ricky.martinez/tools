﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputExt
{
		public static Dictionary<string, bool> keys = new Dictionary<string, bool>();
		//public static Hashtable keys1;
		static public bool GetAxisIsDown(string axis)
		{
			return Input.GetAxisRaw(axis) != 0;
		}

		static public bool GetAxisOnDown(string axis)
		{
			if (Input.GetAxisRaw(axis) != 0)
			{
				if (!keys[axis])
				{
					// Call your event function here.
					keys[axis] = true;
					return true;
				}
				return false;
			}
			else
			{
				keys[axis] = false;
				return false;
			}
		}
}
