﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

    public interface ICollisionHandler
    {

		[field: SerializeField]
		ColliderEvent OnCollide { get; set; }

		void OnCollisionEnter(Collision collision);
    }